// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Templates/Tuple.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "HCGameplayAbilityLibrary.generated.h"



/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGameplayAbilityLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "HC|Lib|GameplayAbility")
		static void RemoveLooseGameplayTag(class UAbilitySystemComponent* InASC, FGameplayTag GameplayTagToRemove, int32 AmountToRemove);

	UFUNCTION(BlueprintCallable, Category = "HC|Lib|GameplayAbility")
		static float CalculateLaunchVelocity(const FVector& Start, const FVector& Target, float LaunchAngle, float GravityZ = 9.81f);
};
