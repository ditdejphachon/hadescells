// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "HCANS_JumpSection.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCANS_JumpSection : public UAnimNotifyState
{
	GENERATED_BODY()
	
public:
	UHCANS_JumpSection();

	//UAnimNotifyState interfaces
	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference) override;
	virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime, const FAnimNotifyEventReference& EventReference) override;
	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;
	//~UAnimNotifyState interfaces

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HC|Anim|Notify State") FName JumpSectionName;
};
