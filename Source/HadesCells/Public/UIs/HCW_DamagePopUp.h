// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include <GameplayTagContainer.h>
#include "HCW_DamagePopUp.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_DamagePopUp : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) class UTextBlock* DamageText;

	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor HeroDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor ShieldDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor BlueFlameDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor FireDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor WaterDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor IceDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor PoisonDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor ElectricityDamageTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor AddHealthTextStyle;
	UPROPERTY(EditAnywhere, Category = "HC|UI|Text") const FSlateColor DefaultDamageTextStyle;

	UPROPERTY(EditAnywhere, Category = "HC|UI") float TimeTilFadeAway = 1.0f;
	bool bIsHero = false;

public:
	UFUNCTION(BlueprintCallable) void SetDamageText(const float& InDamageValue, const FGameplayTagContainer& AdditionalTagContainer);
	UFUNCTION(BlueprintImplementableEvent) void K2_PostConstruct();
protected:
	virtual void NativeConstruct() override;
	UFUNCTION() void SelfDestruct();
private:
	FTimerHandle FadeTimerHandle;
};
