// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include <Equipment/HCEquipmentDefinition.h>
#include "Equipment/HCEquipmentInstance.h"
#include "HCHUDBase.generated.h"

class UHCW_HeroOverlay;
class UHCW_SwapEquipment;
class UHCHealthComponent;
class UHCProgressionSystemComponent;
class UHCEquipmentManagerComponent;
class UHCW_UpgradeEquipment;
/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCHUDBase : public AHUD
{
	GENERATED_BODY()
	
public:
	AHCHUDBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable) void ToggleHeroOverlay(bool bIsShow);
	void SetUpHeroOverlayHealthSection(UHCHealthComponent* InHealthComponent);
	void SetUpHeroGoldSection(UHCProgressionSystemComponent* InProgressionComponent);
	void SetUpHeroExpSection(UHCProgressionSystemComponent* InProgressionComponent);
	void SetUpHeroOverlayEquipmentSection(UHCEquipmentManagerComponent* InEquipmentManagerComponent);

	void UpdateEquipmentSlot(const UHCEquipmentDefinition* InEquipmentDefinition);
	void ToggleEquipmentSwappingUI(bool bIsShow, UUHCEquipmentData* EquipmentData, EHCEquipmentType EquipmentType);
	void SetUpEnemyKillUI(uint8 EnemiesToKill);
	void UpdateEnemyKillUI(uint8 EnemyKilled);

	void EnableEquipmentCooldown(const FGameplayTag& InTag, float InCooldownTime);

	UFUNCTION(BlueprintCallable) void ToggleDeathScreen(bool bIsShow);

	void ToggleEquipmentUpgradeUI(bool bIsShow);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "HC|UI") TSubclassOf<UHCW_HeroOverlay> WBP_HeroOverlay;
	UPROPERTY(EditDefaultsOnly, Category = "HC|UI") TSubclassOf<UHCW_SwapEquipment> WBP_SwapEquipment;
	UPROPERTY(EditDefaultsOnly, Category = "HC|UI") TSubclassOf<UUserWidget> WBP_DeathScreen;
	UPROPERTY(EditDefaultsOnly, Category = "HC|UI") TSubclassOf<UHCW_UpgradeEquipment> WBP_UpgradeEquipment;


	UHCW_HeroOverlay* W_HeroOverlay;
	UHCW_SwapEquipment* W_SwapEquipment;
	UUserWidget* W_DeathScreen;
	UHCW_UpgradeEquipment* W_UpgradeEquipment;

protected:
	UFUNCTION() void HandleEquipmentAdded(UUHCEquipmentData* EquipmentData, EHCEquipmentSlotType SlotType);
	UFUNCTION() void HandleEquipmentSlotFull(UUHCEquipmentData* EquipmentData, EHCEquipmentType EquipmentType);

};
