// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Equipment/HCEquipmentInstance.h"
#include "HCW_HeroOverlay.generated.h"

class UHCW_EquipmentSlot;
class UProgressBar;
class UTextBlock;
class UUHCEquipmentData;
class UHCW_GameModeInfo;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_HeroOverlay : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UProgressBar* HealthBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UProgressBar* ShieldBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UTextBlock* HealthText;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UTextBlock* ShieldText;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UHCW_EquipmentSlot* MainWeaponSlot;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UHCW_EquipmentSlot* SecondaryWeaponSlot;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UHCW_EquipmentSlot* MainSkillSlot;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UHCW_EquipmentSlot* SecondarySkillSlot;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UHCW_GameModeInfo* GameModeInfo;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UProgressBar* ExpBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UTextBlock* GoldText;

public:
	void SetHealth(const float& InCurrentValue, const float& InMaxValue, const float& InNormalizedValue);
	void SetShield(const float& InCurrentValue, const float& InMaxValue, const float& InNormalizedValue);
	void SetEquipmentSlot(UUHCEquipmentData* InEquipmentData, EHCEquipmentSlotType SlotType);
	void SetEnemyKilledAmount(uint8 InKilledAmount);
	void SetTotalEnemyAmount(uint8 InTotalAmount);

	void SetExp(const float& InNormalizedValue);
	void SetGold(const float& InCurrentValue);

protected:
	UFUNCTION(BlueprintImplementableEvent) void K2_OnExpChanged();
	UFUNCTION(BlueprintImplementableEvent) void K2_OnGoldChanged();
	virtual void NativeConstruct() override;
	virtual void NativeDestruct() override;
private:
	FText GetAttributeText(const float& InCurrentValue, const float& InMaxValue);
	void SetText(UTextBlock* InTextBlock, const FText& InText);
	void SetProgressBar(UProgressBar* InProgressBar, const float& InValue);

};
