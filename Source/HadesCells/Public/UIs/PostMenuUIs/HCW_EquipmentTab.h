// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HCW_EquipmentTab.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_EquipmentTab : public UUserWidget
{
	GENERATED_BODY()
	
};
