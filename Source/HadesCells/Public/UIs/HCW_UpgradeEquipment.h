// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "HCW_EquipmentCard.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HCW_UpgradeEquipment.generated.h"

class UHCW_EquipmentCard;
class UUHCEquipmentData;
class UVerticalBox;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_UpgradeEquipment : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget)) UVerticalBox* EquipmentList;
	UPROPERTY(EditDefaultsOnly, Category = "HC|UI") TSubclassOf<UHCW_EquipmentCard> WBP_EquipmentCardClass;
public:
	void SetEquipmentList(TArray<UUHCEquipmentData*> Infos);
	void SetUpgradeEquipment(UUHCEquipmentData* Data);
protected:
	void ConfirmUpgrade();
private:
	TArray<UHCW_EquipmentCard*> m_UpgradeCards;
	UUHCEquipmentData* m_EquipmentDataToUpgrade;
};
