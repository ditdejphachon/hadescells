// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Equipment/HCEquipmentInstance.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HCW_SwapEquipment.generated.h"

class UUHCEquipmentData;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_SwapEquipment : public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(meta = (BindWidget)) class UImage* Equipment_Image_1; // TODO: replace with the actual equipment card widget
	UPROPERTY(meta = (BindWidget)) class UImage* Equipment_Image_2; // TODO: replace with the actual equipment card widget
	UPROPERTY(meta = (BindWidget)) class UImage* Equipment_Image_3; // TODO: replace with the actual equipment card widget
	UPROPERTY(meta = (BindWidget)) class UImage* Equipment_Border_1;
	UPROPERTY(meta = (BindWidget)) class UImage* Equipment_Border_2;

public:
	void SetEquipmentImages(TArray<UUHCEquipmentData*> EquipmentDatas, EHCEquipmentType EquipmentType);
	UFUNCTION(BlueprintCallable) void MoveLeft();
	UFUNCTION(BlueprintCallable) void MoveRight();

	virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent);
protected:
	virtual void NativeConstruct() override;
	void ConfirmReplacement();
	UFUNCTION() FEventReply OnClickImage_1(FGeometry MyGeometry, const FPointerEvent& MouseEvent);
	UFUNCTION() FEventReply OnClickImage_2(FGeometry MyGeometry, const FPointerEvent& MouseEvent);

private:
	UUHCEquipmentData* EquipmentDataToReplace;
	EHCEquipmentType EquipmentTypeToReplace;
	TArray<UUHCEquipmentData*> EDs;
};
