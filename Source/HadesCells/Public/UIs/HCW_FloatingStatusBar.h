// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HCW_FloatingStatusBar.generated.h"

class UProgressBar;
class UVerticalBox;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_FloatingStatusBar : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UVerticalBox* StatusVerticalBox;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UProgressBar* HealthBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UProgressBar* ShieldBar;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UProgressBar* PostureBar;


public:
	void SetHealthPercentage(float HealthPercentage);
	void SetShieldPercentage(float ShieldPercentage);
	void SetPosturePercentage(float PosturePercentage);
	void RemoveShieldBar();
};
