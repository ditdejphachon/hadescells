// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include <Equipment/HCEquipmentInstance.h>
#include "HCW_EquipmentSlot.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_EquipmentSlot : public UUserWidget
{
	GENERATED_BODY()

public:
	UHCW_EquipmentSlot(const FObjectInitializer& ObjectInitializer);
	void SetEquipmentImage(UTexture2D* InImage);
	void EnableCoolDown(float InCooldownTime);
public:
	UPROPERTY(meta = (BindWidget)) class UImage* EquipmentImage;
	UPROPERTY(meta = (BindWidget)) class UImage* ActivateButtonImage;
	UPROPERTY(meta = (BindWidget)) class UImage* CooldownImage;
	UPROPERTY(meta = (BindWidget)) class UTextBlock* CooldownTimeRemaining;

protected:
	virtual void NativeConstruct() override;
	virtual void NativeDestruct() override;

protected:
	UPROPERTY(EditAnywhere, Category = "HC|UI") EHCEquipmentSlotType EquipmentType;
	UPROPERTY(EditAnywhere, Category = "HC|UI") float CooldownDelay = 0.1f;
private:
	class UTexture2D* ActivateButtonImageTexture;
	float CurrentTimeRemaining;
	float MaxCooldownTime;
	FTimerHandle CooldownHandle;
};
