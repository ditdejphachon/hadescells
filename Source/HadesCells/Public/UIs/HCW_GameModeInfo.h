// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HCW_GameModeInfo.generated.h"

class UTextBlock;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_GameModeInfo : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UTextBlock* Objective;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget)) UTextBlock* EnemiesKilledAmount;
public:
	void SetObjectiveText(const FString& InObjectiveText);
	void SetEnemyKilledAmount(uint8 InKilledAmount);
	void SetTotalEnemyAmount(uint8 InTotalAmount) { TotalEnemyAmount = InTotalAmount; }
private:
	uint8 TotalEnemyAmount;
};
