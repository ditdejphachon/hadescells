// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HCW_EquipmentCard.generated.h"

class UButton;
class UImage;
class UTextBlock;
class UUHCEquipmentData;
class UHCW_UpgradeEquipment;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCW_EquipmentCard : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(meta = (BindWidget)) class UButton* OverlayButton;
	UPROPERTY(meta = (BindWidget)) class UImage* EquipmentImage; 
	UPROPERTY(meta = (BindWidget)) class UTextBlock* EquipmentName; 
	UPROPERTY(meta = (BindWidget)) class UTextBlock* EquipmentLevel; 
	UPROPERTY(meta = (BindWidget)) class UTextBlock* EquipmentDescriptions; 

public:
	UFUNCTION(BlueprintCallable) void SelectCard();
	void SetEquipmentInfo(UUHCEquipmentData* EquipmentData);
	void SetOwner(UHCW_UpgradeEquipment* InOwner);

private:
	UUHCEquipmentData* m_OwningEquipmentData = nullptr;
	UHCW_UpgradeEquipment* m_UpgradeEquipmentWidget = nullptr;
};
