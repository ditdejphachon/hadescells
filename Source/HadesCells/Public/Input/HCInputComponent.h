// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "HCInputConfig.h"
#include "Abilities/HCAbilitySystemComponent.h"

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "HCInputComponent.generated.h"

class UInputAction;
/**
 * 
 */
UCLASS(Config = Input)
class HADESCELLS_API UHCInputComponent : public UEnhancedInputComponent
{
	GENERATED_BODY()

public:
	UHCInputComponent(const FObjectInitializer& ObjectInitializer);
	
public:
	template<class UserClass, typename FuncType>
	void BindNativeAction(const UHCInputConfig* InputConfig, const FGameplayTag& InputTag, ETriggerEvent TriggerEvent, UserClass* Object, FuncType Func);

	template<class UserClass, typename PressedFuncType, typename ReleasedFuncType>
	void BindAbilityActions(const UHCInputConfig* InputConfig, UserClass* Object, PressedFuncType PressedFunc, ReleasedFuncType ReleasedFunc, TArray<uint32>& BindHandles);

	//template<class UserClass, typename TriggeredFuncType>
	//void BindAbilityActions(const UHCInputConfig* InputConfig, UserClass* Object, TriggeredFuncType TriggeredFunc, TArray<uint32>& BindHandles);

	/*template<class UserClass>
	void BindAbilityActions(const UHCInputConfig* InputConfig, UserClass* Object, UAbilitySystemComponent* ASC, bool bIsPressed );*/
};

template<class UserClass, typename FuncType>
void UHCInputComponent::BindNativeAction(const UHCInputConfig* InputConfig, const FGameplayTag& InputTag, ETriggerEvent TriggerEvent, UserClass* Object, FuncType Func)
{
	check(InputConfig);
	if (const UInputAction* IA = InputConfig->FindNativeInputActionForTag(InputTag))
	{
		BindAction(IA, TriggerEvent, Object, Func);
	}
}

//template<class UserClass>
//inline void UHCInputComponent::BindAbilityActions(const UHCInputConfig* InputConfig, UserClass* Object, UAbilitySystemComponent* ASC, bool bIsPressed)
//{
//	check(InputConfig);
//	check(ASC);
//	for (const FHCInputAction& IA : InputConfig->AbilityInputActions)
//	{
//		if (IA.InputAction && IA.InputTag.IsValid())
//		{
//			if (bIsPressed)
//			{
//				ASC->AbilityLocalInputPressed(static_cast<int32>(IA.AbilityInputID));
//			}
//			else 
//			{
//				ASC->AbilityLocalInputReleased(static_cast<int32>(IA.AbilityInputID));
//			}
//		}
//	}
//}

template<class UserClass, typename PressedFuncType, typename ReleasedFuncType>
void UHCInputComponent::BindAbilityActions(const UHCInputConfig* InputConfig, UserClass* Object, PressedFuncType PressedFunc, ReleasedFuncType ReleasedFunc, TArray<uint32>& BindHandles)
{
	check(InputConfig);
	for (const FHCInputAction& IA : InputConfig->AbilityInputActions)
	{
		if (IA.InputAction && IA.InputTag.IsValid())
		{
			if (PressedFunc)
			{
				//BindHandles.Add(BindAction(IA.InputAction, ETriggerEvent::Triggered, Object, PressedFunc, IA.InputTag).GetHandle());
				BindHandles.Add(BindAction(IA.InputAction, ETriggerEvent::Started, Object, PressedFunc, IA.InputTag).GetHandle());
			}

			if (ReleasedFunc)
			{
				BindHandles.Add(BindAction(IA.InputAction, ETriggerEvent::Completed, Object, ReleasedFunc, IA.InputTag).GetHandle());
			}
		}
	}
}

//template<class UserClass, typename TriggeredFuncType>
//void UHCInputComponent::BindAbilityActions(const UHCInputConfig* InputConfig, UserClass* Object, TriggeredFuncType TriggeredFunc, TArray<uint32>& BindHandles)
//{
//	check(InputConfig);
//	for (const FHCInputAction& IA : InputConfig->AbilityInputActions)
//	{
//		if (IA.InputAction && IA.InputTag.IsValid())
//		{
//			if (TriggeredFunc)
//			{
//				BindHandles.Add(BindAction(IA.InputAction, ETriggerEvent::Triggered, Object, TriggeredFunc, IA.InputTag).GetHandle());
//			}
//		}
//	}
//}