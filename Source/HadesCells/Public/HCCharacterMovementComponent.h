// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HCCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
	UHCCharacterMovementComponent(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed")
		float SpeedMultiplier;

	virtual float GetMaxSpeed() const override;
};
