// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HCAOEBase.generated.h"

class UBoxComponent;
class UGameplayEffect;

UCLASS()
class HADESCELLS_API AHCAOEBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AHCAOEBase(const FObjectInitializer& ObjectInitializer);
public:
	UPROPERTY(EditDefaultsOnly, Category = "HC|AOE") TObjectPtr<UBoxComponent> CollisionBox;
	UPROPERTY(EditDefaultsOnly, Category = "HC|AOE") TObjectPtr<UStaticMeshComponent> AreaMesh;

protected:
	virtual void BeginPlay() override;
	void ApplyAreaEffect();
protected:
	UPROPERTY(EditDefaultsOnly, Category = "HC|AOE") TSubclassOf<UGameplayEffect> AreaEffect;
	UPROPERTY(EditDefaultsOnly, Category = "HC|AOE") float AOETickRate;

private:
	FTimerHandle AOETimerHandle;
};
