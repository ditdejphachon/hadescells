// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HCTrapBase.generated.h"

UCLASS()
class HADESCELLS_API AHCTrapBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AHCTrapBase();
	UFUNCTION() virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
public:
	UPROPERTY(EditDefaultsOnly, Category = "HC|Traps") TObjectPtr<class UBoxComponent> CollisionBox;
	UPROPERTY(EditDefaultsOnly, Category = "HC|PickUp") TObjectPtr<UStaticMeshComponent> TrapMesh;

protected:
	virtual void BeginPlay() override;
	virtual void TriggerTrap();
protected:
	UPROPERTY(EditAnywhere, Category = "HC|Traps") TObjectPtr<AActor> StartingTargetPoint;
	UPROPERTY(EditAnywhere, Category = "HC|Traps") TSubclassOf<class AHCProjectileBase> TrapSpawn;
	UPROPERTY(EditAnywhere, Category = "HC|Traps") float LaunchAngle = 0.0f;
	UPROPERTY(EditAnywhere, Category = "HC|Traps") float LaunchSpeed = 3000.0f;
	UPROPERTY(EditAnywhere, Category = "HC|Traps") float TrapDelay = 8.0f;
private:
	bool bCanTrigger;
};
