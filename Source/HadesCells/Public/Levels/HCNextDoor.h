// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Interactables/HCInteractableComponent.h"
#include "NiagaraComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HCNextDoor.generated.h"

UCLASS()
class HADESCELLS_API AHCNextDoor : public AHCInteractableComponent
{
	GENERATED_BODY()
	
public:	
	AHCNextDoor(const FObjectInitializer& ObjectInitializer);

public:
	UPROPERTY(EditDefaultsOnly, Category = "HC") TObjectPtr<UStaticMeshComponent> DoorMesh;

protected:
	virtual void BeginPlay() override;
	virtual void BeginInteraction(AHCHeroCharacter* ReceivingHero = nullptr) override;
	UFUNCTION() void OnReadyToProceed();
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult);
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC") class UParticleSystem* ParticleFX;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC") class UNiagaraSystem* NiagaraFX;

private:
	UNiagaraComponent* NiagaraComponent;
	UParticleSystemComponent* ParticleSystemComponent;
};
