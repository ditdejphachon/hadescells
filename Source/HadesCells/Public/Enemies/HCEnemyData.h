// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/HCPawnData.h"
#include "HCEnemyData.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCEnemyData : public UHCPawnData
{
	GENERATED_BODY()
		
public:
	// Ability sets to grant to this pawn's ability system.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Equipment")
	TArray<TObjectPtr<class UUHCEquipmentData>> EquipmentsToAdd;
};
