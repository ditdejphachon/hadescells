// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Characters/HCCharacterBase.h"
#include "HCEnemyBase.generated.h"

class UHCW_FloatingStatusBar;
/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCEnemyBase : public AHCCharacterBase
{
	GENERATED_BODY()
public:
	AHCEnemyBase(const FObjectInitializer& ObjectInitializer);
protected:
	// Called when the game starts or when spawned
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void BeginPlay() override;
	void InitializeFloatingStatusBar();
	virtual void HandleHealthChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator) override;
	virtual void HandleShieldChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator) override;
	virtual void HandlePostureChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator) override;
	UFUNCTION(BlueprintCallable, Category="HC|Enemy")
	virtual void ActivateAbility(const FGameplayTag& InGameplayTag);
	virtual void DropResources();
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HC|Character|Component")
		class UWidgetComponent* FloatingStatusBarComponent;
	UPROPERTY(BlueprintReadOnly, EditAnyWhere, Category = "HC|Character|Widget")
		TSubclassOf<UHCW_FloatingStatusBar> WBP_FloatingStatusBar;
	UPROPERTY(EditAnywhere, Category = "HC|Character|UI") FVector2D FloatingStatusBarSize = FVector2D(100, 8);
	UHCW_FloatingStatusBar* W_FloatingStatusBar;
	UPROPERTY(EditAnywhere, Category = "HC|Character") TSubclassOf<class AHCAutoPickUp> AutoPickUpClass;
};
