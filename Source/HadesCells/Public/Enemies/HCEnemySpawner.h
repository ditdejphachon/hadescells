// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "HCEnemyBase.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HCEnemySpawner.generated.h"

USTRUCT(BlueprintType)
struct FHCSpawnEnemyInfo
{
	GENERATED_BODY();

public:
	UPROPERTY(EditAnywhere) TSubclassOf<AHCEnemyBase> EnemyToSpawn = nullptr; // Ability to add
};


UCLASS()
class HADESCELLS_API AHCEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHCEnemySpawner();
	UFUNCTION(BlueprintCallable) void StartSpawning();

public:
	UPROPERTY(EditAnywhere, Category = "HC") FHCSpawnEnemyInfo SpawnEnemyInfos;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC") class UParticleSystem* ParticleFX;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC") class UNiagaraSystem* NiagaraFX;
protected:
	virtual void BeginPlay() override;
};
