// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "HCAIControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCAIControllerBase : public AAIController
{
	GENERATED_BODY()
public:
    AHCAIControllerBase();

    virtual void BeginPlay() override;
    virtual void OnPossess(APawn* InPawn) override;

protected:
    UPROPERTY(EditDefaultsOnly, Category = "HC|AI") UBehaviorTree* BehaviorTree;
    UBlackboardComponent* BlackboardComponent;
};
