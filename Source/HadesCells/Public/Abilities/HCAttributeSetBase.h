// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#pragma region includes
#include "AbilitySystemComponent.h"
#pragma endregion


#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "HCAttributeSetBase.generated.h"

/**
 * This macro defines a set of helper functions for accessing and initializing attributes.
 *
 * The following example of the macro:
 *		ATTRIBUTE_ACCESSORS(ULyraHealthSet, Health)
 * will create the following functions:
 *		static FGameplayAttribute GetHealthAttribute();
 *		float GetHealth() const;
 *		void SetHealth(float NewVal);
 *		void InitHealth(float NewVal);
 */
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCAttributeSetBase : public UAttributeSet
{
	GENERATED_BODY()
public:
	UHCAttributeSetBase();

	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData Health;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData MaxHealth;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData Shield;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData MaxShield;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData Posture;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData MaxPosture;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData DashCount;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData MaxDashCount;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData MoveSpeed;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData CharacterLevel;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData Experience;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData MaxExperience;
	UPROPERTY(BlueprintReadOnly) FGameplayAttributeData Gold;
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, Health)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, MaxHealth)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, Shield)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, MaxShield)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, Posture)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, MaxPosture)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, DashCount)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, MaxDashCount)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, MoveSpeed)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, CharacterLevel)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, Experience)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, MaxExperience)
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, Gold)

	// Damage is a meta attribute used by the DamageExecution to calculate final damage, which then turns into -Health
	// Temporary value that only exists on the Server. Not replicated.
	UPROPERTY(BlueprintReadOnly, Category = "Damage") FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UHCAttributeSetBase, Damage)

	// TODO: [cells, cells reward], [gold, gold reward]

protected:

	// Helper function to proportionally adjust the value of an attribute when it's associated max attribute changes.
	// (i.e. When MaxHealth increases, Health increases by an amount that maintains the same percentage as before)
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty);

public:
	/**
	* attribute manipulation functions
	*/
	// handle pre attribute changes
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	// handle post gameplay effect execution
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;


};
