// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameplayAbilityTargetActors/HCGA_TargetActor_CursorTrace.h"
#include "../Equipment/Projectile/HCProjectileBase.h"
#include "CoreMinimal.h"
#include "Abilities/HCGameplayAbility_FromEquipment.h"
#include "HCGameplayAbility_RangeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGameplayAbility_RangeWeapon : public UHCGameplayAbility_FromEquipment
{
	GENERATED_BODY()
	
public:
	UHCGameplayAbility_RangeWeapon(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	//GameplayAbility Interfaces
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	//~GameplayAbility Interfaces

	UFUNCTION(BlueprintCallable, Category = "HC|Ability|Range") FTransform DetermineSpawnTransform(const FVector& InInstigatorLocation, const FVector& InTargetLocation, const double SpawnPitch, const float InstigatorOffset = 100.0f, const float GroundOffset = 100.0f);

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "HC|Ability") TSubclassOf<AHCGA_TargetActor_CursorTrace> TargetActorClass;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "HC|Ability") TSubclassOf<AHCProjectileBase> ProjectileClass;

protected:
	virtual void ExecuteAbility() {};

protected:
	UPROPERTY(BlueprintReadWrite) FVector LookAtLocation;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "HC|Ability|Spawn") float m_SpawnPitch;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "HC|Ability|Spawn") float m_InstigatorOffset;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "HC|Ability|Spawn") float m_GroundOffset;
	FGameplayAbilitySpecHandle m_Handle;
	const FGameplayAbilityActorInfo* m_ActorInfo;
	FGameplayAbilityActivationInfo m_ActivationInfo;
};
