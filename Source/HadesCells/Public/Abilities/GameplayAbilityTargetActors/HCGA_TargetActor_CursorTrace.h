// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "HCGA_TargetActor_CursorTrace.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCGA_TargetActor_CursorTrace : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()

public:
	AHCGA_TargetActor_CursorTrace(const FObjectInitializer& ObjectInitializer);

	virtual void StartTargeting(UGameplayAbility* Ability) override;
	virtual void ConfirmTargetingAndContinue() override;
	virtual bool ShouldProduceTargetData() const;
	virtual void Tick(float DeltaSeconds) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void SetSize(const FVector& InSize) { m_Size = InSize; };
	UFUNCTION(BlueprintCallable) void SetDecalSize(class UDecalComponent* DecalComp);

private:
	UFUNCTION() void OnMouseClickConfirm(const FInputActionValue& Value);
	UFUNCTION() void OnMouseClickCancel(const FInputActionValue& Value);
	void BindToMouseClick();
	void UnbindFromMouseClick();
private:
	class UHCGameplayAbility* m_CurrentAbility;
	TArray<int32> m_BindingHandles;
	FVector m_Size;
};
