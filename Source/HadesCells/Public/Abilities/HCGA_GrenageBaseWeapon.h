// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/HCGameplayAbility_RangeWeapon.h"
#include "HCGA_GrenageBaseWeapon.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGA_GrenageBaseWeapon : public UHCGameplayAbility_RangeWeapon
{
	GENERATED_BODY()
public:
	UHCGA_GrenageBaseWeapon(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	//GameplayAbility Interfaces
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	//~GameplayAbility Interfaces

protected:
	void ExecuteAbility() override;
	UFUNCTION() 
		void OnReceiveValidTargetData(const FGameplayAbilityTargetDataHandle& Data);
	UFUNCTION() 
		void OnCancelTargeting(const FGameplayAbilityTargetDataHandle& Data);
};
