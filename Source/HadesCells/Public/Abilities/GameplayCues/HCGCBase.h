// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayCueNotify_Actor.h"
#include "HCGCBase.generated.h"

USTRUCT(BlueprintType)
struct FHCGCInfo
{
	GENERATED_BODY();

public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC|GC|Particle System") class UParticleSystem* ParticleFX;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC|GC|Particle System") class UNiagaraSystem* NiagaraFX;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC|GC|Particle System") FName AttachPointNames;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC|GC|Particle System") FVector FXScale = FVector::One();
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC|GC|Particle System") TEnumAsByte<EAttachLocation::Type>  AttachLocation;
};

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCGCBase : public AGameplayCueNotify_Actor
{
	GENERATED_BODY()

public:
	AHCGCBase();
	virtual void HandleGameplayCue(AActor* MyTarget, EGameplayCueEvent::Type EventType, const FGameplayCueParameters& Parameters) override;
public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC|GC|Particle System") TArray<FHCGCInfo> GameplayCueInfos;
protected:
	void SpawnEffects(AActor* InActor);
	void RemoveEffects();
	virtual void OnOwnerDestroyed(AActor* DestroyedActor);

private:
	TArray<class UParticleSystemComponent*> CreatedParticles;
	TArray<AActor*> RootActors;
};
