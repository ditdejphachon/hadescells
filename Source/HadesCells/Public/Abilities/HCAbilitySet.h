// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Equipment/HCEquipmentInstance.h"

#include "GameplayAbilitySpec.h"
#include "GameplayEffectTypes.h"
#include "GameplayTagContainer.h"

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "HCAbilitySet.generated.h"

class UAttributeSet;
class UGameplayEffect;
class UHCAbilitySystemComponent;
class UHCGameplayAbility;

USTRUCT(BlueprintType)
struct FHCAbilitySet_GameplayAbility
{
	GENERATED_BODY();

public:
	UPROPERTY(EditDefaultsOnly) TSubclassOf<UHCGameplayAbility> Ability = nullptr; // Ability to add
	UPROPERTY(EditDefaultsOnly) int32 AbilityLevel = 1;
	UPROPERTY(EditDefaultsOnly) FGameplayTag InputTag; // input tag for ability activation
};

USTRUCT(BlueprintType)
struct FHCAbilitySet_GameplayEffect
{
	GENERATED_BODY();

public:
	UPROPERTY(EditDefaultsOnly) TSubclassOf<UGameplayEffect> GameplayEffect = nullptr; // Ability to add
	UPROPERTY(EditDefaultsOnly) float EffectLevel = 1.0f;
};

USTRUCT(BlueprintType)
struct FHCAbilitySet_AttributeSet
{
	GENERATED_BODY();

public:
	UPROPERTY(EditDefaultsOnly) TSubclassOf<UAttributeSet> AttributeSet;
};

USTRUCT(BlueprintType)
struct FHCAbilitySet_GrantedHandles
{
	GENERATED_BODY()

	void AddAbilitySpecHandle(const FGameplayAbilitySpecHandle& Handle);
	void AddGameplayEffectHandle(const FActiveGameplayEffectHandle& Handle);
	void AddAttributeSet(UAttributeSet* AttributeSet);
	void TakeFromAbilitySystem(UHCAbilitySystemComponent* HCASC);

protected:
	UPROPERTY() TArray<FGameplayAbilitySpecHandle> AbilitySpecHandles;
	UPROPERTY() TArray<FActiveGameplayEffectHandle> GameplayEffectHandles;
	UPROPERTY() TArray<TObjectPtr<UAttributeSet>> GrantedAttributeSets;
};

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCAbilitySet : public UPrimaryDataAsset
{
	GENERATED_BODY()
	
public:
	UHCAbilitySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	void GiveToAbililtySystem(UHCAbilitySystemComponent* HCASC, FHCAbilitySet_GrantedHandles* OutGrantedHandles, EHCEquipmentSlotType SlotType, int32 AbilityLevel, UObject* SourceObject = nullptr) const;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay Ability") TArray<FHCAbilitySet_GameplayAbility> GrantedGameplayAbilities;
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay Effect") TArray<FHCAbilitySet_GameplayEffect> GrantedGameplayEffects;
	UPROPERTY(EditDefaultsOnly, Category = "Attribute Sets") TArray<FHCAbilitySet_AttributeSet> GrantedAttributes;

};
