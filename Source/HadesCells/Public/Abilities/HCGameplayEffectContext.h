// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameplayEffectTypes.h"
#include "HAL/Platform.h"
#include "UObject/Class.h"
#include "UObject/WeakObjectPtr.h"
#include "UObject/WeakObjectPtrTemplates.h"

#include "CoreMinimal.h"
#include "HCGameplayEffectContext.generated.h"

/**
 * 
 */
USTRUCT()
struct HADESCELLS_API FHCGameplayEffectContext : public FGameplayEffectContext
{
	GENERATED_BODY()

public:
	FHCGameplayEffectContext() : FGameplayEffectContext() {}
	FHCGameplayEffectContext(AActor* InInstigator, AActor* InEffectCauser) : FGameplayEffectContext(InInstigator, InEffectCauser) {}

	static FHCGameplayEffectContext* ExtractEffectContext(struct FGameplayEffectContextHandle InHandle);

	// WARNING: not doing SetAbilitySource() for now since I do not know where to use it
	
	virtual FGameplayEffectContext* Duplicate() const;
	virtual UScriptStruct* GetScriptStruct() const override;
	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;
	/** Returns the physical material from the hit result if there is one */
	const UPhysicalMaterial* GetPhysicalMaterial() const;
public:
	/** ID to allow the identification of multiple bullets that were part of the same cartridge */
	UPROPERTY() int32 CartridgeID = -1;

protected:
	/** Ability Source object (should implement ILyraAbilitySourceInterface). NOT replicated currently */
	UPROPERTY() TWeakObjectPtr<const UObject> AbilitySourceObject;
};


template<>
struct TStructOpsTypeTraits<FHCGameplayEffectContext> : public TStructOpsTypeTraitsBase2<FHCGameplayEffectContext>
{
	enum
	{
		WithNetSerializer = true,
		WithCopy = true
	};
};
