// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Ability/HCGameplayAbility.h"

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "HCAbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
public:
	UHCAbilitySystemComponent(const FObjectInitializer& ObjectInitializer);

	void AbilityInputTagPressed(const FGameplayTag& InputTag);
	void AbilityInputTagReleased(const FGameplayTag& InputTag);

	bool IsActivationGroupBlocked(EHCAbilityActivationGroup Group) const;

	// add/remove/cancel ability from activation group
	void AddAbilityToActivationGroup(EHCAbilityActivationGroup Group, UHCGameplayAbility* HCAbility);
	void RemoveAbilityToActivationGroup(EHCAbilityActivationGroup Group, UHCGameplayAbility* HCAbility);
	void CancelActivationGroupAbilities(EHCAbilityActivationGroup Group, UHCGameplayAbility* IgnoreHCAbility, bool bReplicateCancelAbility);

	typedef TFunctionRef<bool(const UHCGameplayAbility* HCAbility, FGameplayAbilitySpecHandle Handle)> TShouldCancelAbilityFunc;
	void CancelAbilitiesByFunc(TShouldCancelAbilityFunc ShouldCancelFunc, bool bReplicatedCancelAbility);

	void GetAbilityTargetData(const FGameplayAbilitySpecHandle AbilityHandle, FGameplayAbilityActivationInfo ActivationInfo, FGameplayAbilityTargetDataHandle& OutTargetDataHandle);

	// process the current input to determine which abilities to call
	void ProcessAbilityInput();

	UFUNCTION(BlueprintCallable) bool MyHasAnyGameplayTags(const FGameplayTag& InTag);
	UFUNCTION(BlueprintCallable) void MyAddLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count = 1);
	
public:
	bool bCharacterAbilitiesGiven = false;
	bool bStartupEffectsApplied = false;

protected:
	virtual void AbilitySpecInputPressed(FGameplayAbilitySpec& Spec) override;
	virtual void AbilitySpecInputReleased(FGameplayAbilitySpec& Spec) override;

protected:

	// Handles to abilities that had their input pressed this frame.
	TArray<FGameplayAbilitySpecHandle> InputPressedSpecHandles;

	// Handles to abilities that had their input released this frame.
	TArray<FGameplayAbilitySpecHandle> InputReleasedSpecHandles;

	// Handles to abilities that have their input held.
	TArray<FGameplayAbilitySpecHandle> InputHeldSpecHandles;

	// Number of abilities running in each activation group.
	int32 ActivationGroupCounts[(uint8)EHCAbilityActivationGroup::MAX];

private:
	void ClearAbilityInput();
};
