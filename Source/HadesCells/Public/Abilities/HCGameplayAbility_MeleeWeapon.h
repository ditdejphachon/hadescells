// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/HCGameplayAbility_FromEquipment.h"
#include "HCGameplayAbility_MeleeWeapon.generated.h"

class UHCWeaponInstance;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGameplayAbility_MeleeWeapon : public UHCGameplayAbility_FromEquipment
{
	GENERATED_BODY()
public:
	UHCGameplayAbility_MeleeWeapon();

	UFUNCTION(BlueprintCallable, Category = "HC|Ability") UHCWeaponInstance* GetWeaponInstance() const;

	// GameplayAbility Interfaces
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags = nullptr, const FGameplayTagContainer* TargetTags = nullptr, OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	// ~GameplayAbility Interfaces

	UFUNCTION() void ExecuteEndAbility(FGameplayTag EventTag, FGameplayEventData EventData);

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability|Montage") class UAnimMontage* MeleeAnimMontage;

protected:
	void PlayAttackMontage();
	UFUNCTION() void OnMontageComplete(FGameplayTag EventTag, FGameplayEventData EventData);
	UFUNCTION() void OnMontageBlendOut(FGameplayTag EventTag, FGameplayEventData EventData);
	UFUNCTION() void OnMontageInterruped(FGameplayTag EventTag, FGameplayEventData EventData);
	UFUNCTION() void OnMontageCancelled(FGameplayTag EventTag, FGameplayEventData EventData);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability|Montage") float MontagePlaybackRate = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability|Montage") bool bStopWhenAbilityEnds = false;

	
};
