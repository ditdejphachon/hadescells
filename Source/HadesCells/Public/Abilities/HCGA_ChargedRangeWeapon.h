// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/HCGameplayAbility_RangeWeapon.h"
#include "HCGA_ChargedRangeWeapon.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGA_ChargedRangeWeapon : public UHCGameplayAbility_RangeWeapon
{
	GENERATED_BODY()
	
public:
	UHCGA_ChargedRangeWeapon(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

protected:
	UFUNCTION(BlueprintCallable) bool IsCharged(const float& TimeHeld) { return TimeHeld >= AllowableChargeThreshold; }
	void ExecuteAbility() override;
	UFUNCTION() void OnInputReleased(float TimeHeld);
	UFUNCTION() void OnMontageEvent();
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability") UAnimMontage* OnReleasedMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability") float AllowableChargeThreshold = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability") FHCGCInfo ChargingEffectInfo;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability") FHCGCInfo ChargedEffectInfo;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability") float ProjectileSpeed = 6000.0f;
	
private:
	AHCGA_TargetActor_CursorTrace* SpawnedTargetActor;
	class UNiagaraComponent* ChargingPS;
	class UNiagaraComponent* ChargedPS;
	FTimerHandle ChargingHandle;
};
