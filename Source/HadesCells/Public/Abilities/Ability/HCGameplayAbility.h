// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "HCGameplayAbility.generated.h"

class UHCAbilitySystemComponent;
class AHCCharacterBase;
/*
 *
 *	Defines how an ability is meant to activate.
 */
UENUM(BlueprintType)
enum class EHCAbilityActivationPolicy : uint8
{
	// Try to activate the ability when the input is triggered.
	OnInputTriggered,

	// Continually try to activate the ability while the input is active.
	WhileInputActive,

	// Try to activate the ability when an avatar is assigned.
	OnSpawn
};

/*
 *
 *	Defines how an ability activates in relation to other abilities.
 */
UENUM(BlueprintType)
enum class EHCAbilityActivationGroup : uint8
{
	// Ability runs independently of all other abilities.
	Independent,

	// Ability is canceled and replaced by other exclusive abilities.
	Exclusive_Replaceable,

	// Ability blocks all other exclusive abilities from activating.
	Exclusive_Blocking,

	MAX	UMETA(Hidden)
};

/**
 * 
 */
UCLASS(Abstract, HideCategories = Input, Meta = (ShortTooltip = "The base gameplay ability class used by this project."))
class HADESCELLS_API UHCGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UHCGameplayAbility(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintCallable, Category = "HC|Ability")
		UHCAbilitySystemComponent* GetHCAbilitySystemComponentFromActorInfo() const;

	// TODO: player controller getter
	// TODO: controller getter

	UFUNCTION(BlueprintCallable, Category = "HC|Ability")
		AHCCharacterBase* GetHCCharacterFromActorInfo() const;

	EHCAbilityActivationPolicy GetActivationPolicy() const { return ActivationPolicy; }
	EHCAbilityActivationGroup GetActivationGroup() const { return ActivationGroup; }

	void TryActivateAbilityOnSpawn(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) const;

	// Returns true if the requested activation group is a valid transaction
	UFUNCTION(BlueprintCallable, BlueprintPure = false, Category = "HC|Ability", Meta = (ExpandBoolAsExecs = "ReturnValue"))
		bool CanChangeActivationGroup(EHCAbilityActivationGroup NewGroup) const;

	// Tries to change the activation group.  Returns true if it successfully changed.
	UFUNCTION(BlueprintCallable, BlueprintPure = false, Category = "HC|Ability", Meta = (ExpandBoolAsExecs = "ReturnValue"))
		bool ChangeActivationGroup(EHCAbilityActivationGroup NewGroup);

	void SetBlockAbilityWithTags(const FGameplayTagContainer& InTagsToBlock);

	UFUNCTION(BlueprintCallable, Category = "HC|Ability") const FGameplayTagContainer GetAbilityTags() const { return AbilityTags; };
public:
	FGameplayTagContainer InputActivationGameplayTags;
	FGameplayTagContainer AdditionalBlockingTags;

protected:
	// NOTE: skipping on failed methods for now
#pragma region UGameplayAbility Interface
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual void SetCanBeCanceled(bool bCanBeCanceled) override;
	virtual void OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
	virtual void OnRemoveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	virtual bool CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const override;
	virtual void ApplyCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const override;
	virtual FGameplayEffectContextHandle MakeEffectContext(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const override;

	// NOTE: not doing ApplyAbilityTagsToGameplayEffectSpec since I do not know what it does
	virtual bool DoesAbilitySatisfyTagRequirements(const UAbilitySystemComponent& AbilitySystemComponent, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const override;

	virtual void OnPawnAvatarSet();

#pragma endregion UGameplayAbility Interface

	UFUNCTION(BlueprintImplementableEvent, Category = "HC|Ability") void K2_OnAbilityAdded();
	UFUNCTION(BlueprintImplementableEvent, Category = "HC|Ability") void K2_OnAbilityRemoved();
	UFUNCTION(BlueprintImplementableEvent, Category = "HC|Ability") void K2_OnPawnAvatarSet();
	UFUNCTION(BlueprintImplementableEvent, Category = "HC|Ability") void K2_OnAbilityActivate();
	UFUNCTION(BlueprintImplementableEvent, Category = "HC|Ability") void K2_OnAbilityEnded();


protected:
	// Defines how this ability is meant to activate.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability Activation")
		EHCAbilityActivationPolicy ActivationPolicy;

	// Defines the relationship between this ability activating and other abilities activating.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability Activation")
		EHCAbilityActivationGroup ActivationGroup;

	// Additional cost that must be paid to activate the ability
	UPROPERTY(EditDefaultsOnly, Instanced)
		TArray<TObjectPtr<class UHCAbilityCost>> AdditionalCosts;
};
