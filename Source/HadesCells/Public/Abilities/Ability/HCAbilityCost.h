// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "HCGameplayAbility.h"
#include "GameplayAbilitySpec.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HCAbilityCost.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCAbilityCost : public UObject
{
	GENERATED_BODY()
	
public:
	UHCAbilityCost() {};

	/*
	* @return true if we can pay for the ability cost.
	*/
	virtual bool CheckCost(const UHCGameplayAbility* Ability, const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, FGameplayTagContainer* OptionalRelevantTags) const 
	{
		return true;
	};

	/*
	* Apply ability's cost to the target
	*/
	virtual void ApplyCost(const UHCGameplayAbility* Ability, const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) {};

	bool ShouldOnlyApplyCostOnHit() const { return bOnlyApplyCostOnHit; }
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HC|Costs")
		bool bOnlyApplyCostOnHit = false;
};
