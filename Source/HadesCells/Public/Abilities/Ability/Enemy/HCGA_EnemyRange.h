// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/HCGameplayAbility_RangeWeapon.h"
#include "HCGA_EnemyRange.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGA_EnemyRange : public UHCGameplayAbility_RangeWeapon
{
	GENERATED_BODY()
public:
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
protected:
	UPROPERTY(EditDefaultsOnly, Category = "HC|Ability|Spawn") float TargetActorLifeSpan = 1.0f;
};
