// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/HCGameplayAbility_MeleeWeapon.h"
#include "HCGA_EnemyMelee.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGA_EnemyMelee : public UHCGameplayAbility_MeleeWeapon
{
	GENERATED_BODY()
	
public:
	void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;

protected:
	void PlayPreAttackMontage();
	UFUNCTION() void OnPreAttackMontageBlendOut(FGameplayTag EventTag, FGameplayEventData EventData);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability|Montage") class UAnimMontage* PreAttackAnimMontage;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability|Montage") float PreAttackMontagePlaybackRate = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability|Montage") bool bPreAttackStopWhenAbilityEnds = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability") FGameplayTagContainer AdditionalGameplayTags;
};
