// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "HCAT_PlayMontageAndWaitForEvents.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHCPlayMontageAndWaitForEventsDelegate, FGameplayTag, EventTag /* EventTag */, FGameplayEventData, EventData/* Event Data*/);

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCAT_PlayMontageAndWaitForEvents : public UAbilityTask
{
	GENERATED_BODY()
public:
	UHCAT_PlayMontageAndWaitForEvents(const FObjectInitializer& ObjectInitializer);

	/**
		this ability task has some black magic from the plugin that automagically calls Activate().
		Inside of K2Node_LatentAbilityCall as stated in the AbilityTask.h.
		Ability logic written in C++ probably needs to call Activate() itself manually.
		Activate() is called to trigger the actual task once the delegates have been set up.
		Note that the default implementation does nothing and you don't have to call it.
	*/
	virtual void Activate() override;
	virtual void ExternalCancel() override;
	virtual void OnDestroy(bool AbilityEnded) override;

	UPROPERTY(BlueprintAssignable) FHCPlayMontageAndWaitForEventsDelegate OnComplete;
	UPROPERTY(BlueprintAssignable) FHCPlayMontageAndWaitForEventsDelegate OnBlendOut;
	UPROPERTY(BlueprintAssignable) FHCPlayMontageAndWaitForEventsDelegate OnInterrupted;
	UPROPERTY(BlueprintAssignable) FHCPlayMontageAndWaitForEventsDelegate OnCancelled;
	UPROPERTY(BlueprintAssignable) FHCPlayMontageAndWaitForEventsDelegate EventReceived;

	/**
	 Play a montage and wait for it end.
	 If a gameplay event happens that matches EventTags (or EventTags is empty),
	 the EventReceived delegate will fire with a tag and event data.
	 If bStopWhenAbilityEnds is true, this montage will be aborted if the ability ends normally.
	 It is always stopped when the ability is explicitly cancelled.
	 On normal execution, OnBlendOut is called when the montage is blending out,
	 and OnCompleted when it is completely done playing.
	 OnInterrupted is called if another montage overwrites this,
	 and OnCancelled is called if the ability or task is cancelled.

	 @param TaskInstanceName Set to override the name of this task, for later querying.
	 @param MontageToPlay The montage to play on the character.
	 @param EventTags Any gameplay events matching this tag will activate the EventReceived callback.
		If empty, all events wil trigger callback.
	 @param Rate Change to play the montage faster or slower
	 @param bStopWhenAbilityEnds If true, this montage will be aborted if the ability ends normally.
		It is always stopped whenthe ability is explicitly cancelled.
	 @param AnimRootMotionTranslationScale Change to modify size of root motion or set to 0 to block it entirely
	 */
	UFUNCTION(BlueprintCallable, Category = "HC|Ability|Task", meta=(HidePin = "InOwingAbility", DefaultToSelf = "InOwingAbility")) // TODO: add meta tag
		static UHCAT_PlayMontageAndWaitForEvents* PlayMontageAndWaitForEvent(
			UGameplayAbility* InOwingAbility,
			FName InTaskInstanceName,
			UAnimMontage* InMontageToPlay,
			FGameplayTagContainer InEventTags,
			float InRate,
			FName InStartSection,
			bool bStopWhenAbilityEnds = true,
			float InAnimRootMotionTranslationScale = 1.0f
		);
private:
	UPROPERTY() UAnimMontage* MontageToPlay;
	UPROPERTY() FGameplayTagContainer EventTags;
	UPROPERTY() float Rate;
	UPROPERTY() FName StartSection;
	UPROPERTY() float AnimRootMotionTranslationScale;
	UPROPERTY() bool bStopWhenAbilityEnds;

	bool StopPlayingMontage();
	class UHCAbilitySystemComponent* GetTargetASC();

	void OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted);
	void OnAbilityCancelled();
	void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);
	void OnGameplayEvent(FGameplayTag EventTag, const FGameplayEventData* Payload);

	FOnMontageBlendingOutStarted BlendingOutDelegate;
	FOnMontageEnded MontageEndedDelegate;
	FDelegateHandle CancelledHandle;
	FDelegateHandle EventHandle;
};
