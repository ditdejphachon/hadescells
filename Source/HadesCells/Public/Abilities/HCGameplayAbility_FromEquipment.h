// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Ability/HCGameplayAbility.h"
#include "HCGameplayAbility_FromEquipment.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGameplayAbility_FromEquipment : public UHCGameplayAbility
{
	GENERATED_BODY()
	
public:
	UHCGameplayAbility_FromEquipment(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintCallable, Category = "HC|Ability") class UHCEquipmentInstance* GetAssociatedEquipment() const;
	// TODO: make a getter from associated item
		// NOTE: This is for inventory staffs. Let's wait a bit
	// TODO: check is data valid
#if WITH_EDITOR
	virtual EDataValidationResult IsDataValid(TArray<FText>& ValidationErrors) override;
#endif

public:
	UFUNCTION(BlueprintPure) FVector GetLookAtHeroDirection();
	UFUNCTION(BlueprintPure) FRotator GetLookAtHeroRotator();
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Ability") UAnimMontage* CastMontage;
};
