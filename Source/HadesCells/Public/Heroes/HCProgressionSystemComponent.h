// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HCProgressionSystemComponent.generated.h"

class UHCAttributeSetBase;
class UHCAbilitySystemComponent;
struct FGameplayEffectSpec;
struct FOnAttributeChangeData;

DECLARE_MULTICAST_DELEGATE_FourParams(FHCGold_AttributeChanged, UHCProgressionSystemComponent* /* ProgressionSystemComponent */, float /*OldValue*/, float /* NewValue */, AActor* /* Instigator */);
DECLARE_MULTICAST_DELEGATE_FourParams(FHCExp_AttributeChanged, UHCProgressionSystemComponent* /* ProgressionSystemComponent */, float /*OldValue*/, float /* NewValue */, AActor* /* Instigator */);
DECLARE_MULTICAST_DELEGATE_OneParam(FHCExpFull_AttributeChanged, UHCProgressionSystemComponent* /* ProgressionSystemComponent */);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HADESCELLS_API UHCProgressionSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHCProgressionSystemComponent();
	void InitializeWithAbilitySystem(UHCAbilitySystemComponent* InASC);
	void UninitializeFromAbilitySystem();
	float GetExp() const;
	float GetMaxExp() const;
	float GetExpNormalized() const;
	float GetGold() const;
	float GetLeftOverExp() const { return m_LeftOverExp; }
public:
	FHCGold_AttributeChanged  OnGoldChanged;
	FHCExp_AttributeChanged  OnExpChanged;
	FHCExp_AttributeChanged  OnMaxExpChanged;
	FHCExpFull_AttributeChanged OnExpFull;

protected:
	virtual void HandleGoldChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleExpChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleMaxExpChanged(const FOnAttributeChangeData& ChangeData);

protected:
	UPROPERTY() TObjectPtr<UHCAbilitySystemComponent> AbilitySystemComponent;
	UPROPERTY() TObjectPtr<const UHCAttributeSetBase> AttributeSet;
private:
	float m_LeftOverExp = 0.0f;
		
};
