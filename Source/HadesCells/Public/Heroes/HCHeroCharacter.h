// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "InputActionValue.h"
#include "GameplayTagContainer.h"

#include "CoreMinimal.h"
#include "Characters/HCCharacterBase.h"
#include "HCHeroCharacter.generated.h"

class UInputComponent;
/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCHeroCharacter : public AHCCharacterBase
{
	GENERATED_BODY()

public:
	AHCHeroCharacter(const class FObjectInitializer& ObjectInitializer);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual FVector LookAtCursor() override;
	UFUNCTION(BlueprintCallable, Category = "HC|HCCharacter|Attributes") float GetDashCount() const;
	UFUNCTION(BlueprintCallable, Category = "HC|HCCharacter|Attributes") float GetMaxDashCount() const;
	UFUNCTION(BlueprintCallable, Category = "HC|HCCharacter|Attributes") void BindConfirmCancelToASC();
	TObjectPtr<UHCPawnData> GetPawnData() const { return PawnData; }
	UFUNCTION(BlueprintCallable, Category = "HC|HCCharacter|Camera") void PlayCameraShake();
	UFUNCTION(BlueprintImplementableEvent) void K2_OnPlayerDied();
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Camera|Shake") TSubclassOf<class UCameraShakeBase> CameraShake;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Camera|Shake") FVector Epicenter;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Camera|Shake") float InnerRadius; 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Camera|Shake") float OuterRadius;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Camera|Shake") float Falloff;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Camera|Shake") bool bOrientShakeTowardsEpicenter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Camera") float CameraDistance;
	UPROPERTY(BlueprintReadOnly, Category = "HC|Control") FVector HeroWorldDirection;
	UPROPERTY(BlueprintReadOnly, Category = "HC|Control") FVector CursorWorldLocation;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character|Component") float LookAtRayCastLength = 5000.0f;
	class AHCInteractableComponent* CurrentInteractableComponent;
	UPROPERTY(BlueprintReadOnly, Category = "HC|Control") FVector LookAtLocation;
	UPROPERTY(BlueprintReadWrite, Category = "HC|Control") bool bAllowOnTickLookAtCursor;

protected:
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void HandleHealthChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator) override;
	virtual void HandleShieldChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator) override;
	virtual void HandleGoldChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator) override;
	virtual void HandleExpChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator) override;
	virtual void HandleExpFull(UHCProgressionSystemComponent* InProgressionComponent);
	void HandleDashCountChanged(const FOnAttributeChangeData& ChangeData);
	void HandleMaxDashCountChanged(const FOnAttributeChangeData& ChangeData);


	/** Called for movement input */
	void Move(const FInputActionValue& Value);
	void Interact(const FInputActionValue& Value);
	void Input_AbilityInputTagPressed(FGameplayTag InputTag);
	void Input_AbilityInputTagReleased(FGameplayTag InputTag);
	//void Input_AbilityInputTagTriggered(const FInputActionInstance& InputActionInstance, FGameplayTag InputTag);

	void Die() override;
	void FinishDying() override;
	void OnActiveGameplayEffectAddedCallback(UAbilitySystemComponent* Target, const FGameplayEffectSpec& SpecApplied, FActiveGameplayEffectHandle ActiveHandle);
	void HandleItemCooldown(const FGameplayTag CallbackTag);
protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "HC|Camera") class USpringArmComponent* CameraBoom;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "HC|Camera") class UCameraComponent* FollowCamera;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HC|Camera") float FollowCameraFOV = 80.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HC|Camera") float bsFactor_1 = 0.5f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HC|Camera") float bsFactor_2 = 0.5f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HC|Camera") float bsFactor_3 = 0.5f;


	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "HC|Input") class UInputMappingContext* DefaultInputMappingContext;

private:
	bool bHasBindedConfirmCancelToASC;
	FTimerHandle DeathHandle;
};
