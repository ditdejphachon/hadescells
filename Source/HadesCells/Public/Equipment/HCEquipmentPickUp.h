// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactables/HCInteractableComponent.h"
#include "HCEquipmentPickUp.generated.h"



UCLASS()
class HADESCELLS_API AHCEquipmentPickUp : public AHCInteractableComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHCEquipmentPickUp(const FObjectInitializer& ObjectInitializer);
	virtual void Tick(float DeltaTime) override;
	void OnConstruction(const FTransform& Transform) override;
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult) override;
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
	bool GiveEquipment(AHCHeroCharacter* ReceivingHero);
	virtual void BeginInteraction(AHCHeroCharacter* ReceivingHero = nullptr);
	void ReplaceEquipmentData(UUHCEquipmentData* NewED);
public:
	UPROPERTY(EditDefaultsOnly,  Category = "HC|PickUp") TObjectPtr<UStaticMeshComponent> SpawnerMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|PickUp") TObjectPtr<UStaticMeshComponent> EquipmentDisplayMesh;
protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void PlayPickUpEffects();
	void SetDisplayMesh();

protected:
	UPROPERTY(EditDefaultsOnly, Category = "HC|PickUp") TObjectPtr<UUHCEquipmentData> EquipmentData;
	UPROPERTY(EditDefaultsOnly, Category = "HC|PickUp") float Amplitude = 1.0f; 
	UPROPERTY(EditDefaultsOnly, Category = "HC|PickUp") float Period = 5.0f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|PickUp") float EquipmentMeshRotationSpeed = 20.0f;
	float TotalTime = 0.0f;
};
