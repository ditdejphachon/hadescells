// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/Projectile/HCThrowableProjectileBase.h"
#include "HCImpactProjectileBase.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCImpactProjectileBase : public AHCThrowableProjectileBase
{
	GENERATED_BODY()
public:
	AHCImpactProjectileBase(const FObjectInitializer& ObjectInitializer);
	// Actor interfaces
	virtual void BeginPlay() override;
	//~Actor interfaces

	UFUNCTION() virtual void OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
protected:
	virtual void OnParticleSystemFinished(UParticleSystemComponent* FinishedComponent) override;


};
