// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/Projectile/HCThrowableProjectileBase.h"
#include "HCDeployableTrapBase.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCDeployableTrapBase : public AHCThrowableProjectileBase
{
	GENERATED_BODY()
public:
	AHCDeployableTrapBase(const FObjectInitializer& ObjectInitializer);
	// Actor interfaces
	virtual void BeginPlay() override;
	//~Actor interfaces
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HC|Projectile|Components") UBoxComponent* BoxCollider;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HC|Projectile") bool bShouldSpawnActors;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HC|Projectile") bool bShouldHitOnce = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HC|Projectile") float TrapLifeSpan;

protected:
	UFUNCTION() virtual void OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION() virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION() void OnEndOfLifeSpan();
	virtual void PostTrapActivation(AActor* InActor);
private:
	FTimerHandle LifeSpanTimerHandle;
	bool bHit;
};
