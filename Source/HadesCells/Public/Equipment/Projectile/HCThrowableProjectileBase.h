// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/Projectile/HCProjectileBase.h"
#include "HCThrowableProjectileBase.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCThrowableProjectileBase : public AHCProjectileBase
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AHCThrowableProjectileBase(const FObjectInitializer& ObjectInitializer);

protected:
	UFUNCTION() virtual void Detonate();
};
