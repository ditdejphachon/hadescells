// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/Projectile/HCThrowableProjectileBase.h"
#include "HCTimerProjectileBase.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCTimerProjectileBase : public AHCThrowableProjectileBase
{
	GENERATED_BODY()

public:
	AHCTimerProjectileBase(const FObjectInitializer& ObjectInitializer);

	// Actor interfaces
	virtual void BeginPlay() override;
	//~Actor interfaces

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "HC|Projectile|Timer") float TimeTilDetonate;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") int32 FlashingStencilValue = 2;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float FlashingDelay = 0.1f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float BsFactor_1 = 0.1f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float BsFactor_2 = 0.1f;

protected:
	UFUNCTION() void PreDetonate();
private:
	UFUNCTION() void StartFlashing();
private:
	FTimerHandle TimeTilDetonateHandle;
	FTimerHandle FlashingHandle;

	bool bIsFlashing = true;
};
