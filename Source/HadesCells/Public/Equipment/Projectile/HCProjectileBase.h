// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include <Characters/HCCharacterBase.h>
#include <Abilities/GameplayCues/HCGCBase.h>
#include "Particles/ParticleSystemComponent.h"
#include "AbilitySystemComponent.h"

#include "HCProjectileBase.generated.h"

USTRUCT(BlueprintType)
struct FHCProjectileExtraActorInfo
{
	GENERATED_BODY()
	FHCProjectileExtraActorInfo() {}
	UPROPERTY(EditAnywhere, Category = "HC|Projectile") TSubclassOf<AActor> ActorToSpawn;
	UPROPERTY(EditAnywhere, Category = "HC|Projectile") uint8 NumberToSpawn;
	UPROPERTY(EditAnywhere, Category = "HC|Projectile") float SpawnRadius;

};

class UBoxComponent;

UCLASS()
class HADESCELLS_API AHCProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHCProjectileBase(const FObjectInitializer& ObjectInitializer);

	// Actor interfaces
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform) override;
	//~Actor interfaces
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components") UStaticMeshComponent* ProjectileMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components") class UParticleSystemComponent* PSComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components") class UNiagaraComponent* NSComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components") UProjectileMovementComponent* ProjectileMovementComponent;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "HC|Projectile|Damage") TArray<TSubclassOf<UGameplayEffect>> DamageEffects;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "HC|Projectile|Particle System") TArray<FHCGCInfo> GameplayCueInfos;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Projectile") float SphereTraceRadius;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Projectile") float TraceEndMultiplier;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HC|Projectile") bool bIsSelfDamage;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Projectile") bool bIsDebug;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HC|Projectile", meta = (ExposeOnSpawn = "true")) float CalculatedInitialSpeed = 0.0f;
protected:
	UFUNCTION() virtual void OnParticleSystemFinished(UParticleSystemComponent* FinishedComponent) { 
		Destroy();
	};
	
	virtual void ApplyGameEffectToTarget(UAbilitySystemComponent* InASC, ACharacter* InActor);
	virtual void SpawnParticleSystems(bool bDestroySelf = true);
	virtual void SpawnExtraActors();
	void KnockActorBack(AActor* InActor, FVector InDirection = FVector::ZeroVector);
	UFUNCTION(BlueprintImplementableEvent) void K2_OnHit();
	void DestroyParticleSystems();

protected:
	TArray<AActor*> AppliedActors;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Projectile") TArray<FHCProjectileExtraActorInfo> ActorToSpawnInfos;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Projectile") float KnockBackDistance;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Projectile") float KnockBackSpeed = 3000.0f;
};
