// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HCEquipmentInstance.generated.h"

UENUM(BlueprintType)
enum class EHCEquipmentType : uint8
{
	None,
	Weapon,
	Skill,
	Buff
};

UENUM(BlueprintType)
enum class EHCEquipmentSlotType : uint8
{
	None,
	MainWeapon,
	SecondaryWeapon,
	MainSkill,
	SecondarySkill
};

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class HADESCELLS_API UHCEquipmentInstance : public UObject
{
	GENERATED_BODY()

public:
	UHCEquipmentInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	// UObject interface
	virtual UWorld* GetWorld() const override final;
	// end UObject interface}

	// getter/setter for the instigator
	UFUNCTION(BlueprintPure, Category = "HC|Equipment")
		UObject* GetInstigator() const { return Instigator; }
	void SetInstigator(UObject* InInstigator) { Instigator = Instigator; }

	// pawn functions
	UFUNCTION(BlueprintPure, Category = "HC|Equipment")
		APawn* GetPawn() const;
	UFUNCTION(BlueprintPure, Category = "HC|Equipment", meta = (DeterminesOutputType = PawnType))
		APawn* GetTypedPawn(TSubclassOf<APawn> PawnType) const;
	UFUNCTION(BlueprintPure, Category = "HC|Equipment")
		TArray<AActor*> GetSpawnedActor() const { return SpawnedActors; }

	virtual void SpawnEquipmentActors(const TArray<struct FHCEquipmentActorToSpawn>& ActorToSpawn);
	virtual void DestroyEquipmentActors();

	virtual void OnEquipped();
	virtual void OnUnEquipped();
public:
	UPROPERTY(EditDefaultsOnly, Category = "HC|Equipment") EHCEquipmentType EquipmentType;

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "HC|Equipment") void K2_OnEquipped();
	UFUNCTION(BlueprintImplementableEvent, Category = "HC|Equipment") void K2_OnUnEquipped();

private:
	UPROPERTY() TObjectPtr<UObject> Instigator;
	UPROPERTY() TArray<TObjectPtr<AActor>> SpawnedActors;
};