// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameplayTagContainer.h"
#include "GameplayEffectTypes.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HCWeaponBase.generated.h"

class UBoxComponent;
UCLASS()
class HADESCELLS_API AHCWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHCWeaponBase();
	// Actor interfaces
	virtual void BeginPlay() override;
	//~Actor interfaces

	void SetAttacking();
	void SetEndAttacking();
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components") UStaticMeshComponent* MeshComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components") UStaticMeshComponent* WeaponMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components") UBoxComponent* BoxCollider;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "HC|Weapon|Damage") TArray<TSubclassOf<class UGameplayEffect>> DamageEffects;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HC|Weapon") float KnockBackDistance = 100.0f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Weapon") float KnockBackSpeed = 3000.0f;

	bool bIsAttacking = false;
	bool bIsReadyForExec = true;
	class AHCCharacterBase* InstigatorActor;
protected:
	UFUNCTION() void StartAttackExec();
	UFUNCTION() virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION() virtual void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	FTimerHandle CollisionHandle;
	FGameplayEffectSpecHandle DamageEffectSpecHandle;
	TArray<AActor*> AppliedActors;
	
};
