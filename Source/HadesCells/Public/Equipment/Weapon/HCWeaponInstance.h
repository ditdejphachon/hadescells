// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/HCEquipmentInstance.h"
#include "HCWeaponInstance.generated.h"

class FAnimInstance;
class UAnimMontage;
/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCWeaponInstance : public UHCEquipmentInstance
{
	GENERATED_BODY()
	
public:
	UHCWeaponInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	//UHCEquipmentInstance interface
	virtual void OnEquipped() override;
	virtual void OnUnEquipped() override;
	//~UHCEquipmentInstance interface
protected:
	// TODO: recheck later to see if we need to make a wrapper the animation
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HC|Equipment|Weapon|Animation") TSubclassOf<FAnimInstance> OnEquippedAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="HC|Equipment|Weapon|Animation") TSubclassOf<FAnimInstance> OnUnequippedAnim;*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HC|Equipment|Weapon|Animation") UAnimMontage* OnEquippedAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HC|Equipment|Weapon|Animation") UAnimMontage* OnUnEquippedAnim;
};
