// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Equipment/Weapon/HCWeaponInstance.h"
#include "HCRangeWeaponInstance.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCRangeWeaponInstance : public UHCWeaponInstance
{
	GENERATED_BODY()
	
};
