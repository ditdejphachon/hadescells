// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HCEquipmentDefinition.generated.h"

USTRUCT()
struct FHCEquipmentActorToSpawn
{
	GENERATED_BODY()

	FHCEquipmentActorToSpawn() {}

	UPROPERTY(EditAnywhere, Category = "HC|Equipment") TSubclassOf<AActor> ActorToSpawn;
	UPROPERTY(EditAnywhere, Category = "HC|Equipment") FName AttachSocket;
	UPROPERTY(EditAnywhere, Category = "HC|Equipment") FTransform AttachTransform;
};
/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class HADESCELLS_API UHCEquipmentDefinition : public UObject
{
	GENERATED_BODY()
	
public:
	UHCEquipmentDefinition(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UPROPERTY(EditDefaultsOnly, Category = "HC|Equipment") TSubclassOf<class UHCEquipmentInstance> InstanceType;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Equipment") TArray<TObjectPtr<const class UHCAbilitySet>> AbilitySetsToGrant;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Equipment") TArray<FHCEquipmentActorToSpawn> ActorsToSpawn;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Equipment") int32 AbilityLevel;
};
