// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "UHCEquipmentData.generated.h"

class UHCEquipmentDefinition;
class UStaticMesh;
class UNiagaraSystem;

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class HADESCELLS_API UUHCEquipmentData : public UPrimaryDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TSubclassOf<UHCEquipmentDefinition> EquipmentDefinition;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TObjectPtr<UStaticMesh> DisplayMesh;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) FVector DisplayMeshOffset;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) FVector DisplayMeshScale = FVector(1.0f, 1.0f, 1.0f);
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TObjectPtr<UTexture2D> DisplayIcon;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) FText Name;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) FText Description;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TArray<FText> UpgradeDescriptions;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) int32 MaxLevel;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TObjectPtr<USoundBase> PickedUpSound;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TObjectPtr<UNiagaraSystem> PickedUpNiagaraEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) TObjectPtr<UParticleSystem> PickedUpParticleEffect;

	FPrimaryAssetId GetPrimaryAssetId() const override
	{
		return FPrimaryAssetId("Equipments", GetFName());
	}
};
