// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Abilities/HCAbilitySet.h"
#include "Equipment/HCEquipmentInstance.h"

#include "CoreMinimal.h"
#include "Components/PawnComponent.h"
#include "HCEquipmentManagerComponent.generated.h"

class UUHCEquipmentData;
class UActorComponent;
class UHCAbilitySystemComponent;
class UHCEquipmentManagerComponent;
class UHCEquipmentDefinition;
class UHCEquipmentInstance;
struct FHCEquipmentList;

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnEquipmentAdded, UUHCEquipmentData*, EHCEquipmentSlotType);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnEquipmentRemoved, EHCEquipmentSlotType);
DECLARE_MULTICAST_DELEGATE_TwoParams(FHCEquipmentSlotFull, UUHCEquipmentData*, EHCEquipmentType);

USTRUCT(BlueprintType)
struct FHCAppliedEquipmentEntry
{
	GENERATED_BODY()

	UPROPERTY() TSubclassOf<UHCEquipmentDefinition> EquipmentDefinition;
	UPROPERTY() TObjectPtr<UHCEquipmentInstance> Instance = nullptr;
	UPROPERTY() FHCAbilitySet_GrantedHandles GrantedHandles;

	FHCAppliedEquipmentEntry() {}
	FString GetDebugString() const;
	bool IsValid() const { return Instance != nullptr; }
	void Clear()
	{
		EquipmentDefinition = nullptr;
		Instance = nullptr;
	}

private:
	friend FHCEquipmentList;
	friend UHCEquipmentManagerComponent;
};

USTRUCT(BlueprintType)
struct FHCEquipmentList
{
	GENERATED_BODY()

	UPROPERTY() FHCAppliedEquipmentEntry MainWeapon;
	UPROPERTY() FHCAppliedEquipmentEntry SecondaryWeapon;
	UPROPERTY() FHCAppliedEquipmentEntry MainSkill;
	UPROPERTY() FHCAppliedEquipmentEntry SecondarySkill;

	FHCEquipmentList(): OwnerComponent(nullptr) {}
	FHCEquipmentList(UActorComponent* InOwnerComponent) : OwnerComponent(InOwnerComponent) {}

public:

	UHCEquipmentInstance* AddEntry(TObjectPtr<UUHCEquipmentData> EquipmentData, UHCEquipmentManagerComponent* InEquipmentManagerComponent);
	void RemoveEntry(UHCEquipmentInstance* InInstanceToRemove, UHCEquipmentManagerComponent* InEquipmentManagerComponent, EHCEquipmentSlotType SlotType);

private:
	UHCAbilitySystemComponent* GetAbilitySystemComponent() const;
private:
	UPROPERTY(NotReplicated) TObjectPtr<UActorComponent> OwnerComponent;
	friend UHCEquipmentManagerComponent;
};

/**
 * TODO: might need to remove the replication since we are doing single player
 */
UCLASS(BlueprintType, Const)
class HADESCELLS_API UHCEquipmentManagerComponent : public UPawnComponent
{
	GENERATED_BODY()
	
public:
	UHCEquipmentManagerComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly) UHCEquipmentInstance* EquipItem(UUHCEquipmentData* EquipmentData);
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly) void UnEquipItem(UHCEquipmentInstance* InInstanceToRemove, EHCEquipmentSlotType SlotType);
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly) void ReplaceItem(UUHCEquipmentData* EquipmentData, EHCEquipmentType EquipmentType, EHCEquipmentSlotType SlotType);

	//ActorComponent interfaces
	virtual void InitializeComponent() override;
	virtual void UninitializeComponent() override;
	//~ActorComponent interface

	FHCEquipmentList GetEquipmentList() { return EquipmentList; }
	TArray<AActor*> GetSpawnActorFromEquipmentSlotType(EHCEquipmentSlotType SlotType);
	void ShowWeaponFromSlotType(EHCEquipmentSlotType SlotType);

public:
	FOnEquipmentAdded OnEquipmentAdded;
	FOnEquipmentRemoved OnEquipmentRemoved;
	FHCEquipmentSlotFull OnEquipmentSlotFull;

	UUHCEquipmentData* MainWeaponEDCache;
	UUHCEquipmentData* SecondaryWeaponEDCache;
	UUHCEquipmentData* MainSkillEDCache;
	UUHCEquipmentData* SecondarySkillEDCache;
	UHCEquipmentInstance* MainWeaponEICache;
	UHCEquipmentInstance* SecondaryWeaponEICache;
	UHCEquipmentInstance* MainSkillEICache;
	UHCEquipmentInstance* SecondarySkillEICache;

private:
	UPROPERTY() FHCEquipmentList EquipmentList;
};
