// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HCInteractableComponent.generated.h"

class AHCHeroCharacter;
class UUHCEquipmentData;
class UCapsuleComponent;

UCLASS()
class HADESCELLS_API AHCInteractableComponent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHCInteractableComponent(const FObjectInitializer& ObjectInitializer);
	void OnConstruction(const FTransform& Transform) override;
	virtual void BeginInteraction(AHCHeroCharacter* ReceivingHero = nullptr);
	UFUNCTION() virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult);
	UFUNCTION() virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
public:
	UPROPERTY(EditDefaultsOnly, Category = "HC|Interactable") TObjectPtr<UCapsuleComponent> CollisionCapsule;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HC|Interactable") class UWidgetComponent* PickUpIndicateComponent;

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "HC|Interactable") FVector2D PickUpIndicateSize = FVector2D(100, 8);

};
