// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactables/HCInteractableComponent.h"
#include "HCAutoPickUp.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCAutoPickUp : public AHCInteractableComponent
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	AHCAutoPickUp(const FObjectInitializer& ObjectInitializer);
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult) override;
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
public:
	float DropGold = 0.0f;
	float DropExp = 0.0f;

private:
	bool m_HasPickedUp = false;
};
