// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HCHealthComponent.generated.h"

class UHCAttributeSetBase;
class UHCAbilitySystemComponent;
struct FGameplayEffectSpec;
struct FOnAttributeChangeData;

DECLARE_MULTICAST_DELEGATE_FourParams(FHCHealth_AttributeChanged, UHCHealthComponent* /* HealthComponent */ , float /*OldValue*/, float /* NewValue */, AActor* /* Instigator */);
DECLARE_MULTICAST_DELEGATE_FourParams(FHCShield_AttributeChanged, UHCHealthComponent* /* HealthComponent */, float /*OldValue*/, float /* NewValue */, AActor* /* Instigator */);
DECLARE_MULTICAST_DELEGATE_FourParams(FHCPosture_AttributeChanged, UHCHealthComponent* /* HealthComponent */, float /*OldValue*/, float /* NewValue */, AActor* /* Instigator */);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HADESCELLS_API UHCHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHCHealthComponent();
	static UHCHealthComponent* FindHealthComponent(const AActor* InActor) { return (InActor ? InActor->FindComponentByClass<UHCHealthComponent>() : nullptr); }
	void InitializeWithAbilitySystem(UHCAbilitySystemComponent* InASC);
	void UninitializeFromAbilitySystem();
	float GetHealth() const;
	float GetMaxHealth() const;
	float GetHealthNormalized() const;
	float GetShield() const;
	float GetMaxShield() const;
	float GetShieldNormalized() const;
	float GetPosture() const;
	float GetMaxPosture() const;
	float GetPostureNormalized() const;

	void ClearDelegates();
public:
	FHCHealth_AttributeChanged  OnHealthChanged;
	FHCHealth_AttributeChanged  OnMaxHealthChanged;
	FHCShield_AttributeChanged  OnShieldChanged;
	FHCShield_AttributeChanged  OnMaxShieldChanged;
	FHCPosture_AttributeChanged OnPostureChanged;
	FHCPosture_AttributeChanged OnMaxPostureChanged;
protected:

	virtual void OnUnregister() override;

	void ClearGameplayTags();

	virtual void HandleHealthChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleMaxHealthChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleOutOfHealth(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec& DamageEffectSpec, float DamageMagnitude);
	virtual void HandleShieldChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleMaxShieldChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleOutOfShield(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec& DamageEffectSpec, float DamageMagnitude);
	virtual void HandlePostureChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleMaxPostureChanged(const FOnAttributeChangeData& ChangeData);
	virtual void HandleOutOfPosture(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec& DamageEffectSpec, float DamageMagnitude);

protected:

	UPROPERTY() TObjectPtr<UHCAbilitySystemComponent> AbilitySystemComponent;
	UPROPERTY() TObjectPtr<const UHCAttributeSetBase> HealthSet;
};
