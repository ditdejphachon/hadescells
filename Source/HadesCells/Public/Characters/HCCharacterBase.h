// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#pragma region includes
#include "AbilitySystemInterface.h"
#include "../Characters/HCPawnData.h"
#include "HCHealthComponent.h"
#include "../Heroes/HCProgressionSystemComponent.h"
#pragma endregion

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HCCharacterBase.generated.h"

UENUM(BlueprintType)
enum class EHCDirection : uint8
{
	Right,
	Top,
	Left,
	Bottom,
};

struct FOnAttributeChangeData;
class UHCEquipmentManagerComponent;


UCLASS()
class HADESCELLS_API AHCCharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AHCCharacterBase(const class FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable, Category = "HC|PlayerState")
		class UHCAbilitySystemComponent* GetHCAbilitySystemComponent() const { return AbilitySystemComponent; }

	/** Implement IAbilitySystemInterface */
	virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable, Category = "HC|Character") virtual bool IsAlive() const;

	/**
	* Getters for attributes from HCAttributeSetBase
	**/
#pragma region Attribute Set interfaces
	UFUNCTION(BlueprintCallable, Category = "HC|HCCharacter|Attributes") float GetMoveSpeed() const;
	UFUNCTION(BlueprintCallable, Category = "HC|HCCharacter|Attributes") float GetMoveSpeedBaseValue() const;
	UFUNCTION(BlueprintCallable, Category = "HC|HCCharacter|Attributes") int32 GetCharacterLevel() const;
#pragma endregion ~Attribute set interfaces

	void HandleCharacterLevelChanged(const FOnAttributeChangeData& ChangeData);
	// ACharacter interfaces
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// ~ACharacter interfaces

	UFUNCTION(BlueprintCallable, Category = "HC|Character|Anim") void JumpSectionForCombo();
	virtual FVector LookAtCursor();
	virtual void PlayHitReactionMontage(EHCDirection InHitDirection);
	void ShowDamagePopUp(const float& InDamage, const FGameplayTagContainer& AdditionalTagContainer);
	UFUNCTION(BlueprintCallable, Category = "HC|Character") virtual void KnockBack(float InDistance, float InSpeed, FVector InDirection = FVector::ZeroVector);

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HC|Character|Component", Meta = (AllowPrivateAccess = "true"))
		TObjectPtr<UHCHealthComponent> HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HC|Character|Component", Meta = (AllowPrivateAccess = "true"))
		TObjectPtr<UHCProgressionSystemComponent> ProgressionSystemComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "HC|Character|Component", Meta = (AllowPrivateAccess = "true"))
		TObjectPtr<UHCEquipmentManagerComponent> EquipmentManagerComponent;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character|Anim") UAnimMontage* DeathMontage;
	class AHCWeaponBase* CurrentAttackWeapon;
	class UHCANS_JumpSection* JumpSectionANS;
	bool bEnableCombo = false;
	FName CurrentMontageSection;

	bool bCanMove = true;
	bool bCanRotate = true;
	bool bCanKnockBack = false;
	bool bHasDied = false;

	float KnockBackDistanceTraveled = 0.0f;
protected:
	/**
		Reference to the ASC.
	*/
	UPROPERTY(VisibleAnywhere, Category = "HC|Character") TObjectPtr<class UHCAbilitySystemComponent> AbilitySystemComponent;
	UPROPERTY(VisibleAnywhere, Category = "HC|Character") TObjectPtr<const class UHCAttributeSetBase> AttributeSetBase;
	/** Pawn data used to create the pawn. Specified from a spawn function or on a placed instance. */
	UPROPERTY(EditAnywhere, Category = "HC|Character") TObjectPtr<UHCPawnData> PawnData;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character|Anim") TMap<EHCDirection, UAnimMontage*> HitReactionMontages;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") TSubclassOf<class UHCW_DamagePopUp> WBP_DamagePopUp;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float PN_DamagePopUp_XY = 10.0f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float PN_DamagePopUp_Z = 10.0f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float DamagePopUpLifeSpan = 1.0f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float KnockBackSpeed = 3000.0f;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float DeathAnimLength = 1.0f; // TODO: change this. Very ugly.
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") int32 FlashingStencilValue = 1;
	UPROPERTY(EditDefaultsOnly, Category = "HC|Character") float FlashingDelay = 0.1f;
	bool bIsShieldBreak = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void InitializeAttributes();
	virtual void HandleHealthChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator);
	virtual void HandleShieldChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator);
	virtual void HandlePostureChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator);
	virtual void HandleTagChanged(const FGameplayTag CallbackTag, int32 NewCount);
	virtual void HandleGoldChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator);
	virtual void HandleExpChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator);

	virtual void Die();
	//UFUNCTION() virtual void FinishDying(UAnimMontage* Montage, bool bInterrupted);
	UFUNCTION() virtual void FinishDying();

	UFUNCTION() virtual void OnGameplayEffectRemoved(const FActiveGameplayEffect& ActiveEffect);

	void StartKnockBack();
	virtual void StartFlashing();
	virtual void ResetFlashing();
	void ClearTimerHandles();

private:
	void MySetCustomDepthStencilValue(int32 InStencilValue);
private:
	bool bParticleSpawned;
	FTimerHandle KnockBackTimerHandle;
	float KnockBackDistance;
	FVector KnockBackDirection;
	TArray<class UWidgetComponent*> DamagePopUpComps;
	FTimerHandle DamageCompHandle;
	FTimerHandle FlashingHandle;

	FDelegateHandle OnHealthChangedHandle;
	FDelegateHandle OnShieldChangedHandle;
	FDelegateHandle OnPostureChangedHandle;
	FDelegateHandle OnMaxHealthChangedHandle;
	FDelegateHandle OnMaxShieldChangedHandle;
	FDelegateHandle OnMaxPostureChangedHandle;

	FDelegateHandle OnGoldChangedHandle;
	FDelegateHandle OnExpChangedHandle;
	FDelegateHandle OnMaxExpChangedHandle;

};
