// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilitySystemInterface.h"


#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "HCPlayerState.generated.h"


class UHCAbilitySystemComponent;
/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:
	AHCPlayerState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	UFUNCTION(BlueprintCallable, Category = "HC|PlayerState")
		UHCAbilitySystemComponent* GetHCAbilitySystemComponent() const { return AbilitySystemComponent; }
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	virtual void PostInitializeComponents() override;

	void SetPawnData(const class UHCPawnData* InPawnData);

public:
	bool bHasSetPawnData;
private:

	// The ability system component sub-object used by player characters.
	UPROPERTY(VisibleAnywhere, Category = "Lyra|PlayerState") TObjectPtr<UHCAbilitySystemComponent> AbilitySystemComponent;

};
