// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <Abilities/HCAbilitySet.h>

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "HCPawnData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Const, Meta = (DisplayName = "HC Pawn Data", ShortTooltip = "Data asset used to define a Pawn."))
class HADESCELLS_API UHCPawnData : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:

	UHCPawnData(const FObjectInitializer& ObjectInitializer);

public:
	// Ability sets to grant to this pawn's ability system.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Abilities")
		TArray<TObjectPtr<class UHCAbilitySet>> AbilitySets;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Gameplay Effect")
		FHCAbilitySet_GameplayEffect StunGameplayEffect;

	// Input configuration used by player controlled pawns to create input mappings and bind input actions.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HC|Input")
		TObjectPtr<class UHCInputConfig> InputConfig;
};
