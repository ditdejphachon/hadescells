// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "HCPlayerController.generated.h"


class UHCAbilitySystemComponent;

/**
 * 
 */
UCLASS()
class HADESCELLS_API AHCPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AHCPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:
#pragma region APlayerController Interfaces
	virtual void PreProcessInput(const float DeltaTime, const bool bGamePaused) override;
	virtual void PostProcessInput(const float DeltaTime, const bool bGamePaused) override;
	virtual void OnPossess(APawn* InPawn) override;
#pragma endregion APlayerController Interfaces


};
