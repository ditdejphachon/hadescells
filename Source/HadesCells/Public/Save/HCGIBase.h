// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "HCSaveGameBase.h"

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "HCGIBase.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCGIBase : public UGameInstance
{
	GENERATED_BODY()

public:
	UHCGIBase();
	UFUNCTION(BlueprintCallable) UHCSaveGameBase* LoadGame();
	UFUNCTION(BlueprintCallable) void SaveGame();
public:
	UPROPERTY(BlueprintReadOnly) FString SaveGameSlotName;
	UPROPERTY(BlueprintReadOnly) int32 UserIndex;
	UPROPERTY(BlueprintReadWrite) UHCSaveGameBase* SaveGameObject;
	UPROPERTY(BlueprintReadWrite) int ChamberNumber;
protected:
	void Init() override;
	void OnStart() override;

};
