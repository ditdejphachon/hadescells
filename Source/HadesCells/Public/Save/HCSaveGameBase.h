// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "../Equipment/UHCEquipmentData.h"
#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "HCSaveGameBase.generated.h"

/**
 * 
 */
UCLASS()
class HADESCELLS_API UHCSaveGameBase : public USaveGame
{
	GENERATED_BODY()
	
public:
	UHCSaveGameBase() {};
	
	UPROPERTY(BlueprintReadWrite) float PlayerHealth;
	UPROPERTY(BlueprintReadWrite) float PlayerShield;

	UPROPERTY(BlueprintReadWrite) TObjectPtr<UUHCEquipmentData> PrimaryWeaponED;
	UPROPERTY(BlueprintReadWrite) TObjectPtr<UUHCEquipmentData> SecondaryWeaponED;
	UPROPERTY(BlueprintReadWrite) TObjectPtr<UUHCEquipmentData> PrimarySkillED;
	UPROPERTY(BlueprintReadWrite) TObjectPtr<UUHCEquipmentData> SecondarySkillED;

	//UPROPERTY(BlueprintReadWrite) FPrimaryAssetId PrimaryWeaponPAI;
	//UPROPERTY(BlueprintReadWrite) FPrimaryAssetId SecondaryWeaponPAI;
	//UPROPERTY(BlueprintReadWrite) FPrimaryAssetId PrimarySkillPAI;
	//UPROPERTY(BlueprintReadWrite) FPrimaryAssetId SecondarySkillPAI;
};
