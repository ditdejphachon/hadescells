// Copyright Epic Games, Inc. All Rights Reserved.

#include "HadesCellsGameMode.h"
#include "HadesCellsCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Enemies/HCEnemyBase.h"
#include <Kismet/GameplayStatics.h>
#include "Enemies/HCEnemySpawner.h"
#include "UIs/HCHUDBase.h"
#include <Save/HCGIBase.h>
#include <Heroes/HCHeroCharacter.h>
#include "Equipment/HCEquipmentManagerComponent.h"
#include <Levels/HCNextDoor.h>

AHadesCellsGameMode::AHadesCellsGameMode()
{
	bCanProceed = false;
	HUDInstance = nullptr;
	EnemyKilled = 0;
	TotalEnemySpawned = 0;
}

void AHadesCellsGameMode::StartPlay()
{
	Super::StartPlay();
	check(GetWorld());
	EnemyKilled = 0;
	TotalEnemySpawned = 0;
	UpdateTotalSpawnedEnemy();
	HUDInstance = Cast<AHCHUDBase>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD());
	if (HUDInstance)
	{
		HUDInstance->SetUpEnemyKillUI(TotalEnemySpawned);
		HUDInstance->UpdateEnemyKillUI(0);
	}
}

void AHadesCellsGameMode::HandleEnemyKilled()
{
	EnemyKilled++;
	EnemyKilled = std::min(EnemyKilled, TotalEnemySpawned);

	if (EnemyKilled == TotalEnemySpawned)
	{
		if (bShouldRespawn)
		{
			EnemyKilled = 0;
			TArray<AActor*> Spawners;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AHCEnemySpawner::StaticClass(), Spawners);
			for (auto& Spawner : Spawners)
			{
				if (AHCEnemySpawner* MySpawner = Cast<AHCEnemySpawner>(Spawner))
				{
					MySpawner->StartSpawning();
				}
			}

			if (HUDInstance)
			{
				HUDInstance->UpdateEnemyKillUI(EnemyKilled);
			}
			return;
		}

		if (UHCGIBase* GI = Cast<UHCGIBase>(GetGameInstance()))
		{
			if (UHCSaveGameBase* SG = GI->LoadGame())
			{
				if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
				{
					SG->PlayerHealth = Hero->HealthComponent->GetHealth();
					SG->PlayerShield = Hero->HealthComponent->GetShield();

					SG->PrimaryWeaponED = Hero->EquipmentManagerComponent->MainWeaponEDCache;
					SG->SecondaryWeaponED = Hero->EquipmentManagerComponent->SecondaryWeaponEDCache;
					SG->PrimarySkillED = Hero->EquipmentManagerComponent->MainSkillEDCache;
					SG->SecondarySkillED = Hero->EquipmentManagerComponent->SecondarySkillEDCache;

					GI->SaveGame();

					TArray<AActor*> NextDoor;
					UGameplayStatics::GetAllActorsOfClass(GetWorld(), AHCNextDoor::StaticClass(), NextDoor);
					if (NextDoor.IsValidIndex(0))
					{
						Hero->CurrentInteractableComponent = Cast<AHCInteractableComponent>(NextDoor[0]);
					}
				}
			
				bCanProceed = true;
				OnReadyToProceed.Broadcast();
			}
		}
	}

	if (HUDInstance)
	{
		HUDInstance->UpdateEnemyKillUI(EnemyKilled);
	}
}

void AHadesCellsGameMode::ProceedToNextLevel()
{
	if (bCanProceed)
	{
		/*if (UHCGIBase* GI = Cast<UHCGIBase>(UGameplayStatics::GetGameInstance(GetWorld())))
		{
			UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), LevelObjs[GI->ChamberNumber]);
			GI->ChamberNumber += 1;
		}*/
		//K2_OnProceed();
	/*	FName LevelToLoad = LevelNames.Pop();
		if (LevelToLoad.IsValid())
		{
			UGameplayStatics::OpenLevel(GetWorld(), LevelToLoad);
		}*/

		if (UHCGIBase* GI = Cast<UHCGIBase>(UGameplayStatics::GetGameInstance(GetWorld())))
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHadesCellsGameMode::ProceedToNextLevel] GI->ChamberNumber: [%d]"), GI->ChamberNumber);
			UGameplayStatics::OpenLevel(GetWorld(), LevelNames[GI->ChamberNumber]);
			GI->ChamberNumber += 1;
		}
	}
}

void AHadesCellsGameMode::RestartLevel()
{
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}

void AHadesCellsGameMode::UpdateTotalSpawnedEnemy()
{
	TArray<AActor*> Spawners;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AHCEnemySpawner::StaticClass(), Spawners);
	TotalEnemySpawned = Spawners.Num();
}
