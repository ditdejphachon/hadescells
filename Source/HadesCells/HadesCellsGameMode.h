// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HadesCellsGameMode.generated.h"

DECLARE_MULTICAST_DELEGATE(FHCReadyToProceed);

class AHCEnemyBase;
class AHCHUDBase;

UCLASS(minimalapi)
class AHadesCellsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHadesCellsGameMode();
	virtual void StartPlay() override;
	void HandleEnemyKilled();
	void ProceedToNextLevel();
	void RestartLevel();
	void UpdateTotalSpawnedEnemy();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable) void K2_OnProceed();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable) void K2_OnPlayerDied();


public:
	FHCReadyToProceed OnReadyToProceed;
	bool bCanProceed;

protected:
	UPROPERTY(EditAnywhere, Category = "HC|GameMode") TArray<FName> LevelNames;
	UPROPERTY(EditAnywhere, Category = "HC|GameMode") TArray<const TSoftObjectPtr<UWorld>> LevelObjs;
	UPROPERTY(EditAnywhere, Category = "HC|GameMode") bool bShouldRespawn = false;

private:
	AHCHUDBase* HUDInstance;
	uint8 TotalEnemySpawned;
	uint8 EnemyKilled;
};



