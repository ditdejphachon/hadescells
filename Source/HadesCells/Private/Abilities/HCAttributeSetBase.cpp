// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCAttributeSetBase.h"

#pragma region includes
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "Characters/HCCharacterBase.h"
#pragma endregion

UHCAttributeSetBase::UHCAttributeSetBase() {}

void UHCAttributeSetBase::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	/**
		The function is called when the attribute is changed.
		So, for max health, mana, and stamina, we want to scale them to match the change
	*/

	Super::PreAttributeChange(Attribute, NewValue);

	// If a Max value changes, adjust current to keep Current % of Current to Max
	if (Attribute == GetMaxHealthAttribute()) // GetMaxHealthAttribute comes from the Macros defined at the top of the header
	{
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}
	else if (Attribute == GetMoveSpeedAttribute())
	{
		// Cannot slow less than 150 units/s and cannot boost more than 1000 units/s
		NewValue = FMath::Clamp<float>(NewValue, 150, 1000);
	}
}

void UHCAttributeSetBase::AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty)
{
	UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent();
	const float CurrentMaxValue = MaxAttribute.GetCurrentValue();
	if (!FMath::IsNearlyEqual(CurrentMaxValue, NewMaxValue) && AbilityComp)
	{
		// Change current value to maintain the current Val / Max Percent
		const float CurrentValue = AffectedAttribute.GetCurrentValue();
		float NewDelta = (CurrentMaxValue > 0.f) ? (CurrentValue * NewMaxValue / CurrentMaxValue) - CurrentMaxValue : NewMaxValue;

		// Apply the change
		AbilityComp->ApplyModToAttributeUnsafe(AffectedAttributeProperty, EGameplayModOp::Additive, NewDelta);
	}
}


void UHCAttributeSetBase::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	// get source's ASC
	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();

	// get tags
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();
	FGameplayTagContainer SpecAssetTags;
	FGameplayTagContainer GrantedTags;
	Data.EffectSpec.GetAllAssetTags(SpecAssetTags);
	Data.EffectSpec.GetAllGrantedTags(GrantedTags);

	// Get the Target actor, which should be our owner
	AActor* TargetActor = nullptr;
	AController* TargetController = nullptr;
	AHCCharacterBase* TargetCharacter = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
		TargetCharacter = Cast<AHCCharacterBase>(TargetActor);
	}

	// Get the Source actor
	AActor* SourceActor = nullptr;
	AController* SourceController = nullptr;
	AHCCharacterBase* SourceCharacter = nullptr;
	if (Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
	{
		SourceActor = Source->AbilityActorInfo->AvatarActor.Get();
		SourceController = Source->AbilityActorInfo->PlayerController.Get();

		// try get controller from pawn
		if (SourceController == nullptr && SourceActor != nullptr)
		{
			if (APawn* Pawn = Cast<APawn>(SourceActor))
			{
				SourceController = Pawn->GetController();
			}
		}

		// Use the controller to find the source pawn
		SourceCharacter = Cast<AHCCharacterBase>(SourceController ? SourceController->GetPawn() : SourceActor);

		// Set the causer actor based on context if it's set
		if (Context.GetEffectCauser())
		{
			SourceActor = Context.GetEffectCauser();
		}
	}

	// Attribute manipulation

	// Health
	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		// Handle other health changes.
		// Health loss should go through Damage.
		SetHealth(FMath::Clamp(GetHealth(), 0.0f, GetMaxHealth()));
		if (SourceCharacter)
		{
			float HealthMagnitude = Data.EvaluatedData.Magnitude;
			if (HealthMagnitude > 0.0f)
			{
				GrantedTags.AddTag(FGameplayTag::RequestGameplayTag(FName("State.Buff.AddHealth")));
			}
			else
			{
				// TODO: need to be more specific???
				//GrantedTags.AddTag(FGameplayTag::RequestGameplayTag(FName("State.Buff.AddHealth")));
			}
			SourceCharacter->ShowDamagePopUp(HealthMagnitude, GrantedTags);
		}
	}
	// Damage
	else if (Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		// Try to extract a hit result
		FHitResult HitResult;
		if (Context.GetHitResult())
		{
			HitResult = *Context.GetHitResult();
		}

		// Store a local copy of the amount of damage done and clear the damage attribute
		const float LocalDamageDone = GetDamage();
		SetDamage(0.f);

		if (LocalDamageDone > 0.0f)
		{
			// If character was alive before damage is added, handle damage
			// This prevents damage being added to dead things and replaying death animations
			bool WasAlive = true;

			if (TargetCharacter)
			{
				WasAlive = TargetCharacter->IsAlive();
			}

			/**
			*	Apply the damage to the shield first if exists
			*/
			const float OldShield = GetShield();
			float DamageAfterShield = LocalDamageDone - OldShield;
			if (OldShield > 0.0f)
			{
				// Apply the shield change and then clamp it
				float NewShield = OldShield - LocalDamageDone;
				NewShield = FMath::Clamp<float>(NewShield, 0.0f, GetMaxShield());
				SetShield(NewShield);
				if (SourceCharacter)
				{
					GrantedTags.AddTag(FGameplayTag::RequestGameplayTag(FName("State.Hit.Shield")));
					SourceCharacter->ShowDamagePopUp(LocalDamageDone, GrantedTags);
				}
			}
			else
			{
				const float OldPosture = GetPosture();
				float NewPosture = OldPosture - LocalDamageDone;
				SetPosture(FMath::Clamp<float>(NewPosture, 0.0f, GetMaxPosture()));
			}

			if (DamageAfterShield > 0)
			{
				// Apply the health change and then clamp it
				float NewHealth = GetHealth() - DamageAfterShield;
				NewHealth = FMath::Clamp(NewHealth, 0.0f, GetMaxHealth());
				SetHealth(NewHealth);

				if (SourceCharacter)
				{
					if (!GrantedTags.HasTag(FGameplayTag::RequestGameplayTag(FName("State.Hit.Shield"))))
					{
						SourceCharacter->ShowDamagePopUp(DamageAfterShield, GrantedTags);
					}
				}
			}

			if (TargetCharacter && WasAlive)
			{
				// TODO: show damage UI

				// TargetCharacter was alive before this damage and now is not alive, give cells and Gold reward to Source.
				if (!TargetCharacter->IsAlive())
				{
					// Don't give rewards to self.
					if (SourceController != TargetController)
					{
						// Create a dynamic instant Gameplay Effect to give the reward
						// TODO: give reward using GameplayEffect
					}
				}
			}
		}
	}

}

