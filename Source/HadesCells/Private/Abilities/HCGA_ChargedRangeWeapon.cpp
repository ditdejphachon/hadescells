// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCGA_ChargedRangeWeapon.h"
#include "Abilities/Tasks/AbilityTask_WaitInputRelease.h"
#include "Abilities/Tasks/AbilityTask_PlayMontageAndWait.h"

#include <HCGameplayAbilityLibrary.h>
#include <Kismet/GameplayStatics.h>
#include <NiagaraFunctionLibrary.h>
#include "NiagaraComponent.h"
#include <Heroes/HCHeroCharacter.h>

UHCGA_ChargedRangeWeapon::UHCGA_ChargedRangeWeapon(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SpawnedTargetActor = nullptr;
	ChargingPS = nullptr;
	ChargedPS = nullptr;
}

void UHCGA_ChargedRangeWeapon::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	UHCGameplayAbility_RangeWeapon::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo()))
	{
		ExecuteAbility();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::ActivateAbility] Not HCCharacterBase. Canceling."));
		K2_CancelAbility();
	}
}

void UHCGA_ChargedRangeWeapon::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	if (ChargingPS)
	{
		ChargingPS->DestroyComponent();
	}

	if (ChargedPS)
	{
		ChargingPS->DestroyComponent();
	}

	if (SpawnedTargetActor)
	{
		SpawnedTargetActor->Destroy();
	}

	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UHCGA_ChargedRangeWeapon::ExecuteAbility()
{
	AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo());
	if (Char == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::ExecuteAbility] Invalid Char. Ending Ability."));
		K2_EndAbility();
		return;
	}

	UAbilityTask_WaitInputRelease* AT = UAbilityTask_WaitInputRelease::WaitInputRelease(this, true);
	if (AT == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::ExecuteAbility] Invalid wait input release Ability Task. Ending Ability."));
		K2_EndAbility();
		return;
	}

	AT->OnRelease.AddDynamic(this, &UHCGA_ChargedRangeWeapon::OnInputReleased);
	AT->ReadyForActivation();
	
	UAbilityTask_PlayMontageAndWait* MAT = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, FName("None"), CastMontage);
	if (MAT == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::ExecuteAbility] Invalid play montage and wait Ability Task. Ending Ability."));
		K2_EndAbility();
		return;
	}

	MAT->ReadyForActivation();

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	SpawnedTargetActor = Cast<AHCGA_TargetActor_CursorTrace>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, TargetActorClass, Char->GetActorTransform(), ESpawnActorCollisionHandlingMethod::AlwaysSpawn, Char));

	if (SpawnedTargetActor == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::OnReceiveValidTargetData] SpawnedTargetActor is invalid"));
		K2_CancelAbility();
		return;
	}

	SpawnedTargetActor->SetOwner(Char);
	SpawnedTargetActor->SetInstigator(Char);
	UGameplayStatics::FinishSpawningActor(SpawnedTargetActor, Char->GetActorTransform());

	FAttachmentTransformRules AttachmentRules{ EAttachmentRule::KeepWorld, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false};

	SpawnedTargetActor->AttachToActor(Char, AttachmentRules, FName("Reticle"));

	ChargingPS = UNiagaraFunctionLibrary::SpawnSystemAttached(ChargingEffectInfo.NiagaraFX, Char->GetMesh(), ChargingEffectInfo.AttachPointNames, FVector(), FRotator(), ChargingEffectInfo.AttachLocation, true, true);

	GetWorld()->GetTimerManager().SetTimer(ChargingHandle, [&, Char]() {
		UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::OnReceiveValidTargetData] Charged"));
		if (ChargingHandle.IsValid())
		{
			ChargedPS = UNiagaraFunctionLibrary::SpawnSystemAttached(ChargedEffectInfo.NiagaraFX, Char->GetMesh(), ChargedEffectInfo.AttachPointNames, FVector(), FRotator(), ChargedEffectInfo.AttachLocation, true, true);

			ChargingPS->DestroyComponent();
		}
	},
	AllowableChargeThreshold - 0.2f, false);
}

void UHCGA_ChargedRangeWeapon::OnInputReleased(float TimeHeld)
{
	if (SpawnedTargetActor)
	{
		SpawnedTargetActor->Destroy();
	}

	if (IsCharged(TimeHeld))
	{
		UAbilityTask_PlayMontageAndWait* MAT = UAbilityTask_PlayMontageAndWait::CreatePlayMontageAndWaitProxy(this, FName("None"), OnReleasedMontage, 1.f, NAME_None, false);
		if (MAT == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::OnInputReleased] Invalid OnReleasedMontage and wait Ability Task. Ending Ability."));
			K2_EndAbility();
			return;
		}
		MAT->OnCompleted.AddDynamic(this, &UHCGA_ChargedRangeWeapon::OnMontageEvent);
		MAT->OnBlendOut.AddDynamic(this, &UHCGA_ChargedRangeWeapon::OnMontageEvent);
		MAT->OnInterrupted.AddDynamic(this, &UHCGA_ChargedRangeWeapon::OnMontageEvent);
		MAT->OnCancelled.AddDynamic(this, &UHCGA_ChargedRangeWeapon::OnMontageEvent);

		MAT->ReadyForActivation();

		AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(GetAvatarActorFromActorInfo());
		if (Hero == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::OnInputReleased] Invalid Hero. Ending Ability."));
			K2_EndAbility();
			return;
		}

		FTransform SpawnTransform = DetermineSpawnTransform(Hero->GetActorLocation(), Hero->LookAtLocation, m_SpawnPitch, m_InstigatorOffset, m_GroundOffset);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		AHCProjectileBase* SpawnedProjectile = Cast<AHCProjectileBase>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ProjectileClass, SpawnTransform, ESpawnActorCollisionHandlingMethod::AlwaysSpawn, Hero));

		if (SpawnedProjectile == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::OnInputReleased] SpawnedProjectile is invalid"));
			K2_CancelAbility();
			return;
		}

		SpawnedProjectile->CalculatedInitialSpeed = ProjectileSpeed;
		SpawnedProjectile->SetOwner(Hero);
		SpawnedProjectile->SetInstigator(Hero);
		UGameplayStatics::FinishSpawningActor(SpawnedProjectile, SpawnTransform);
	}

	K2_EndAbility();
}

void UHCGA_ChargedRangeWeapon::OnMontageEvent()
{
	K2_EndAbility();
}
