// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCGameplayAbility_MeleeWeapon.h"
#include "Equipment/Weapon/HCWeaponInstance.h"
#include <Abilities/AbilityTasks/HCAT_PlayMontageAndWaitForEvents.h>
#include "Characters/HCCharacterBase.h"

UHCGameplayAbility_MeleeWeapon::UHCGameplayAbility_MeleeWeapon()
{
}

UHCWeaponInstance* UHCGameplayAbility_MeleeWeapon::GetWeaponInstance() const
{
	return Cast<UHCWeaponInstance>(GetAssociatedEquipment());
}

bool UHCGameplayAbility_MeleeWeapon::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	bool bResult = Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags);

	return bResult;
}

void UHCGameplayAbility_MeleeWeapon::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo()))
	{
		PlayAttackMontage();
	}
	else
	{
		K2_EndAbility();
	}
}

void UHCGameplayAbility_MeleeWeapon::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UHCGameplayAbility_MeleeWeapon::ExecuteEndAbility(FGameplayTag EventTag, FGameplayEventData EventData)
{
	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
}

void UHCGameplayAbility_MeleeWeapon::PlayAttackMontage()
{
	UHCAT_PlayMontageAndWaitForEvents* AT = UHCAT_PlayMontageAndWaitForEvents::PlayMontageAndWaitForEvent(this, FName("None"), MeleeAnimMontage, FGameplayTagContainer(), MontagePlaybackRate, FName("None"), bStopWhenAbilityEnds);

	AT->OnComplete.AddDynamic(this, &UHCGameplayAbility_MeleeWeapon::OnMontageComplete);
	AT->OnBlendOut.AddDynamic(this, &UHCGameplayAbility_MeleeWeapon::OnMontageBlendOut);
	AT->OnInterrupted.AddDynamic(this, &UHCGameplayAbility_MeleeWeapon::OnMontageInterruped);
	AT->OnCancelled.AddDynamic(this, &UHCGameplayAbility_MeleeWeapon::OnMontageCancelled);

	AT->ReadyForActivation();

	K2_OnAbilityActivate();
}

void UHCGameplayAbility_MeleeWeapon::OnMontageComplete(FGameplayTag EventTag, FGameplayEventData EventData)
{
	K2_EndAbility();
}

void UHCGameplayAbility_MeleeWeapon::OnMontageBlendOut(FGameplayTag EventTag, FGameplayEventData EventData)
{
	K2_EndAbility();
}

void UHCGameplayAbility_MeleeWeapon::OnMontageInterruped(FGameplayTag EventTag, FGameplayEventData EventData)
{
	K2_EndAbility();
}

void UHCGameplayAbility_MeleeWeapon::OnMontageCancelled(FGameplayTag EventTag, FGameplayEventData EventData)
{
	K2_EndAbility();
}
