// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/Ability/Enemy/HCGA_EnemyMelee.h"
#include <Abilities/AbilityTasks/HCAT_PlayMontageAndWaitForEvents.h>
#include "Abilities/HCAbilitySystemComponent.h"
#include "Characters/HCCharacterBase.h"

void UHCGA_EnemyMelee::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo()))
	{
		if (UHCAbilitySystemComponent* HCASC = Char->GetHCAbilitySystemComponent())
		{
			HCASC->AddLooseGameplayTags(AdditionalGameplayTags);
			PlayPreAttackMontage();
		}
	}
}

void UHCGA_EnemyMelee::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo()))
	{
		if (UHCAbilitySystemComponent* HCASC = Char->GetHCAbilitySystemComponent())
		{
			HCASC->RemoveLooseGameplayTags(AdditionalGameplayTags);
		}
	}
	UHCGameplayAbility_MeleeWeapon::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UHCGA_EnemyMelee::PlayPreAttackMontage()
{
	UHCAT_PlayMontageAndWaitForEvents* AT = UHCAT_PlayMontageAndWaitForEvents::PlayMontageAndWaitForEvent(this, FName("None"), PreAttackAnimMontage, FGameplayTagContainer(), PreAttackMontagePlaybackRate, FName("None"), bPreAttackStopWhenAbilityEnds);
	AT->OnBlendOut.AddDynamic(this, &UHCGA_EnemyMelee::OnPreAttackMontageBlendOut);
	AT->ReadyForActivation();
}

void UHCGA_EnemyMelee::OnPreAttackMontageBlendOut(FGameplayTag EventTag, FGameplayEventData EventData)
{
	PlayAttackMontage();
	K2_OnAbilityActivate();
}
