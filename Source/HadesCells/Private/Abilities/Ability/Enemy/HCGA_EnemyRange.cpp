// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/Ability/Enemy/HCGA_EnemyRange.h"
#include <Abilities/AbilityTasks/HCAT_PlayMontageAndWaitForEvents.h>
#include "Abilities/HCAbilitySystemComponent.h"
#include "Characters/HCCharacterBase.h"
#include <HCGameplayAbilityLibrary.h>
#include <Kismet/GameplayStatics.h>
#include <Equipment/Projectile/HCProjectileBase.h>

void UHCGA_EnemyRange::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo());
	ACharacter* Hero = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	if (Char && Hero)
	{
		FVector CharLocation = Char->GetActorLocation();
		FVector HeroLocation = Hero->GetActorLocation();

		UHCAT_PlayMontageAndWaitForEvents* AT = UHCAT_PlayMontageAndWaitForEvents::PlayMontageAndWaitForEvent(this, FName("None"), CastMontage, FGameplayTagContainer(), 1.0f, FName("None"), false);
		AT->ReadyForActivation();

		Char->SetActorRotation(GetLookAtHeroRotator());
		FTransform SpawnTransform = DetermineSpawnTransform(CharLocation, HeroLocation, m_SpawnPitch, m_InstigatorOffset, m_GroundOffset);
		float CalculatedInitialSpeed = UHCGameplayAbilityLibrary::CalculateLaunchVelocity(SpawnTransform.GetLocation(), HeroLocation, m_SpawnPitch);

		// spawn projectile
		AHCProjectileBase* SpawnedProjectile = nullptr;
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			SpawnedProjectile = Cast<AHCProjectileBase>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ProjectileClass, SpawnTransform, ESpawnActorCollisionHandlingMethod::AlwaysSpawn, Char));

			if (SpawnedProjectile == nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("[UHCGA_EnemyRange::OnReceiveValidTargetData] SpawnedProjectile is invalid"));
				K2_CancelAbility();
				return;
			}

			SpawnedProjectile->CalculatedInitialSpeed = CalculatedInitialSpeed;
			SpawnedProjectile->SetOwner(Char);
			SpawnedProjectile->SetInstigator(Char);
			UGameplayStatics::FinishSpawningActor(SpawnedProjectile, SpawnTransform);
		}
		
		// spawn target actor
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			FTransform HeroTransform = Hero->GetActorTransform();

			AHCGA_TargetActor_CursorTrace* TargetActor = Cast<AHCGA_TargetActor_CursorTrace>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, TargetActorClass, HeroTransform, ESpawnActorCollisionHandlingMethod::AlwaysSpawn, SpawnedProjectile));

			if (TargetActor == nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("[UHCGA_EnemyRange::OnReceiveValidTargetData] TargetActor is invalid"));
				K2_CancelAbility();
				return;
			}

			TargetActor->SetOwner(SpawnedProjectile);
			float DecalSize = SpawnedProjectile->SphereTraceRadius;
			TargetActor->SetSize(FVector(DecalSize, DecalSize, DecalSize));
			UGameplayStatics::FinishSpawningActor(TargetActor, HeroTransform);

			TargetActor->SetLifeSpan(TargetActorLifeSpan);
		}
	}

	K2_EndAbility();
}