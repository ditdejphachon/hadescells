// Fill out your copyright notice in the Description page of Project Settings.
#include "Abilities/Ability/HCGameplayAbility.h"

#include "Abilities/HCAbilitySystemComponent.h"
#include "Characters/HCCharacterBase.h"
#include <Abilities/Ability/HCAbilityCost.h>
#include <AbilitySystemBlueprintLibrary.h>
#include "AbilitySystemGlobals.h"
#include <Heroes/HCHeroCharacter.h>
#include "Equipment/HCEquipmentManagerComponent.h"

#define ENSURE_ABILITY_IS_INSTANTIATED_OR_RETURN(FunctionName, ReturnValue)																				\
{																																						\
	if (!ensure(IsInstantiated()))																														\
	{																																					\
		UE_LOG(LogTemp,Error, TEXT("%s: " #FunctionName " cannot be called on a non-instanced ability. Check the instancing policy."), *GetPathName());	\
		return ReturnValue;																																\
	}																																					\
}

UHCGameplayAbility::UHCGameplayAbility(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ActivationPolicy = EHCAbilityActivationPolicy::OnInputTriggered;
	ActivationGroup = EHCAbilityActivationGroup::Independent;
}

UHCAbilitySystemComponent* UHCGameplayAbility::GetHCAbilitySystemComponentFromActorInfo() const
{ 
	return CurrentActorInfo ? Cast<UHCAbilitySystemComponent>(CurrentActorInfo->AbilitySystemComponent.Get()) : nullptr;
}

AHCCharacterBase* UHCGameplayAbility::GetHCCharacterFromActorInfo() const
{
	return CurrentActorInfo ? Cast<AHCCharacterBase>(CurrentActorInfo->AvatarActor.Get()) : nullptr;
}

void UHCGameplayAbility::TryActivateAbilityOnSpawn(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) const
{
	if (ActorInfo && !Spec.IsActive() && (ActivationPolicy == EHCAbilityActivationPolicy::OnSpawn))
	{
		UAbilitySystemComponent* ASC = ActorInfo->AbilitySystemComponent.Get();
		const AActor* AvatorActor = ActorInfo->AvatarActor.Get();

		if (ASC && AvatorActor && (AvatorActor->GetLifeSpan() <= 0.0f))
		{
			// NOTE: no need to check for client/server execution policy cus we doing singplayer BABYYYY
			ASC->TryActivateAbility(Spec.Handle);
		}
	}
}

bool UHCGameplayAbility::CanChangeActivationGroup(EHCAbilityActivationGroup NewGroup) const
{
	// cannot for abiltiy that has not been instantiated and actived
	if (!IsInstantiated() || !IsActive())
	{
		return false;
	}

	if (ActivationGroup == NewGroup)
	{
		return true;
	}

	UHCAbilitySystemComponent* HCASC = GetHCAbilitySystemComponentFromActorInfo();
	check(HCASC);

	if ((ActivationGroup != EHCAbilityActivationGroup::Exclusive_Blocking) && HCASC->IsActivationGroupBlocked(NewGroup))
	{
		// This ability can't change groups if it's blocked (unless it is the one doing the blocking).
		return false;
	}

	if ((NewGroup == EHCAbilityActivationGroup::Exclusive_Replaceable) && !CanBeCanceled())
	{
		// This ability can't become replaceable if it can't be canceled.
		return false;
	}

	return true;
}

bool UHCGameplayAbility::ChangeActivationGroup(EHCAbilityActivationGroup NewGroup)
{
	ENSURE_ABILITY_IS_INSTANTIATED_OR_RETURN(ChangeActivationGroup, false);

	if (!CanChangeActivationGroup(NewGroup))
	{
		return false;
	}

	if (ActivationGroup != NewGroup)
	{
		UHCAbilitySystemComponent* HCASC = GetHCAbilitySystemComponentFromActorInfo();
		check(HCASC);

		HCASC->RemoveAbilityToActivationGroup(ActivationGroup, this);
		HCASC->AddAbilityToActivationGroup(NewGroup, this);

		ActivationGroup = NewGroup;
	}

	return true;
}

void UHCGameplayAbility::SetBlockAbilityWithTags(const FGameplayTagContainer& InTagsToBlock)
{
	for (const FGameplayTag& tag : InTagsToBlock)
	{
		BlockAbilitiesWithTag.AddTag(tag);
	}
}

bool UHCGameplayAbility::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, FGameplayTagContainer* OptionalRelevantTags) const
{
	if (!ActorInfo || !ActorInfo->AbilitySystemComponent.IsValid())
	{
		return false;
	}

	if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	if (UHCAbilitySystemComponent* HCASC = CastChecked<UHCAbilitySystemComponent>(ActorInfo->AbilitySystemComponent))
	{
		if (HCASC->IsActivationGroupBlocked(ActivationGroup))
		{
			// TODO: might need to add "Ability.ActivateFail.ActivationGroup
			return false;
		}
		
		FGameplayTagContainer OwnerTags;
		HCASC->GetOwnedGameplayTags(OwnerTags);
		if (OwnerTags.HasAny(ActivationBlockedTags))
		{
			return false;
		}
	}

	return true;
}

void UHCGameplayAbility::SetCanBeCanceled(bool bCanBeCanceled)
{
	// return if the ability cannot be canceled and the activation group is replacable
	if (!bCanBeCanceled && (ActivationGroup == EHCAbilityActivationGroup::Exclusive_Replaceable))
	{
		UE_LOG(LogTemp, Warning, TEXT("The Ability [%s] cannot block cancelation because its activation group is replacable"), *GetName());
		return;
	}

	Super::SetCanBeCanceled(bCanBeCanceled);
}

void UHCGameplayAbility::OnGiveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	Super::OnGiveAbility(ActorInfo, Spec);
	K2_OnAbilityAdded();
	TryActivateAbilityOnSpawn(ActorInfo, Spec);
}

void UHCGameplayAbility::OnRemoveAbility(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec)
{
	K2_OnAbilityRemoved();

	Super::OnRemoveAbility(ActorInfo, Spec);
}

void UHCGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(ActorInfo->AvatarActor))
	{
		if (TObjectPtr<UHCEquipmentManagerComponent> EMC = Hero->EquipmentManagerComponent)
		{
			if (AbilityTags.HasTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack.1"))))
			{
				EMC->ShowWeaponFromSlotType(EHCEquipmentSlotType::MainWeapon);
			}
			else if (AbilityTags.HasTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack.2"))))
			{
				EMC->ShowWeaponFromSlotType(EHCEquipmentSlotType::SecondaryWeapon);
			}
			// TODO: might need to do this for items
		}

		if (UHCAbilitySystemComponent* HCASC = Hero->GetHCAbilitySystemComponent())
		{
			if (!HCASC->HasAnyMatchingGameplayTags(AbilityTags))
			{
				HCASC->AddLooseGameplayTags(AbilityTags);
			}
		}
	}
}

void UHCGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(ActorInfo->AvatarActor))
	{
		if (UHCAbilitySystemComponent* HCASC = Hero->GetHCAbilitySystemComponent())
		{
			FGameplayTagContainer BeforeOwnedTags;
			HCASC->GetOwnedGameplayTags(BeforeOwnedTags);
			for (const FGameplayTag& Tag : BeforeOwnedTags)
			{
				UE_LOG(LogTemp, Warning, TEXT("[UHCGameplayAbility::EndAbility] Before Owned tag: %s"), *Tag.ToString());
			}
			for (auto& Tag : AbilityTags)
			{
				HCASC->RemoveLooseGameplayTag(Tag);
			}
			FGameplayTagContainer AfterOwnedTags;
			HCASC->GetOwnedGameplayTags(AfterOwnedTags);
			for (const FGameplayTag& Tag : AfterOwnedTags)
			{
				UE_LOG(LogTemp, Warning, TEXT("[UHCGameplayAbility::EndAbility] After Owned tag: %s"), *Tag.ToString());
			}
		}
	}
	K2_OnAbilityEnded();
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

}

bool UHCGameplayAbility::CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	if (!Super::CheckCost(Handle, ActorInfo, OptionalRelevantTags) || !ActorInfo)
	{
		return false;
	}

	for (TObjectPtr<UHCAbilityCost> AdditionalCost: AdditionalCosts)
	{
		if (AdditionalCost)
		{
			if (!AdditionalCost->CheckCost(this, Handle, ActorInfo, OptionalRelevantTags))
			{
				return false;
			}
		}
	}

	return true;
}

void UHCGameplayAbility::ApplyCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) const
{
	Super::ApplyCost(Handle, ActorInfo, ActivationInfo);

	check(ActorInfo);

	// used to check if the ability actually hits the target since some costs are only spent on successful attempt
	auto CheckIfAbilityHitTarget = [&]()
	{
		if (ActorInfo->IsNetAuthority())
		{
			if (UHCAbilitySystemComponent* HCASC = Cast<UHCAbilitySystemComponent>(ActorInfo->AbilitySystemComponent.Get()))
			{
				FGameplayAbilityTargetDataHandle TargetData;
				// TODO: Do GetAbilityTargetData first
				HCASC->GetAbilityTargetData(Handle, ActivationInfo, TargetData);
				for (int32 TargetDataIdx = 0; TargetDataIdx < TargetData.Data.Num(); TargetDataIdx++)
				{
					if (UAbilitySystemBlueprintLibrary::TargetDataHasHitResult(TargetData, TargetDataIdx))
					{
						return true;
					}
				}
			}
		}

		return false;
	};

	// Pay Additional Cost
	for (TObjectPtr<UHCAbilityCost> AdditionalCost : AdditionalCosts)
	{
		if (AdditionalCost)
		{
			if (AdditionalCost->ShouldOnlyApplyCostOnHit())
			{

				if (!CheckIfAbilityHitTarget())
				{
					continue;
				}
			}

			AdditionalCost->ApplyCost(this, Handle, ActorInfo, ActivationInfo);
		}
	}
}

FGameplayEffectContextHandle UHCGameplayAbility::MakeEffectContext(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const
{
	FGameplayEffectContextHandle ContextHandle = Super::MakeEffectContext(Handle, ActorInfo);
	FGameplayEffectContext* Context = ContextHandle.Get();
	check(Context);
	check(ActorInfo);

	AActor* EffectCauser = ActorInfo ? ActorInfo->AvatarActor.Get() : nullptr;
	AActor* Instigator = ActorInfo ? ActorInfo->OwnerActor.Get() : nullptr;

	// WARNING: the code here ignores the Overrided FGameplayEffectContext class
	Context->AddInstigator(Instigator, EffectCauser);
	Context->AddSourceObject(GetSourceObject(Handle, ActorInfo));
	
	return FGameplayEffectContextHandle();
}

bool UHCGameplayAbility::DoesAbilitySatisfyTagRequirements(const UAbilitySystemComponent& AbilitySystemComponent, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	bool bBlocked = false;
	bool bMissing = false;

	UAbilitySystemGlobals& ASG = UAbilitySystemGlobals::Get();
	const FGameplayTag& BlockedTag = ASG.ActivateFailTagsBlockedTag;
	const FGameplayTag& MissingTag = ASG.ActivateFailTagsMissingTag;

	// Check if the tags are blocked
	if (AbilitySystemComponent.AreAbilityTagsBlocked(AbilityTags))
	{
		bBlocked = true;
	}

	// WARNING: we are not considering additional blocking tags.

	// check blocked and required tags activation from the UPROPERTY
	if (ActivationBlockedTags.Num() || ActivationRequiredTags.Num())
	{
		static FGameplayTagContainer AbilitySystemComponentTags;

		AbilitySystemComponentTags.Reset();
		AbilitySystemComponent.GetOwnedGameplayTags(AbilitySystemComponentTags);

		if (AbilitySystemComponentTags.HasAny(ActivationBlockedTags))
		{
			// also check for death tag
			if (OptionalRelevantTags && AbilitySystemComponentTags.HasTag(FGameplayTag::RequestGameplayTag(FName("State.Dead"))))
			{
				OptionalRelevantTags->AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.ActivationFail.IsDead")));
			}

			bBlocked = true;
		}

		if (!AbilitySystemComponentTags.HasAll(ActivationRequiredTags))
		{
			bMissing = true;
		}
	}

	if (SourceTags)
	{
		if (SourceBlockedTags.Num() || SourceRequiredTags.Num())
		{
			if (SourceTags->HasAny(SourceBlockedTags))
			{
				bBlocked = true;
			}

			if (!SourceTags->HasAll(SourceRequiredTags))
			{
				bMissing = true;
			}
		}
	}

	if (TargetTags)
	{
		if (TargetBlockedTags.Num() || TargetRequiredTags.Num())
		{
			if (TargetTags->HasAny(TargetBlockedTags))
			{
				bBlocked = true;
			}

			if (!TargetTags->HasAll(TargetRequiredTags))
			{
				bMissing = true;
			}
		}
	}

	if (bBlocked)
	{
		if (OptionalRelevantTags && BlockedTag.IsValid())
		{
			OptionalRelevantTags->AddTag(BlockedTag);
		}
		return false;
	}
	if (bMissing)
	{
		if (OptionalRelevantTags && MissingTag.IsValid())
		{
			OptionalRelevantTags->AddTag(MissingTag);
		}
		return false;
	}

	return true;
}

void UHCGameplayAbility::OnPawnAvatarSet()
{
	K2_OnPawnAvatarSet();
}
