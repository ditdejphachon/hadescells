// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCGameplayAbility_RangeWeapon.h"
#include <Heroes/HCHeroCharacter.h>
#include <Kismet/KismetMathLibrary.h>


UHCGameplayAbility_RangeWeapon::UHCGameplayAbility_RangeWeapon(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	LookAtLocation = FVector::ZeroVector;
}

void UHCGameplayAbility_RangeWeapon::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	m_Handle = Handle;
	m_ActorInfo = ActorInfo;
	m_ActivationInfo = ActivationInfo;

	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(ActorInfo->AvatarActor.Get()))
	{
		LookAtLocation = Hero->LookAtCursor();
	}

	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	K2_OnAbilityActivate();
}

void UHCGameplayAbility_RangeWeapon::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	if (IsEndAbilityValid(Handle, ActorInfo))
	{
		Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
	}
}

FTransform UHCGameplayAbility_RangeWeapon::DetermineSpawnTransform(const FVector& InInstigatorLocation, const FVector& InTargetLocation, const double SpawnPitch, const float InstigatorOffset, const float GroundOffset)
{
	FVector ProjectileDirection = (InTargetLocation - InInstigatorLocation).GetSafeNormal();
	FVector FinalLocation = (ProjectileDirection * InstigatorOffset) + (InInstigatorLocation + FVector(0.0, 0.0, InstigatorOffset));

	FRotator FinalRotator = UKismetMathLibrary::MakeRotFromX(ProjectileDirection);
	FinalRotator.Pitch = SpawnPitch;
	return FTransform(FinalRotator, FinalLocation);
}