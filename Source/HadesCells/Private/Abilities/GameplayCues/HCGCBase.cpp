// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/GameplayCues/HCGCBase.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Particles/ParticleSystem.h"
#include "GameFramework/Character.h"
#include <NiagaraFunctionLibrary.h>

AHCGCBase::AHCGCBase()
{
    CreatedParticles.Empty();
    RootActors.Empty();
}

void AHCGCBase::HandleGameplayCue(AActor* MyTarget, EGameplayCueEvent::Type EventType, const FGameplayCueParameters& Parameters)
{
    Super::HandleGameplayCue(MyTarget, EventType, Parameters);
    switch (EventType)
    {
    case EGameplayCueEvent::OnActive:
    case EGameplayCueEvent::Executed:
        SpawnEffects(MyTarget);
        break;
    case EGameplayCueEvent::Removed:
        RemoveEffects();
        break;
    default:
        UE_LOG(LogTemp, Warning, TEXT("[AHCGCBase::HandleGameplayCue] Invalid EventType"));
        break;
    }
}

void AHCGCBase::SpawnEffects(AActor* InActor)
{
    if (ACharacter* Actor = Cast<ACharacter>(InActor))
    {
        if (RootActors.Contains(Actor))
        {
            UE_LOG(LogTemp, Warning, TEXT("[AHCGCBase::SpawnEffects] Already spawned"));
            return;
        }

        if (USceneComponent* Mesh = Actor->GetMesh())
        {
            for (const FHCGCInfo& GCInfo : GameplayCueInfos)
            {
                if (GCInfo.NiagaraFX)
                {
                    UNiagaraFunctionLibrary::SpawnSystemAttached(GCInfo.NiagaraFX, Mesh, GCInfo.AttachPointNames, FVector(), FRotator(), GCInfo.AttachLocation, true, true);
                }

                if (GCInfo.ParticleFX)
                {
                    CreatedParticles.Add(UGameplayStatics::SpawnEmitterAttached(GCInfo.ParticleFX, Mesh, GCInfo.AttachPointNames, FVector(), FRotator(), GCInfo.FXScale, GCInfo.AttachLocation, true, EPSCPoolMethod::AutoRelease));
                  /*  UGameplayStatics::SpawnEmitterAttached(GCInfo.ParticleFX, Mesh, GCInfo.AttachPointNames, FVector(), FRotator(), GCInfo.FXScale, GCInfo.AttachLocation, true);*/
                }
            }

            RootActors.AddUnique(InActor);
        }  
    }
}

void AHCGCBase::OnOwnerDestroyed(AActor* DestroyedActor)
{
    RemoveEffects();
    Super::OnOwnerDestroyed(DestroyedActor);
}

void AHCGCBase::RemoveEffects()
{
    int NumCreatedParticles = CreatedParticles.Num();
    for (int i = 0; i < NumCreatedParticles; i++)
    {
        if (CreatedParticles.IsValidIndex(i) && CreatedParticles[i]->IsValidLowLevelFast())
        {
            CreatedParticles[i]->Deactivate();
            //CreatedParticles[i]->DestroyComponent();
        }
    }

    CreatedParticles.Empty();
    RootActors.Empty();
}
