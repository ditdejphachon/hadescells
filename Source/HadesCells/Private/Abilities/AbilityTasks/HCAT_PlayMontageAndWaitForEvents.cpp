// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/AbilityTasks/HCAT_PlayMontageAndWaitForEvents.h"
#include "Abilities/HCAbilitySystemComponent.h"
#include "GameFramework/Character.h"
#include "AbilitySystemGlobals.h"
#include "Characters/HCCharacterBase.h"


UHCAT_PlayMontageAndWaitForEvents::UHCAT_PlayMontageAndWaitForEvents(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Rate = 1.0f;
	bStopWhenAbilityEnds = true;
}

void UHCAT_PlayMontageAndWaitForEvents::Activate()
{
	if (Ability == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCAT_PlayMontageAndWaitForEvents::Activate] Ability is nullptr"));
		return;
	}
	
	bool bMontagePlayed = false;

	if (UHCAbilitySystemComponent* HCASC = GetTargetASC())
	{
		const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();

		if (UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance())
		{
			EventHandle = HCASC->AddGameplayEventTagContainerDelegate( 
				EventTags, 
				FGameplayEventTagMulticastDelegate::FDelegate::CreateUObject(this, &UHCAT_PlayMontageAndWaitForEvents::OnGameplayEvent)
			);

			//if (AHCCharacterBase* CurrentCharacter = Cast<AHCCharacterBase>(ActorInfo->AvatarActor))
			//{
			//	float DisplayTime = .5f; // Time in seconds for the message to be displayed on the screen
			//	int32 Key = -1; // Unique key to identify the message (use -1 for a new message)
			//	FColor TextColor = FColor::White; // Color of the text
			//	FString Message = TEXT("Your message here");

			//	if (GEngine)
			//	{
			//		GEngine->AddOnScreenDebugMessage(Key, DisplayTime, TextColor, Message);
			//	}
			//	CurrentCharacter->LookAtCursor();
			//}

			const float Duration = HCASC->PlayMontage(Ability, Ability->GetCurrentActivationInfo(), MontageToPlay, Rate, StartSection);

			if (Duration > 0.0f)
			{
				if (!ShouldBroadcastAbilityTaskDelegates())
				{
					UE_LOG(LogTemp, Warning, TEXT("[%s] Should not broadcast ability to task. return now."), *FString(__FUNCTION__));
					return;
				}

				CancelledHandle = Ability->OnGameplayAbilityCancelled.AddUObject(this, &UHCAT_PlayMontageAndWaitForEvents::OnAbilityCancelled);
				BlendingOutDelegate.BindUObject(this, &UHCAT_PlayMontageAndWaitForEvents::OnMontageBlendingOut);
				MontageEndedDelegate.BindUObject(this, &UHCAT_PlayMontageAndWaitForEvents::OnMontageEnded);

				AnimInstance->Montage_SetBlendingOutDelegate(BlendingOutDelegate, MontageToPlay);
				AnimInstance->Montage_SetEndDelegate(MontageEndedDelegate, MontageToPlay);

				if (ACharacter* OwningCharacter = Cast<ACharacter>(GetAvatarActor()))
				{
					OwningCharacter->SetAnimRootMotionTranslationScale(AnimRootMotionTranslationScale);
				}

				bMontagePlayed = true;
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("[%s] Duration is less than 0"), *FString(__FUNCTION__));
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[%s] AnimInstance is nullptr"), *FString(__FUNCTION__));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[%s] HCASC is nullptr"), *FString(__FUNCTION__));

	}

	if (!bMontagePlayed)
	{
		UE_LOG(
			LogTemp,
			Warning,
			TEXT("[%s] called in Ability [%s] failed to play montage [%s]; Task Instance Name [%s]."),
			*FString(__FUNCTION__),
			*Ability->GetName(),
			*GetNameSafe(MontageToPlay),
			*InstanceName.ToString()
		);

		if (ShouldBroadcastAbilityTaskDelegates())
		{
			UE_LOG(LogTemp, Warning, TEXT("[%s] Should not broadcast ability to task. return now."), *FString(__FUNCTION__));
			OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}

	SetWaitingOnAvatar();

}

void UHCAT_PlayMontageAndWaitForEvents::ExternalCancel()
{
	check(AbilitySystemComponent.Get());
	OnAbilityCancelled();
	Super::ExternalCancel();
}

void UHCAT_PlayMontageAndWaitForEvents::OnDestroy(bool AbilityEnded)
{
	/**
		 Note: Clearing montage end delegate isn't necessary
		 since its not a multicast and will be cleared when the next montage plays.
		 (If we are destroyed, it will detect this and not do anything)
		 This delegate, however, should be cleared as it is a multicast
	*/
	if (Ability)
	{
		Ability->OnGameplayAbilityCancelled.Remove(CancelledHandle);
		if (AbilityEnded && bStopWhenAbilityEnds)
		{
			StopPlayingMontage();
		}
	}

	if (UHCAbilitySystemComponent* HCASC = GetTargetASC())
	{
		HCASC->RemoveGameplayEventTagContainerDelegate(EventTags, EventHandle);
	}

	Super::OnDestroy(AbilityEnded);
}

UHCAT_PlayMontageAndWaitForEvents* UHCAT_PlayMontageAndWaitForEvents::PlayMontageAndWaitForEvent(UGameplayAbility* InOwingAbility, FName InTaskInstanceName, UAnimMontage* InMontageToPlay, FGameplayTagContainer InEventTags, float InRate, FName InStartSection, bool bStopWhenAbilityEnds, float InAnimRootMotionTranslationScale)
{
	UAbilitySystemGlobals::NonShipping_ApplyGlobalAbilityScaler_Rate(InRate);

	UHCAT_PlayMontageAndWaitForEvents* PlayEvent = NewAbilityTask<UHCAT_PlayMontageAndWaitForEvents>(InOwingAbility, InTaskInstanceName);
	PlayEvent->MontageToPlay = InMontageToPlay;
	PlayEvent->EventTags = InEventTags;
	PlayEvent->Rate = InRate;
	PlayEvent->StartSection = InStartSection;
	PlayEvent->AnimRootMotionTranslationScale = InAnimRootMotionTranslationScale;
	PlayEvent->bStopWhenAbilityEnds = bStopWhenAbilityEnds;

	return PlayEvent;
}

bool UHCAT_PlayMontageAndWaitForEvents::StopPlayingMontage()
{
	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();

	if (UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance())
	{
		if (AbilitySystemComponent.IsValid() && Ability)
		{
			if (AbilitySystemComponent->GetAnimatingAbility() == Ability && AbilitySystemComponent->GetCurrentMontage() == MontageToPlay)
			{
				if (FAnimMontageInstance* AnimMonInstance = AnimInstance->GetActiveInstanceForMontage(MontageToPlay))
				{
					AnimMonInstance->OnMontageBlendingOutStarted.Unbind();
					AnimMonInstance->OnMontageEnded.Unbind();
				}

				AbilitySystemComponent->CurrentMontageStop();

				return true;
			}
		}
	}

	return false;
}

UHCAbilitySystemComponent* UHCAT_PlayMontageAndWaitForEvents::GetTargetASC()
{
	return Cast<UHCAbilitySystemComponent>(AbilitySystemComponent);
}

void UHCAT_PlayMontageAndWaitForEvents::OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
	if (Ability && Ability->GetCurrentMontage() == MontageToPlay)
	{
		if (Montage == MontageToPlay)
		{
			if (AbilitySystemComponent.IsValid())
			{
				AbilitySystemComponent->ClearAnimatingAbility(Ability);

				if (ACharacter* OwningCharacter = Cast<ACharacter>(GetAvatarActor()))
				{
					OwningCharacter->SetAnimRootMotionTranslationScale(1.0f);
				}
			}
		}
	}

	if (ShouldBroadcastAbilityTaskDelegates())
	{
		if (bInterrupted)
		{
			OnInterrupted.Broadcast(FGameplayTag(), FGameplayEventData());
		}
		else
		{
			OnBlendOut.Broadcast(FGameplayTag(), FGameplayEventData());

		}
	}
	
}

void UHCAT_PlayMontageAndWaitForEvents::OnAbilityCancelled()
{
	if (StopPlayingMontage())
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}
}

void UHCAT_PlayMontageAndWaitForEvents::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (!bInterrupted)
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnComplete.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}

	EndTask();
}

void UHCAT_PlayMontageAndWaitForEvents::OnGameplayEvent(FGameplayTag EventTag, const FGameplayEventData* Payload)
{
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		FGameplayEventData TempData = *Payload;
		TempData.EventTag = EventTag;

		EventReceived.Broadcast(EventTag, TempData);
	}
}
