// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCGA_GrenageBaseWeapon.h"
#include "Characters/HCCharacterBase.h"
#include "Abilities/HCAbilitySystemComponent.h"
#include "Abilities/Tasks/AbilityTask_WaitTargetData.h"
#include <HCGameplayAbilityLibrary.h>
#include <Kismet/GameplayStatics.h>
#include <Equipment/Projectile/HCProjectileBase.h>

UHCGA_GrenageBaseWeapon::UHCGA_GrenageBaseWeapon(const FObjectInitializer& ObjectInitialize)
	: Super(ObjectInitialize)
{

}

void UHCGA_GrenageBaseWeapon::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	UHCGameplayAbility_RangeWeapon::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	/*if (!K2_CheckAbilityCooldown())
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed during checking ability cooldown. Canceling."));
		K2_CancelAbility();
		return;
	}*/

	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo()))
	{
		bool bCanActivateItem_1 = GetAbilityTags().HasTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.1")))
			&& !Char->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor1")));
		bool bCanActivateItem_2 = GetAbilityTags().HasTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.2")))
			&& !Char->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor2")));
		if (bCanActivateItem_1 || bCanActivateItem_2)
		{
			ExecuteAbility();
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::ActivateAbility] GameplayTags do not meet activation requirements. Canceling."));
			K2_CancelAbility();
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGA_ChargedRangeWeapon::ActivateAbility] Not HCCharacterBase. Canceling."));
		K2_CancelAbility();
	}

}

void UHCGA_GrenageBaseWeapon::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	UHCGameplayAbility_RangeWeapon::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);
}

void UHCGA_GrenageBaseWeapon::ExecuteAbility()
{
	UAbilityTask_WaitTargetData* AT = UAbilityTask_WaitTargetData::WaitTargetData(this, FName("None"), EGameplayTargetingConfirmation::Type::UserConfirmed, TargetActorClass);
	AGameplayAbilityTargetActor* TargetActor = nullptr;
	if (AT->BeginSpawningActor(this, TargetActorClass, TargetActor))
	{
		if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo()))
		{
			FGameplayAbilityTargetingLocationInfo LI = MakeTargetLocationInfoFromOwnerActor();
			LI.LiteralTransform.SetLocation((Char->GetActorForwardVector() * 1000.0f) + LI.LiteralTransform.GetLocation());
			TargetActor->StartLocation = LI;
			AT->FinishSpawningActor(this, TargetActor);
			AT->ValidData.Clear();
			AT->Cancelled.Clear();

			AT->ValidData.RemoveAll(this);
			AT->Cancelled.RemoveAll(this);

			AT->ValidData.AddDynamic(this, &UHCGA_GrenageBaseWeapon::OnReceiveValidTargetData);
			AT->Cancelled.AddDynamic(this, &UHCGA_GrenageBaseWeapon::OnCancelTargeting);
			AT->ReadyForActivation();
		}
	}
}

void UHCGA_GrenageBaseWeapon::OnReceiveValidTargetData(const FGameplayAbilityTargetDataHandle& Data)
{
	if (CommitAbilityCooldown(m_Handle, m_ActorInfo, m_ActivationInfo, true))
	{
		if (const FHitResult* HR = Data.Data.GetData()->Get()->GetHitResult())
		{
			AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo());

			if (Char == nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("[UHCGA_GrenageBaseWeapon::OnReceiveValidTargetData] Char is invalid"));
				K2_CancelAbility();
				return;
			}

			FTransform SpawnTransform = DetermineSpawnTransform(Char->GetActorLocation(), HR->Location, m_SpawnPitch, m_InstigatorOffset, m_GroundOffset);

			float CalculatedInitialSpeed = UHCGameplayAbilityLibrary::CalculateLaunchVelocity(SpawnTransform.GetLocation(), HR->Location, m_SpawnPitch);

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			AHCProjectileBase* SpawnedProjectile = Cast<AHCProjectileBase>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ProjectileClass, SpawnTransform, ESpawnActorCollisionHandlingMethod::AlwaysSpawn, Char));

			if (SpawnedProjectile == nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("[UHCGA_GrenageBaseWeapon::OnReceiveValidTargetData] SpawnedProjectile is invalid"));
				K2_CancelAbility();
				return;
			}

			SpawnedProjectile->CalculatedInitialSpeed = CalculatedInitialSpeed;
			SpawnedProjectile->SetOwner(Char);
			SpawnedProjectile->SetInstigator(Char);
			UGameplayStatics::FinishSpawningActor(SpawnedProjectile, SpawnTransform);

			if (UAbilitySystemComponent* ASC = Char->GetAbilitySystemComponent())
			{
				FGameplayTag Item1Tag = FGameplayTag::RequestGameplayTag(FName("Ability.Item.1"));
				FGameplayTag Item2Tag = FGameplayTag::RequestGameplayTag(FName("Ability.Item.2"));
				FGameplayTag Target1Tag = FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor1"));
				FGameplayTag Target2Tag = FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor2"));

				if (GetAbilityTags().HasTag(Item1Tag))
				{
					ASC->RemoveLooseGameplayTag(Target1Tag);
				}
				else if (GetAbilityTags().HasTag(Item2Tag))
				{
					ASC->RemoveLooseGameplayTag(Target2Tag);
				}
			}
			K2_EndAbility();
		}
	}
	else
	{
		K2_CancelAbility();
	}
}

void UHCGA_GrenageBaseWeapon::OnCancelTargeting(const FGameplayAbilityTargetDataHandle& Data)
{
	K2_CancelAbility();
}
