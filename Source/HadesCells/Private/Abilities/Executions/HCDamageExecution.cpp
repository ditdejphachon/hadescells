// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/Executions/HCDamageExecution.h"
#include "Abilities/HCAttributeSetBase.h"
#include <Abilities/HCGameplayEffectContext.h>

// TODO: create armor and damage struct
struct FDamageStatics
{
	FGameplayEffectAttributeCaptureDefinition BaseDamageDef;
	FGameplayEffectAttributeCaptureDefinition BaseShieldDef;

	FDamageStatics()
	{
		BaseDamageDef = FGameplayEffectAttributeCaptureDefinition(UHCAttributeSetBase::GetDamageAttribute(), EGameplayEffectAttributeCaptureSource::Source, true);
		BaseShieldDef = FGameplayEffectAttributeCaptureDefinition(UHCAttributeSetBase::GetShieldAttribute(), EGameplayEffectAttributeCaptureSource::Source, true);
	}
};

static FDamageStatics DamageStatics()
{
	static FDamageStatics Static;
	return Static;
}

UHCDamageExecution::UHCDamageExecution()
{
	RelevantAttributesToCapture.Add(DamageStatics().BaseDamageDef);
	RelevantAttributesToCapture.Add(DamageStatics().BaseShieldDef);

}

void UHCDamageExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	// TODO: get all nescessary spec
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	// TODO: might not need typed context for now since we are not doing any ability attenuations
	
	// Get source and target tag containers
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	// set aggregator evalution parameter
	FAggregatorEvaluateParameters EvaluateParameters;
	EvaluateParameters.SourceTags = SourceTags;
	EvaluateParameters.TargetTags = TargetTags;

	float Shield = 0.0f;
	float Damage = 0.0f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().BaseShieldDef, EvaluateParameters, Shield);
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().BaseDamageDef, EvaluateParameters, Damage);
	Shield = FMath::Max(Shield, 0.0f);
	Damage = FMath::Max(Damage, 0.0f);
	
	// TODO: calculate all damage manipulations (distance, hit from behind, material penetration, etc.)
	

	float NewShield = FMath::Max(Shield - Damage, 0.0f); // TODO: assign this
	float NewDamage = FMath::Max(Damage, 0.0f); // TODO: assign this

	// TODO: broadcast the result
	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(UHCAttributeSetBase::GetDamageAttribute(), EGameplayModOp::Override, NewDamage));
	//OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(UHCAttributeSetBase::GetShieldAttribute(), EGameplayModOp::Override, NewShield));
}
