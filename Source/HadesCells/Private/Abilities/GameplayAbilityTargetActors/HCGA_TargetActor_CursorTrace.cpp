// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/GameplayAbilityTargetActors/HCGA_TargetActor_CursorTrace.h"

#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include <Abilities/Ability/HCGameplayAbility.h>
#include <Heroes/HCHeroCharacter.h>
#include <Kismet/KismetMathLibrary.h>
#include "Input/HCInputConfig.h"
#include <InputMappingContext.h>
#include <Input/HCInputComponent.h>
#include "Components/DecalComponent.h"

AHCGA_TargetActor_CursorTrace::AHCGA_TargetActor_CursorTrace(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	m_BindingHandles.Empty();
	bDestroyOnConfirmation = true;
}

// TODO: receive an input ability tag on constructor to use it to check for which tag to add to ASC
void AHCGA_TargetActor_CursorTrace::StartTargeting(UGameplayAbility* Ability)
{
	Super::StartTargeting(Ability);
	if (UHCGameplayAbility* CurrentAbility = Cast<UHCGameplayAbility>(Ability))
	{
		m_CurrentAbility = CurrentAbility;
		BindToMouseClick();	

		if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(PrimaryPC->AcknowledgedPawn))
		{
			FGameplayTag CursorTag{};
			if (m_CurrentAbility->AbilityTags.HasTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.1"))))
			{
				CursorTag = FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor1"));
			}
			else if (m_CurrentAbility->AbilityTags.HasTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.2"))))
			{
				CursorTag = FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor2"));
			}
			Hero->GetHCAbilitySystemComponent()->AddLooseGameplayTag(CursorTag);
		}
	
	}
}

void AHCGA_TargetActor_CursorTrace::ConfirmTargetingAndContinue()
{
	FGameplayAbilityTargetData_SingleTargetHit* TargetData = new FGameplayAbilityTargetData_SingleTargetHit();
	TargetData->HitResult.Location = GetActorLocation();

	FGameplayAbilityTargetDataHandle TargetDataHandle;
	TargetDataHandle.Data.Add(TSharedPtr<FGameplayAbilityTargetData>(TargetData));
	TargetDataReadyDelegate.Broadcast(TargetDataHandle);
	UnbindFromMouseClick();
	Destroy();

}

bool AHCGA_TargetActor_CursorTrace::ShouldProduceTargetData() const
{
	return true;
}

void AHCGA_TargetActor_CursorTrace::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(StartLocation.SourceActor.Get()))
	{
		const APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (PC)
		{
			FVector StartMouseWorldLocation;
			FVector StartMouseWorldDirection;
			PC->DeprojectMousePositionToWorld(StartMouseWorldLocation, StartMouseWorldDirection);

			FVector CursorWorldLocation = StartMouseWorldLocation + (StartMouseWorldDirection * Hero->LookAtRayCastLength);

			FCollisionQueryParams QueryParams;
			QueryParams.AddIgnoredActor(this);

			FHitResult Hit;
			GetWorld()->LineTraceSingleByChannel(Hit, StartMouseWorldLocation, CursorWorldLocation, ECC_Visibility, QueryParams);

			Hit.Location.Z = 10.0f;
			SetActorLocation(Hit.Location);
		}
	}
}

void AHCGA_TargetActor_CursorTrace::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AHCGA_TargetActor_CursorTrace::SetDecalSize(UDecalComponent* DecalComp)
{
	if (DecalComp)
	{
		DecalComp->DecalSize = m_Size;
	}
}

void AHCGA_TargetActor_CursorTrace::OnMouseClickConfirm(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Warning, TEXT("OnMouseClickConfirm"));
	ConfirmTargetingAndContinue();
}

void AHCGA_TargetActor_CursorTrace::OnMouseClickCancel(const FInputActionValue& Value)
{
	UE_LOG(LogTemp, Warning, TEXT("OnMouseClickCancel"));
	CancelTargeting();
}

void AHCGA_TargetActor_CursorTrace::BindToMouseClick()
{
	if (APlayerController* PC = Cast<APlayerController>(PrimaryPC))
	{
		if (UHCInputComponent* EIC = Cast<UHCInputComponent>(PC->InputComponent))
		{
			AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(PC->AcknowledgedPawn);
			TObjectPtr<UHCPawnData> PawnData = Hero->GetPawnData();
			UHCInputConfig* InputConfig = PawnData->InputConfig.Get();
			if (InputConfig)
			{
				for (FHCInputAction IA : InputConfig->AbilityInputActions)
				{
					if (m_CurrentAbility->AbilityTags.HasTag(IA.InputTag))
					{
						FEnhancedInputActionEventBinding& ConfirmHandle = EIC->BindAction(IA.InputAction.Get(), ETriggerEvent::Started, this, FName("OnMouseClickConfirm"));
						m_BindingHandles.Add(ConfirmHandle.GetHandle());
						
					}

					if (m_CurrentAbility->AbilityTags.HasTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack.2"))))
					{
						FEnhancedInputActionEventBinding& CancelHandle = EIC->BindAction(IA.InputAction.Get(), ETriggerEvent::Started, this, FName("OnMouseClickCancel"));
						m_BindingHandles.Add(CancelHandle.GetHandle());
					}
				}
			}
		}
	}
}

void AHCGA_TargetActor_CursorTrace::UnbindFromMouseClick()
{
	if (APlayerController* PC = Cast<APlayerController>(PrimaryPC))
	{
		if (UHCInputComponent* EIC = Cast<UHCInputComponent>(PC->InputComponent))
		{
			for (int32& Handles : m_BindingHandles)
			{
				EIC->RemoveActionBindingForHandle(Handles);
			}
		}
	}
}
