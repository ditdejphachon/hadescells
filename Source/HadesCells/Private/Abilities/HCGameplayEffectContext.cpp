// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCGameplayEffectContext.h"


FHCGameplayEffectContext* FHCGameplayEffectContext::ExtractEffectContext(FGameplayEffectContextHandle InHandle)
{
	FGameplayEffectContext* BaseEffectContext = InHandle.Get();
	if (BaseEffectContext && BaseEffectContext->GetScriptStruct()->IsChildOf(FHCGameplayEffectContext::StaticStruct()))
	{
		return (FHCGameplayEffectContext*)BaseEffectContext;
	}
	return nullptr;
}

FGameplayEffectContext* FHCGameplayEffectContext::Duplicate() const
{
	FHCGameplayEffectContext* NewContext = new FHCGameplayEffectContext();
	*NewContext = *this;

	//if (const FHitResult* HitResult = GetHitResult())
	//{
	//	NewContext->AddHitResult(*HitResult, false);
	//}

	return NewContext;
}

UScriptStruct* FHCGameplayEffectContext::GetScriptStruct() const
{
	return FHCGameplayEffectContext::StaticStruct();
}

bool FHCGameplayEffectContext::NetSerialize(FArchive& Ar, UPackageMap* Map, bool& bOutSuccess)
{
	FGameplayEffectContext::NetSerialize(Ar, Map, bOutSuccess);
	return true;
}

const UPhysicalMaterial* FHCGameplayEffectContext::GetPhysicalMaterial() const
{
	if (const FHitResult* HitResultPtr = GetHitResult())
	{
		return HitResultPtr->PhysMaterial.Get();
	}
	return nullptr;
}
