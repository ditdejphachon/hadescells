// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCAbilitySystemComponent.h"
#include <Heroes/HCHeroCharacter.h>
#include "Equipment/HCEquipmentManagerComponent.h"

UHCAbilitySystemComponent::UHCAbilitySystemComponent(const FObjectInitializer& ObjectInitializer)
{
	InputPressedSpecHandles.Reset();
	InputReleasedSpecHandles.Reset();
	InputHeldSpecHandles.Reset();

	FMemory::Memset(ActivationGroupCounts, 0, sizeof(ActivationGroupCounts));
}


void UHCAbilitySystemComponent::AbilityInputTagPressed(const FGameplayTag& InputTag)
{
	if (InputTag.IsValid())
	{
		for (const FGameplayAbilitySpec& AbilitySpec : ActivatableAbilities.Items)
		{
			if (AbilitySpec.Ability && AbilitySpec.DynamicAbilityTags.HasTagExact(InputTag))
			{
				InputPressedSpecHandles.AddUnique(AbilitySpec.Handle);
				InputHeldSpecHandles.AddUnique(AbilitySpec.Handle);
			}
		}
	}
}

void UHCAbilitySystemComponent::AbilityInputTagReleased(const FGameplayTag& InputTag)
{
	if (InputTag.IsValid())
	{
		for (const FGameplayAbilitySpec& AbilitySpec : ActivatableAbilities.Items)
		{
			if (AbilitySpec.Ability && AbilitySpec.DynamicAbilityTags.HasTagExact(InputTag))
			{
				InputReleasedSpecHandles.AddUnique(AbilitySpec.Handle);
				InputHeldSpecHandles.Remove(AbilitySpec.Handle);
			}
		}
	}
}

bool UHCAbilitySystemComponent::IsActivationGroupBlocked(EHCAbilityActivationGroup Group) const
{
	bool bBlocked = false;

	switch (Group)
	{
		// Independent abilities are never blocked
		case EHCAbilityActivationGroup::Independent:break;
		case EHCAbilityActivationGroup::Exclusive_Replaceable:
		case EHCAbilityActivationGroup::Exclusive_Blocking:
			// Exclusive abilities can activate if nothing is blocking
			bBlocked = (ActivationGroupCounts[(uint8)EHCAbilityActivationGroup::Exclusive_Blocking] > 0);
			break;
		default:
			checkf(false, TEXT("[UHCAbilitySystemComponent::IsActivationGroupBlocked] Invalid ActivationGroup [%d]\n"), (uint8)Group);
			break;
	}

	return bBlocked;
}

void UHCAbilitySystemComponent::AddAbilityToActivationGroup(EHCAbilityActivationGroup Group, UHCGameplayAbility* HCAbility)
{
	check(HCAbility);
	check(ActivationGroupCounts[(uint8)Group] < INT32_MAX); // check if the array was properly initialized

	ActivationGroupCounts[(uint8)Group]++;

	const bool bReplicateCancelAbility = false;

	switch (Group)
	{
		case EHCAbilityActivationGroup::Independent: break; // Independent abilities do not cancel any other abilities
		case EHCAbilityActivationGroup::Exclusive_Replaceable:
		case EHCAbilityActivationGroup::Exclusive_Blocking:
			CancelActivationGroupAbilities(EHCAbilityActivationGroup::Exclusive_Replaceable, HCAbility, bReplicateCancelAbility);
			break;
		default:
			checkf(false, TEXT("AddAbilityToActivationGroup: Invalid ActivationGroup [%d]\n"), (uint8)Group);
			break;
	}

	// check exclusive count
	const int32 ExclusiveCount = ActivationGroupCounts[(uint8)EHCAbilityActivationGroup::Exclusive_Replaceable] + ActivationGroupCounts[(uint8)EHCAbilityActivationGroup::Exclusive_Blocking];
	if (ensure(ExclusiveCount > 1))
	{
		UE_LOG(LogTemp, Error, TEXT("[%s]: Multiple exclusive ability are running."), __FUNCTION__);
	}
}

void UHCAbilitySystemComponent::RemoveAbilityToActivationGroup(EHCAbilityActivationGroup Group, UHCGameplayAbility* HCAbility)
{
	check(HCAbility);
	check(ActivationGroupCounts[(uint8)Group > 0]);
	ActivationGroupCounts[(uint8)Group]--;
}

void UHCAbilitySystemComponent::CancelActivationGroupAbilities(EHCAbilityActivationGroup Group, UHCGameplayAbility* IgnoreHCAbility, bool bReplicateCancelAbility)
{
	auto ShouldCancelFunc = [this, Group, IgnoreHCAbility](const UHCGameplayAbility* HCAbility, FGameplayAbilitySpecHandle Handle)
	{
		return ((HCAbility->GetActivationGroup() == Group) && (HCAbility != IgnoreHCAbility));
	};

	CancelAbilitiesByFunc(ShouldCancelFunc, bReplicateCancelAbility);
}

void UHCAbilitySystemComponent::CancelAbilitiesByFunc(TShouldCancelAbilityFunc ShouldCancelFunc, bool bReplicatedCancelAbility)
{
	ABILITYLIST_SCOPE_LOCK();
	for (const FGameplayAbilitySpec& AbilitySpec: ActivatableAbilities.Items)
	{
		if (!AbilitySpec.IsActive())
		{
			continue;
		}

		UHCGameplayAbility* HCAbilityCDO = CastChecked<UHCGameplayAbility>(AbilitySpec.Ability);
		if (HCAbilityCDO->GetInstancingPolicy() != EGameplayAbilityInstancingPolicy::NonInstanced)
		{
			// Cancel all spawned instances, not CD0
			TArray<UGameplayAbility*> Instances = AbilitySpec.GetAbilityInstances();
			for (UGameplayAbility* AbilityInstance : Instances)
			{
				UHCGameplayAbility* HCAbiltyInstance = CastChecked<UHCGameplayAbility>(AbilityInstance);

				if (ShouldCancelFunc(HCAbiltyInstance, AbilitySpec.Handle))
				{
					if (HCAbiltyInstance->CanBeCanceled())
					{
						HCAbiltyInstance->CancelAbility(AbilitySpec.Handle, AbilityActorInfo.Get(), HCAbiltyInstance->GetCurrentActivationInfo(), bReplicatedCancelAbility);
					}
					else
					{
						UE_LOG(LogTemp, Warning, TEXT("Cannot Cancel Ability %s"), *HCAbiltyInstance->GetName());
					}
				}
			}
		}
		else
		{
			// Cancel the non-instanced ability CDO
			if (ShouldCancelFunc(HCAbilityCDO, AbilitySpec.Handle))
			{
				check(HCAbilityCDO->CanBeCanceled());
				HCAbilityCDO->CancelAbility(AbilitySpec.Handle, AbilityActorInfo.Get(), FGameplayAbilityActivationInfo(), bReplicatedCancelAbility);
			}
		}
	}
}

void UHCAbilitySystemComponent::GetAbilityTargetData(const FGameplayAbilitySpecHandle AbilityHandle, FGameplayAbilityActivationInfo ActivationInfo, FGameplayAbilityTargetDataHandle& OutTargetDataHandle)
{
	TSharedPtr<FAbilityReplicatedDataCache> ReplicatedData = AbilityTargetDataMap.Find(FGameplayAbilitySpecHandleAndPredictionKey(AbilityHandle, ActivationInfo.GetActivationPredictionKey()));

	if (ReplicatedData.IsValid())
	{
		OutTargetDataHandle = ReplicatedData->TargetData;
	}
}

void UHCAbilitySystemComponent::ProcessAbilityInput()
{
	//FGameplayTagContainer BlockedTags(FGameplayTag::RequestGameplayTag(FName("Gameplay.AbilityInputBlocked")));
	//if (HasAllMatchingGameplayTags(BlockedTags))
	//{
	//	ClearAbilityInput();
	//	return;
	//}
	AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(GetAvatarActor());
	static TArray<FGameplayAbilitySpecHandle> AbilitiesToActivates;
	AbilitiesToActivates.Reset();

	/*
	*	Process all held inputs
	*/
	for (const FGameplayAbilitySpecHandle& SpecHandle: InputHeldSpecHandles)
	{
		if (FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(SpecHandle))
		{
			if (AbilitySpec->Ability && !AbilitySpec->IsActive())
			{
				const UHCGameplayAbility* HCAbilityCD0 = CastChecked<UHCGameplayAbility>(AbilitySpec->Ability);
				if (HCAbilityCD0->GetActivationPolicy() == EHCAbilityActivationPolicy::WhileInputActive)
				{
					GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Green, *FString::Printf(TEXT("Calling AbilitySpecInputPressed with [%s]"), *AbilitySpec->GetDebugString()));
					AbilitySpecInputPressed(*AbilitySpec);
					AbilitiesToActivates.AddUnique(AbilitySpec->Handle);
				}
			}
		}
	}

	/*
	* Process all pressed inputs
	*/
	for (const FGameplayAbilitySpecHandle& SpecHandle : InputPressedSpecHandles)
	{
		if (FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(SpecHandle))
		{
			if (AbilitySpec->Ability)
			{
				if (Hero)
				{
					if (AbilitySpec->IsActive() 
						|| Hero->GetHCAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor1")))
						|| Hero->GetHCAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("Ability.WaitForTarget.Cursor2")))
						)
					{
						AbilitySpecInputPressed(*AbilitySpec);
					}
					else
					{
						const UHCGameplayAbility* HCAbilityCD0 = CastChecked<UHCGameplayAbility>(AbilitySpec->Ability);
						if (HCAbilityCD0->GetActivationPolicy() == EHCAbilityActivationPolicy::OnInputTriggered)
						{
							AbilitiesToActivates.AddUnique(AbilitySpec->Handle);
						}
					}
				}
			}
		}
	}

	/*
	* Try to activate all the abilities that are from presses and holds.
	* We do it all at once so that held inputs don't activate the ability
	* and then also send a input event to the ability because of the press
	*/
	for (const FGameplayAbilitySpecHandle& AbilitySpecHandle : AbilitiesToActivates)
	{
		TryActivateAbility(AbilitySpecHandle);
	}

	/*
	* Process all relesaed inputs
	*/
	for (const FGameplayAbilitySpecHandle& SpecHandle : InputReleasedSpecHandles)
	{
		if (FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(SpecHandle))
		{
			if (AbilitySpec->Ability)
			{
				GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, *FString::Printf(TEXT("Calling AbilitySpecInputReleased with [%s]"), *AbilitySpec->GetDebugString()));
				AbilitySpecInputReleased(*AbilitySpec);
			}
		}
	}

	// clear all cached ability handles
	InputPressedSpecHandles.Reset();
	InputReleasedSpecHandles.Reset();
}

bool UHCAbilitySystemComponent::MyHasAnyGameplayTags(const FGameplayTag& InTag)
{
	FGameplayTagContainer CurrentOwnedTags;
	GetOwnedGameplayTags(CurrentOwnedTags);
	for (const FGameplayTag& Tag : CurrentOwnedTags)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCAbilitySystemComponent::MyHasAnyGameplayTags] CurrentOwnedTags Owned tag: %s"), *Tag.ToString());
	}

	return CurrentOwnedTags.HasTag(InTag);
}

void UHCAbilitySystemComponent::MyAddLooseGameplayTags(const FGameplayTagContainer& GameplayTags, int32 Count)
{
	AddLooseGameplayTags(GameplayTags, Count);
}

void UHCAbilitySystemComponent::AbilitySpecInputPressed(FGameplayAbilitySpec& Spec)
{
	Super::AbilitySpecInputPressed(Spec);

	// We don't support UGameplayAbility::bReplicateInputDirectly.
	// Use replicated events instead so that the WaitInputPress ability task works.
	if (Spec.IsActive())
	{
		// Invoke the InputPressed event. This is not replicated here. If someone is listening, they may replicate the InputPressed event to the server.
		InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputPressed, Spec.Handle, Spec.ActivationInfo.GetActivationPredictionKey());
	}
}

void UHCAbilitySystemComponent::AbilitySpecInputReleased(FGameplayAbilitySpec& Spec)
{
	Super::AbilitySpecInputReleased(Spec);

	// We don't support UGameplayAbility::bReplicateInputDirectly.
	// Use replicated events instead so that the WaitInputRelease ability task works.
	if (Spec.IsActive())
	{
		// Invoke the InputReleased event. This is not replicated here. If someone is listening, they may replicate the InputReleased event to the server.
		InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputReleased, Spec.Handle, Spec.ActivationInfo.GetActivationPredictionKey());
	}
}

void UHCAbilitySystemComponent::ClearAbilityInput()
{
	InputPressedSpecHandles.Reset();
	InputReleasedSpecHandles.Reset();
	InputHeldSpecHandles.Reset();

}
