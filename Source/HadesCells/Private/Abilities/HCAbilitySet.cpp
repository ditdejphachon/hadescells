// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCAbilitySet.h"

#include "Abilities/HCAbilitySystemComponent.h"
#include "Abilities/Ability/HCGameplayAbility.h"
#include <Equipment/HCEquipmentInstance.h>


UHCAbilitySet::UHCAbilitySet(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UHCAbilitySet::GiveToAbililtySystem(UHCAbilitySystemComponent* HCASC, FHCAbilitySet_GrantedHandles* OutGrantedHandles, EHCEquipmentSlotType SlotType, int32 AbilityLevel, UObject* SourceObject) const
{
	check(HCASC);

	if (!HCASC->IsOwnerActorAuthoritative())
	{
		// must be authoritative to give abilities
		return;
	}
	// grant abilities to owner
	for (FHCAbilitySet_GameplayAbility GA : GrantedGameplayAbilities)
	{
		if (!IsValid(GA.Ability))
		{
			UE_LOG(LogTemp, Error, TEXT("The granted ability [%s] is not valid"), *GetNameSafe(this));
			continue;
		}
		FGameplayTagContainer AdditionalBlockingTags;
		switch (SlotType)
		{
			case EHCEquipmentSlotType::MainWeapon:
				GA.InputTag = FGameplayTag::RequestGameplayTag(FName("Ability.Attack.1"));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.1")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.2")));
				break;
			case EHCEquipmentSlotType::SecondaryWeapon:
				GA.InputTag = FGameplayTag::RequestGameplayTag(FName("Ability.Attack.2"));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.1")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.2")));
				break;
			case EHCEquipmentSlotType::MainSkill:
				GA.InputTag = FGameplayTag::RequestGameplayTag(FName("Ability.Item.1"));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.2")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack.1")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack.2")));
				break;
			case EHCEquipmentSlotType::SecondarySkill:
				GA.InputTag = FGameplayTag::RequestGameplayTag(FName("Ability.Item.2"));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Item.1")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack.1")));
				AdditionalBlockingTags.AddTag(FGameplayTag::RequestGameplayTag(FName("Ability.Attack.2")));
				break;
			default:
				UE_LOG(LogTemp, Warning, TEXT("[UHCAbilitySet::GiveToAbililtySystem] [WARNING] Invalid EHCEquipmentSlotType"));
				break;
		}

		// get CDO
		UHCGameplayAbility* GACDO = GA.Ability->GetDefaultObject<UHCGameplayAbility>();
		GACDO->AbilityTags.Reset();
		GACDO->AbilityTags.AddTag(GA.InputTag);
		GACDO->AdditionalBlockingTags = AdditionalBlockingTags;
		GACDO->SetBlockAbilityWithTags(AdditionalBlockingTags);

		// dynamically add cooldown tag
		{
			if (UGameplayEffect* GECooldown = GACDO->GetCooldownGameplayEffect())
			{
				FGameplayTag CooldownTag_1 = FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item1"));
				FGameplayTag CooldownTag_2 = FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item2"));
				GECooldown->InheritableOwnedTagsContainer.RemoveTag(CooldownTag_1);
				GECooldown->InheritableOwnedTagsContainer.RemoveTag(CooldownTag_2);
				if (SlotType == EHCEquipmentSlotType::MainSkill)
				{
					GECooldown->InheritableOwnedTagsContainer.AddTag(CooldownTag_1);
				}
				else if (SlotType == EHCEquipmentSlotType::SecondarySkill)
				{
					GECooldown->InheritableOwnedTagsContainer.AddTag(CooldownTag_2);
				}
			}
		}

		FGameplayAbilitySpec AbilitySpec(GACDO, AbilityLevel);
		AbilitySpec.SourceObject = SourceObject;
		AbilitySpec.DynamicAbilityTags.AddTag(GA.InputTag);

		const FGameplayAbilitySpecHandle AbilitySpecHandle = HCASC->GiveAbility(AbilitySpec);

		// store the handle in the list (use again for the removal)
		if (OutGrantedHandles)
		{
			OutGrantedHandles->AddAbilitySpecHandle(AbilitySpecHandle);
		}
	}
	
	// grant attribute
	for (const FHCAbilitySet_AttributeSet& AS : GrantedAttributes)
	{
		if (!IsValid(AS.AttributeSet))
		{
			UE_LOG(LogTemp, Error, TEXT("The granted attribute [%s] is not valid"), *GetNameSafe(this));
			continue;
		}

		UAttributeSet* ASOBJ = NewObject<UAttributeSet>(HCASC->GetOwner(), AS.AttributeSet);
		HCASC->AddAttributeSetSubobject(ASOBJ);

		if (OutGrantedHandles)
		{
			OutGrantedHandles->AddAttributeSet(ASOBJ);
		}
	}

	// grant gameplay effect
	for (const FHCAbilitySet_GameplayEffect& GE : GrantedGameplayEffects)
	{
		if (!IsValid(GE.GameplayEffect))
		{
			UE_LOG(LogTemp, Error, TEXT("The granted gameplay effect [%s] is not valid"), *GetNameSafe(this));
			continue;
		}

		const UGameplayEffect* GECDO = GE.GameplayEffect->GetDefaultObject<UGameplayEffect>();
		const FActiveGameplayEffectHandle GEHandle = HCASC->ApplyGameplayEffectToSelf(GECDO, GE.EffectLevel, HCASC->MakeEffectContext());
		if (OutGrantedHandles)
		{
			OutGrantedHandles->AddGameplayEffectHandle(GEHandle);
		}
	}

}

#pragma region FHCAbilitySet_GrantedHandles
void FHCAbilitySet_GrantedHandles::AddAbilitySpecHandle(const FGameplayAbilitySpecHandle& Handle)
{
	if (Handle.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("[FHCAbilitySet_GrantedHandles::AddAbilitySpecHandle] Handle: [%s]"), *Handle.ToString());
		AbilitySpecHandles.Add(Handle);
	}
}

void FHCAbilitySet_GrantedHandles::AddGameplayEffectHandle(const FActiveGameplayEffectHandle& Handle)
{
	if (Handle.IsValid())
	{
		GameplayEffectHandles.Add(Handle);
	}
}

void FHCAbilitySet_GrantedHandles::AddAttributeSet(UAttributeSet* AttributeSet)
{
	GrantedAttributeSets.Add(AttributeSet);
}

void FHCAbilitySet_GrantedHandles::TakeFromAbilitySystem(UHCAbilitySystemComponent* HCASC)
{
	UE_LOG(LogTemp, Warning, TEXT("[FHCAbilitySet_GrantedHandles::TakeFromAbilitySystem] Started Taking from ability away"));
	check(HCASC);

	if (!HCASC->IsOwnerActorAuthoritative())
	{
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("[FHCAbilitySet_GrantedHandles::TakeFromAbilitySystem] AbilitySpecHandles: [%d]"), AbilitySpecHandles.Num());


	for (const FGameplayAbilitySpecHandle& Handle : AbilitySpecHandles)
	{
		if (Handle.IsValid())
		{
			UE_LOG(LogTemp, Warning, TEXT("[FHCAbilitySet_GrantedHandles::TakeFromAbilitySystem] Handle: [%s]"), *Handle.ToString());
			HCASC->ClearAbility(Handle);
		}
	}

	for (const FActiveGameplayEffectHandle& Handle : GameplayEffectHandles)
	{
		if (Handle.IsValid())
		{
			HCASC->RemoveActiveGameplayEffect(Handle);
		}
	}

	for (UAttributeSet* Set : GrantedAttributeSets)
	{
		HCASC->RemoveSpawnedAttribute(Set);
	}

	AbilitySpecHandles.Reset();
	GameplayEffectHandles.Reset();
	GrantedAttributeSets.Reset();
}
#pragma endregion

