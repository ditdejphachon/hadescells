// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/HCGameplayAbility_FromEquipment.h"
#include "Equipment/HCEquipmentInstance.h"
#include <Kismet/GameplayStatics.h>
#include "Characters/HCCharacterBase.h"
#include <Kismet/KismetMathLibrary.h>

UHCGameplayAbility_FromEquipment::UHCGameplayAbility_FromEquipment(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
}

UHCEquipmentInstance* UHCGameplayAbility_FromEquipment::GetAssociatedEquipment() const
{
	if (FGameplayAbilitySpec* Spec = UGameplayAbility::GetCurrentAbilitySpec())
	{
		return Cast<UHCEquipmentInstance>(Spec->SourceObject.Get());
	}
	return nullptr;
}

#if WITH_EDITOR
EDataValidationResult UHCGameplayAbility_FromEquipment::IsDataValid(TArray<FText>& ValidationErrors)
{
	EDataValidationResult Result = Super::IsDataValid(ValidationErrors);

	if (InstancingPolicy == EGameplayAbilityInstancingPolicy::NonInstanced)
	{
		ValidationErrors.Add(NSLOCTEXT("HC", "EquipmentAbilityMustBeInstanced", "Equipment ability must be instanced"));
		Result = EDataValidationResult::Invalid;
	}

	return Result;
}
#endif

FVector UHCGameplayAbility_FromEquipment::GetLookAtHeroDirection()
{
	FVector HeroDirection{};

	AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo());
	ACharacter* Hero = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	if (Char && Hero)
	{
		FVector CharLocation = Char->GetActorLocation();
		FVector HeroLocation = Hero->GetActorLocation();

		HeroDirection = (HeroLocation - CharLocation).GetUnsafeNormal();
	}

	return HeroDirection;
}

FRotator UHCGameplayAbility_FromEquipment::GetLookAtHeroRotator()
{
	FRotator HeroLookAtRotator{};
	AHCCharacterBase* Char = Cast<AHCCharacterBase>(GetAvatarActorFromActorInfo());
	ACharacter* Hero = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	if (Char && Hero)
	{
		FVector CharLocation = Char->GetActorLocation();
		FVector HeroLocation = Hero->GetActorLocation();
		HeroLookAtRotator = UKismetMathLibrary::FindLookAtRotation(CharLocation, HeroLocation);
	}

	return HeroLookAtRotator;
}