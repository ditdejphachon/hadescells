// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/HCHealthComponent.h"

#include "Abilities/HCAbilitySystemComponent.h"
#include "Abilities/HCAttributeSetBase.h"
#include "GameplayEffectExtension.h"

UHCHealthComponent::UHCHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	AbilitySystemComponent = nullptr;
}

void UHCHealthComponent::InitializeWithAbilitySystem(UHCAbilitySystemComponent* InASC)
{
	AActor* Owner = GetOwner();
	check(Owner);

	if (AbilitySystemComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UHCHealthComponent: Health component for owner [%s] has already been initialized with an ability system."), *GetNameSafe(Owner));
		return;
	}

	AbilitySystemComponent = InASC;
	if (!AbilitySystemComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UHCHealthComponent: Cannot initialize health component for owner [%s] with NULL ability system."), *GetNameSafe(Owner));
		return;
	}

	HealthSet = AbilitySystemComponent->GetSet<UHCAttributeSetBase>();
	if (!HealthSet)
	{
		UE_LOG(LogTemp, Error, TEXT("UHCHealthComponent: Cannot initialize health component for owner [%s] with NULL health set on the ability system."), *GetNameSafe(Owner));
		return;
	}

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetHealthAttribute()).AddUObject(this, &UHCHealthComponent::HandleHealthChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetMaxHealthAttribute()).AddUObject(this, &UHCHealthComponent::HandleMaxHealthChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetShieldAttribute()).AddUObject(this, &UHCHealthComponent::HandleShieldChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetMaxShieldAttribute()).AddUObject(this, &UHCHealthComponent::HandleMaxShieldChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetPostureAttribute()).AddUObject(this, &UHCHealthComponent::HandlePostureChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetMaxPostureAttribute()).AddUObject(this, &UHCHealthComponent::HandleMaxPostureChanged);

	// TODO: implement out of health delegate in attribute class and bind it here

	OnHealthChanged.Broadcast(this, HealthSet->GetHealth(), HealthSet->GetHealth(), nullptr);
	OnMaxHealthChanged.Broadcast(this, HealthSet->GetMaxHealth(), HealthSet->GetMaxHealth(), nullptr);
	OnShieldChanged.Broadcast(this, HealthSet->GetShield(), HealthSet->GetShield(), nullptr);
	OnMaxShieldChanged.Broadcast(this, HealthSet->GetMaxShield(), HealthSet->GetMaxShield(), nullptr);
}

void UHCHealthComponent::UninitializeFromAbilitySystem()
{
	ClearGameplayTags();

	if (HealthSet)
	{
		// TODO: remove OnOutOfHealth delegate
	}

	HealthSet = nullptr;
	AbilitySystemComponent = nullptr;
}

float UHCHealthComponent::GetHealth() const
{
	return (HealthSet ? HealthSet->GetHealth() : 0.0f);
}

float UHCHealthComponent::GetMaxHealth() const
{
	return (HealthSet ? HealthSet->GetMaxHealth() : 0.0f);
}

float UHCHealthComponent::GetHealthNormalized() const
{
	if (HealthSet)
	{
		const float Health = HealthSet->GetHealth();
		const float MaxHealth = HealthSet->GetMaxHealth();

		return ((MaxHealth > 0.0f) ? (Health / MaxHealth) : 0.0f);
	}

	return 0.0f;
}

float UHCHealthComponent::GetShield() const
{
	return (HealthSet ? HealthSet->GetShield() : 0.0f);
}

float UHCHealthComponent::GetMaxShield() const
{
	return (HealthSet ? HealthSet->GetMaxShield() : 0.0f);
}

float UHCHealthComponent::GetShieldNormalized() const
{
	if (HealthSet)
	{
		const float Shield = HealthSet->GetShield();
		const float MaxShield = HealthSet->GetMaxShield();

		return ((MaxShield > 0.0f) ? (Shield / MaxShield) : 0.0f);
	}

	return 0.0f;
}

float UHCHealthComponent::GetPosture() const
{
	return (HealthSet ? HealthSet->GetPosture() : 0.0f);
}

float UHCHealthComponent::GetMaxPosture() const
{
	return (HealthSet ? HealthSet->GetMaxPosture() : 0.0f);
}

float UHCHealthComponent::GetPostureNormalized() const
{
	if (HealthSet)
	{
		const float Posture = HealthSet->GetPosture();
		const float MaxPosture = HealthSet->GetMaxPosture();

		return ((MaxPosture > 0.0f) ? (Posture / MaxPosture) : 0.0f);
	}

	return 0.0f;
}

void UHCHealthComponent::ClearDelegates()
{
	OnHealthChanged.Clear();
	OnMaxHealthChanged.Clear();
	OnShieldChanged.Clear();
	OnMaxShieldChanged.Clear();
	OnPostureChanged.Clear();
	OnMaxPostureChanged.Clear();
}

void UHCHealthComponent::OnUnregister()
{
	UninitializeFromAbilitySystem();

	Super::OnUnregister();
}

void UHCHealthComponent::ClearGameplayTags()
{
	if (AbilitySystemComponent)
	{
		// TODO: clear deaths ability tags
	}
}

static AActor* GetInstigatorFromAttrChangeData(const FOnAttributeChangeData& ChangeData)
{
	if (ChangeData.GEModData != nullptr)
	{
		const FGameplayEffectContextHandle& EffectContext = ChangeData.GEModData->EffectSpec.GetEffectContext();
		return EffectContext.GetOriginalInstigator();
	}

	return nullptr;
}

void UHCHealthComponent::HandleHealthChanged(const FOnAttributeChangeData& ChangeData)
{
	OnHealthChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}

void UHCHealthComponent::HandleMaxHealthChanged(const FOnAttributeChangeData& ChangeData)
{
	OnMaxHealthChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}

void UHCHealthComponent::HandleOutOfHealth(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec& DamageEffectSpec, float DamageMagnitude)
{
	// TODO: add out-of-health gameplay tag eg. play death scene and shits
}

void UHCHealthComponent::HandleShieldChanged(const FOnAttributeChangeData& ChangeData)
{
	OnShieldChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}

void UHCHealthComponent::HandleMaxShieldChanged(const FOnAttributeChangeData& ChangeData)
{
	OnMaxShieldChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}

void UHCHealthComponent::HandleOutOfShield(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec& DamageEffectSpec, float DamageMagnitude)
{
	// TODO: add out-of-shield gameplay tag eg. play shield break anim

}

void UHCHealthComponent::HandlePostureChanged(const FOnAttributeChangeData& ChangeData)
{
	OnPostureChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}

void UHCHealthComponent::HandleMaxPostureChanged(const FOnAttributeChangeData& ChangeData)
{
	OnMaxPostureChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}

void UHCHealthComponent::HandleOutOfPosture(AActor* DamageInstigator, AActor* DamageCauser, const FGameplayEffectSpec& DamageEffectSpec, float DamageMagnitude)
{
}


