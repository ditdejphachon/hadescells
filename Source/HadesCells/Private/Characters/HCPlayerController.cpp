// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/HCPlayerController.h"
#include "Characters/HCCharacterBase.h"
#include "Characters/HCPlayerState.h"
#include "Abilities/HCAbilitySystemComponent.h"

AHCPlayerController::AHCPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	
}

//UAbilitySystemComponent* AHCPlayerController::GetAbilitySystemComponent() const
//{
//	return GetHCAbilitySystemComponent();
//}

void AHCPlayerController::PreProcessInput(const float DeltaTime, const bool bGamePaused)
{
	Super::PreProcessInput(DeltaTime, bGamePaused);
}

void AHCPlayerController::PostProcessInput(const float DeltaTime, const bool bGamePaused)
{
	AHCPlayerState* HCPS = Cast<AHCPlayerState>(CastChecked<AHCPlayerState>(PlayerState, ECastCheckedType::NullAllowed));
	if (HCPS)
	{
		if (UHCAbilitySystemComponent* HCASC = HCPS->GetHCAbilitySystemComponent())
		{
			HCASC->ProcessAbilityInput();
		}
	}
	
	Super::PostProcessInput(DeltaTime, bGamePaused);
}

void AHCPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AHCPlayerState* HCPS = InPawn->GetPlayerState<AHCPlayerState>();

	if (HCPS)
	{
		HCPS->GetAbilitySystemComponent()->InitAbilityActorInfo(HCPS, InPawn);
	}
}
