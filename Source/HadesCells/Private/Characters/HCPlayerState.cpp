// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/HCPlayerState.h"
#include "Abilities/HCAbilitySystemComponent.h"
#include <Abilities/HCAbilitySet.h>
#include <Characters/HCPawnData.h>
#include <Equipment/HCEquipmentInstance.h>


AHCPlayerState::AHCPlayerState(const FObjectInitializer& ObjectInitializer)
{
	AbilitySystemComponent = ObjectInitializer.CreateDefaultSubobject<UHCAbilitySystemComponent>(this, TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);	

	bHasSetPawnData = false;
}

UAbilitySystemComponent* AHCPlayerState::GetAbilitySystemComponent() const
{
	return GetHCAbilitySystemComponent();
}

void AHCPlayerState::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	//check(AbilitySystemComponent);
	//AbilitySystemComponent->InitAbilityActorInfo(this, GetPawn());
}

void AHCPlayerState::SetPawnData(const UHCPawnData* InPawnData)
{
	checkf(AbilitySystemComponent, TEXT("[AHCCharacterBase::InitializeAttributes] AbilitySystemComponent is null ptr"));
	checkf(InPawnData, TEXT("[AHCCharacterBase::InitializeAttributes] PawnData is null ptr"));

	//if (InPawnData->DefaultAttributes)
	//{
	//	FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
	//	EffectContext.AddSourceObject(this);

	//	FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(InPawnData->DefaultAttributes,1, EffectContext);
	//	if (NewHandle.IsValid())
	//	{
	//		FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent.Get());
	//	}
	//}

	if (!InPawnData->AbilitySets.IsEmpty())
	{
		for (const UHCAbilitySet* AbilitySet : InPawnData->AbilitySets)
		{
			if (AbilitySet)
			{
				AbilitySet->GiveToAbililtySystem(AbilitySystemComponent, nullptr, EHCEquipmentSlotType::None, 1, GetOwner());
			}
		}

		bHasSetPawnData = true;
	}
}
