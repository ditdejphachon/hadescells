// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/HCCharacterBase.h"
#pragma region includes
#include "Abilities/HCAbilitySystemComponent.h"
#include "Abilities/HCAttributeSetBase.h"
#include "Components/CapsuleComponent.h"
#include "HCCharacterMovementComponent.h"
#include "Abilities/HCAbilitySet.h"
#include <Characters/HCPlayerState.h>
#include "Abilities/GameplayAbilityTypes.h"
#include "Animations/AnimNotifiers/HCANS_JumpSection.h"
#include <Kismet/GameplayStatics.h>
#include <Characters/HCPlayerController.h>

#include "Particles/ParticleSystemComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include <Equipment/HCEquipmentManagerComponent.h>
#include "Components/WidgetComponent.h"
#include "UIs/HCW_DamagePopUp.h"
#include <Heroes/HCHeroCharacter.h>
#include <HadesCells/HadesCellsGameMode.h>
#pragma endregion

// Sets default values
AHCCharacterBase::AHCCharacterBase(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UHCCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Overlap);
	GetMesh()->bReceivesDecals = false;

	HealthComponent = CreateDefaultSubobject<UHCHealthComponent>(TEXT("HealthComponent"));
	OnHealthChangedHandle = HealthComponent->OnHealthChanged.AddUObject(this, &AHCCharacterBase::HandleHealthChanged);
	OnShieldChangedHandle = HealthComponent->OnShieldChanged.AddUObject(this, &AHCCharacterBase::HandleShieldChanged);
	OnPostureChangedHandle = HealthComponent->OnPostureChanged.AddUObject(this, &AHCCharacterBase::HandlePostureChanged);
	OnMaxHealthChangedHandle = HealthComponent->OnMaxHealthChanged.AddUObject(this, &AHCCharacterBase::HandleHealthChanged);
	OnMaxShieldChangedHandle = HealthComponent->OnMaxShieldChanged.AddUObject(this, &AHCCharacterBase::HandleShieldChanged);
	OnMaxPostureChangedHandle = HealthComponent->OnMaxPostureChanged.AddUObject(this, &AHCCharacterBase::HandlePostureChanged);

	ProgressionSystemComponent = CreateDefaultSubobject<UHCProgressionSystemComponent>(TEXT("ProgressionSystemComponent"));
	OnGoldChangedHandle = ProgressionSystemComponent->OnGoldChanged.AddUObject(this, &AHCCharacterBase::HandleGoldChanged);
	OnExpChangedHandle = ProgressionSystemComponent->OnExpChanged.AddUObject(this, &AHCCharacterBase::HandleExpChanged);
	OnMaxExpChangedHandle = ProgressionSystemComponent->OnMaxExpChanged.AddUObject(this, &AHCCharacterBase::HandleExpChanged);

	EquipmentManagerComponent = CreateDefaultSubobject<UHCEquipmentManagerComponent>(TEXT("EquipmentManagerComponent"));

	AbilitySystemComponent = ObjectInitializer.CreateDefaultSubobject<UHCAbilitySystemComponent>(this, TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->OnAnyGameplayEffectRemovedDelegate().AddUObject(this, &AHCCharacterBase::OnGameplayEffectRemoved);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Root")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Freeze")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Slow")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Oil")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Poison")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Bleed")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Shock")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);
	AbilitySystemComponent->RegisterGameplayTagEvent(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Fire")), EGameplayTagEventType::NewOrRemoved).AddUObject(this, &AHCCharacterBase::HandleTagChanged);

	bHasDied = false;

	if (USkeletalMeshComponent* MySkeletalMeshComp = GetMesh())
	{
		MySkeletalMeshComp->bRenderCustomDepth = true;
	}

	DamagePopUpComps.Empty();
}


UAbilitySystemComponent* AHCCharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent.Get();
}

#pragma region Attribute Set interfaces
bool AHCCharacterBase::IsAlive() const
{
	if (HealthComponent)
	{
		return HealthComponent->GetHealth() > 0.0f;
	}

	return false;
}

float AHCCharacterBase::GetMoveSpeed() const
{
	if (AttributeSetBase)
	{
		return AttributeSetBase->GetMoveSpeed();
	}

	return 0.0f;
}

float AHCCharacterBase::GetMoveSpeedBaseValue() const
{
	if (AttributeSetBase)
	{
		return AttributeSetBase->GetMoveSpeedAttribute().GetGameplayAttributeData(AttributeSetBase.Get())->GetBaseValue();
	}

	return 0.0f;
}

int32 AHCCharacterBase::GetCharacterLevel() const
{
	if (AttributeSetBase)
	{
		return static_cast<int32>(AttributeSetBase->GetCharacterLevel());
	}

	return 0;
}

#pragma endregion ~Attribute set interfaces

void AHCCharacterBase::HandleCharacterLevelChanged(const FOnAttributeChangeData& ChangeData)
{
}

void AHCCharacterBase::Die()
{
	UE_LOG(LogTemp, Warning, TEXT("[AHCCharacterBase::Die] Start dying"));

	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCCharacterBase::Die] Already Dead"));
		return;
	}

	bHasDied = true;

	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->RemoveAllGameplayCues();

		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetCharacterMovement()->GravityScale = 0;
		GetCharacterMovement()->Velocity = FVector(0);

		if (DeathMontage)
		{
			if (UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance())
			{
				//AnimInstance->OnMontageEnded.AddUniqueDynamic(this, &AHCCharacterBase::FinishDying);
				AnimInstance->Montage_Play(DeathMontage);
				//DeathMontage->SequenceLength // TODO: interesting. use this
			}
		}
	}
}

void AHCCharacterBase::FinishDying()
{
	{
		// Set the timer with a lambda function
		FTimerHandle TimerHandle;
		FTimerDelegate TimerDelegate;
		TimerDelegate.BindLambda([&]() {
			if (UWorld* World = GetWorld())
			{
				if (AHadesCellsGameMode* GM = Cast<AHadesCellsGameMode>(World->GetAuthGameMode()))
				{
					GM->HandleEnemyKilled();
				}
				Destroy();
			}
			});

		float Delay = DeathAnimLength;  // Delay in seconds
		GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, Delay, false);
	}

	ClearTimerHandles();
}

// Called when the game starts or when spawned
void AHCCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	AHCPlayerState* HCPS = Cast<AHCPlayerState>(GetPlayerState());

	if (HCPS)
	{
		if (HCPS->bHasSetPawnData)
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCCharacterBase::BeginPlay] Pawn data already setted. Returning..."));
			return;
		}
		AbilitySystemComponent = HCPS->GetHCAbilitySystemComponent();
		HCPS->SetPawnData(PawnData);
	}

	InitializeAttributes();
}

void AHCCharacterBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	ClearTimerHandles();
}

void AHCCharacterBase::InitializeAttributes()
{
	HealthComponent->InitializeWithAbilitySystem(AbilitySystemComponent);
	ProgressionSystemComponent->InitializeWithAbilitySystem(AbilitySystemComponent);
	AttributeSetBase = AbilitySystemComponent->GetSet<UHCAttributeSetBase>();
}

void AHCCharacterBase::HandleHealthChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
}

void AHCCharacterBase::HandleShieldChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
}

void AHCCharacterBase::HandlePostureChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
}

void AHCCharacterBase::HandleTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	auto CancelOtherAblilty = [&](TArray<FName> AbilityToCancelNames, TArray<FName> AbilityToIgnoreNames) {
		FGameplayTagContainer AbilityTagsToCancel;

		for (const FName& AbilityToCancel : AbilityToCancelNames)
		{
			AbilityTagsToCancel.AddTag(FGameplayTag::RequestGameplayTag(AbilityToCancel));
		}

		FGameplayTagContainer AbilityTagsToIgnore;
		for (const FName& AbilityToIgnore : AbilityToIgnoreNames)
		{
			AbilityTagsToIgnore.AddTag(FGameplayTag::RequestGameplayTag(AbilityToIgnore));
		}

		AbilitySystemComponent->CancelAbilities(&AbilityTagsToCancel, &AbilityTagsToIgnore);
	};

	if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun"))))
	{
		if (NewCount > 0)
		{
			TArray<FName> AbilityToCancelNames;
			AbilityToCancelNames.Add(FName("Ability"));
			TArray<FName> AbilityToIgnoreNames;
			AbilityToIgnoreNames.Add(FName("Ability.NotCanceledBy.Stun"));
			CancelOtherAblilty(AbilityToCancelNames, AbilityToIgnoreNames);
			GetCharacterMovement()->bUseControllerDesiredRotation = false;
		}
		else
		{
			GetCharacterMovement()->bUseControllerDesiredRotation = true;

			UGameplayEffect* RefillEffect = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("PostureRefillEffect")));
			RefillEffect->DurationPolicy = EGameplayEffectDurationType::Instant;

			FGameplayModifierInfo ModifierInfo;
			ModifierInfo.Attribute = AttributeSetBase->GetPostureAttribute();
			ModifierInfo.ModifierOp = EGameplayModOp::Override;
			ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(HealthComponent->GetMaxPosture()));

			RefillEffect->Modifiers.Add(ModifierInfo);

			FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
			FActiveGameplayEffectHandle ActiveEffectHandle = AbilitySystemComponent->ApplyGameplayEffectToSelf(RefillEffect, 1.0f, EffectContext);
		}
	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Root"))))
	{
		if (NewCount > 0)
		{
			TArray<FName> AbilityToCancelNames;
			AbilityToCancelNames.Add(FName("Ability"));
			TArray<FName> AbilityToIgnoreNames;
			AbilityToIgnoreNames.Add(FName("Ability.NotCanceledBy.Root"));
			GetCharacterMovement()->bUseControllerDesiredRotation = false;
		}
		else
		{
			GetCharacterMovement()->bUseControllerDesiredRotation = true;
		}
	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Freeze"))))
	{

	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Slow"))))
	{

	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Oil"))))
	{

	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Poison"))))
	{

	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Bleed"))))
	{

	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Shock"))))
	{

	}
	else if (CallbackTag.MatchesTagExact(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Fire"))))
	{

	}
}

void AHCCharacterBase::HandleGoldChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
}

void AHCCharacterBase::HandleExpChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
}

void AHCCharacterBase::ShowDamagePopUp(const float& InDamage, const FGameplayTagContainer& AdditionalTagContainer)
{
	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCCharacterBase::ShowDamagePopUp] already dead. not showing DMG pop up"));
		return;
	}

	StartFlashing();

	UWidgetComponent* DamagePopUpComp = NewObject<UWidgetComponent>(this);
	if (DamagePopUpComp)
	{
		DamagePopUpComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		DamagePopUpComp->RegisterComponent();
		FAttachmentTransformRules AttachRules(
			EAttachmentRule::KeepRelative, // Location rule
			EAttachmentRule::KeepWorld,
			EAttachmentRule::KeepRelative, // Scale rule
			false // Weld simulated bodies
		);
		DamagePopUpComp->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);

		auto RandomPosition = [&](int InMinValue, int InMaxValue, float InNoiseFrequency) -> int {
			float RandomSeed = FMath::FRand();
			float PerlinNoiseValue = FMath::PerlinNoise1D(RandomSeed * InNoiseFrequency);

			// Remap the Perlin noise value from the range [-1, 1] to the range [0, 1]
			float RemappedPerlinNoiseValue = (PerlinNoiseValue + 1.0f) / 2.0f;

			int RandomIntWithNoise = FMath::Lerp(InMinValue, InMaxValue, RemappedPerlinNoiseValue);
			return RandomIntWithNoise;
		};

		DamagePopUpComp->SetRelativeLocation(FVector(RandomPosition(10, 100, PN_DamagePopUp_XY), RandomPosition(10, 100, PN_DamagePopUp_XY), RandomPosition(50, 210, PN_DamagePopUp_Z)));

		DamagePopUpComp->SetWidgetSpace(EWidgetSpace::Screen);

		AHCPlayerController* PC = Cast<AHCPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		if (PC)
		{
			if (UHCW_DamagePopUp* DamagePopUp = CreateWidget<UHCW_DamagePopUp>(PC, WBP_DamagePopUp))
			{
				DamagePopUp->bIsHero = Cast<AHCHeroCharacter>(this) ? true : false;
				DamagePopUp->SetDamageText(InDamage, AdditionalTagContainer);
				DamagePopUpComp->SetWidget(DamagePopUp);
			}
		}

		DamagePopUpComps.Add(DamagePopUpComp);

		GetWorld()->GetTimerManager().SetTimer(DamageCompHandle, [&]() {
			//GetWorld()->GetTimerManager().ClearTimer(DamageCompHandle);
			UE_LOG(LogTemp, Warning, TEXT("Destroying Damage Component"));
			if (DamagePopUpComps.IsValidIndex(0))
			{
				UWidgetComponent* _DamagePopUpComp = DamagePopUpComps[0];
				if (_DamagePopUpComp != nullptr && _DamagePopUpComp->IsActive())
				{
					_DamagePopUpComp->Deactivate();
				}

				DamagePopUpComps.RemoveAt(0);
			}
		},
		DamagePopUpLifeSpan, false);
	}
}

void AHCCharacterBase::KnockBack(float InDistance, float InSpeed, FVector InDirection)
{
	if (AbilitySystemComponent)
	{
		FGameplayTagContainer RootTags{ FGameplayTag::RequestGameplayTag(FName("State.Debuff.Root")) };
		if (AbilitySystemComponent->HasAnyMatchingGameplayTags(RootTags))
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCCharacterBase::KnockBack] Still Rooted. Cannot be knocked back"));
			return;
		}
	}

	KnockBackDistanceTraveled = 0.0f;
	KnockBackDistance = InDistance;
	KnockBackDirection = InDirection;
	KnockBackSpeed = InSpeed;
	StartKnockBack();
}

void AHCCharacterBase::StartKnockBack()
{
	float DeltaSeconds = GetWorld()->GetDeltaSeconds();
	FVector KnockBackVelocity;
	KnockBackVelocity = KnockBackDirection * KnockBackSpeed;
	KnockBackVelocity.Z = 0.0f;

	AddActorWorldOffset(KnockBackVelocity * DeltaSeconds, true, false);

	KnockBackDistanceTraveled += (KnockBackVelocity * DeltaSeconds).Size();

	if (KnockBackDistanceTraveled >= KnockBackDistance)
	{
		GetWorldTimerManager().ClearTimer(KnockBackTimerHandle);
	}
	else
	{
		GetWorldTimerManager().SetTimer(KnockBackTimerHandle, this, &AHCCharacterBase::StartKnockBack, DeltaSeconds, false);
	}
}

void AHCCharacterBase::StartFlashing()
{
	MySetCustomDepthStencilValue(FlashingStencilValue);

	FTimerDelegate TimerDelegate;
	TimerDelegate.BindLambda([&]() {
		ResetFlashing();
		});

	GetWorldTimerManager().SetTimer(FlashingHandle, TimerDelegate, FlashingDelay, false);
}

void AHCCharacterBase::ResetFlashing()
{
	MySetCustomDepthStencilValue(0);
}

void AHCCharacterBase::ClearTimerHandles()
{
	if (DamageCompHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(DamageCompHandle);
	}

	if (FlashingHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(FlashingHandle);
	}
}

void AHCCharacterBase::MySetCustomDepthStencilValue(int32 InStencilValue)
{
	TArray<UActorComponent*> ComponentToFlashs = GetComponentsByClass(USkeletalMeshComponent::StaticClass());
	for (UActorComponent* Component : ComponentToFlashs)
	{
		if (UPrimitiveComponent* PrimComp = Cast<UPrimitiveComponent>(Component))
		{
			PrimComp->SetCustomDepthStencilValue(InStencilValue);
		}
	}
}

void AHCCharacterBase::OnGameplayEffectRemoved(const FActiveGameplayEffect& ActiveEffect)
{
	FGameplayTagContainer GameplayTags;
	ActiveEffect.Spec.GetAllAssetTags(GameplayTags);
	for (const FGameplayTag& Tag : GameplayTags)
	{
		/*if (Tag.MatchesTag(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Fire"))))
		{

		}*/
	}
}

void AHCCharacterBase::PlayHitReactionMontage(EHCDirection InHitDirection)
{
	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCCharacterBase::PlayHitReactionMontage] Already Died0. Returning."));
		return;
	}

	if (HealthComponent)
	{
		if (HealthComponent->GetShield() > 0.0f)
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCCharacterBase::PlayHitReactionMontage] The Shield is still up. Returning."));
			return;
		}
	}

	if (HitReactionMontages.Contains(InHitDirection))
	{
		auto MontageToPlay = HitReactionMontages.Find(InHitDirection);
		if (UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance())
		{
			if (MontageToPlay)
			{
				AnimInstance->Montage_Play(*MontageToPlay);
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Reaction montage does not exist"));
	}
}

// Called every frame
void AHCCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AHCCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AHCCharacterBase::JumpSectionForCombo()
{
	if (bEnableCombo)
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance && JumpSectionANS)
		{
			UAnimMontage* CurrentMontage = AnimInstance->GetCurrentActiveMontage();
			FName SectionNameToChange = AnimInstance->Montage_GetCurrentSection();
			FName NextSectionName = JumpSectionANS->JumpSectionName;

			AnimInstance->Montage_SetNextSection(SectionNameToChange, NextSectionName, CurrentMontage);

		}
	}
}

FVector AHCCharacterBase::LookAtCursor()
{
	return FVector::ZeroVector;
}