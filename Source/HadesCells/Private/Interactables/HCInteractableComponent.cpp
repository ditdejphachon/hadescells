// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactables/HCInteractableComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/CapsuleComponent.h"
#include <Heroes/HCHeroCharacter.h>

// Sets default values
AHCInteractableComponent::AHCInteractableComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionCapsule"));
	CollisionCapsule->InitCapsuleSize(80.0f, 80.0f);
	CollisionCapsule->OnComponentBeginOverlap.AddDynamic(this, &AHCInteractableComponent::OnOverlapBegin);
	CollisionCapsule->OnComponentEndOverlap.AddDynamic(this, &AHCInteractableComponent::OnOverlapEnd);

	RootComponent = CollisionCapsule;

	PickUpIndicateComponent = CreateDefaultSubobject<UWidgetComponent>(FName("PickUpIndicateComponent"));
	PickUpIndicateComponent->SetupAttachment(RootComponent);
	PickUpIndicateComponent->SetRelativeLocation(FVector(0, 0, 120));
	PickUpIndicateComponent->SetWidgetSpace(EWidgetSpace::Screen);
	PickUpIndicateComponent->SetDrawSize(PickUpIndicateSize);
	PickUpIndicateComponent->SetVisibility(false, true);
}

void AHCInteractableComponent::OnConstruction(const FTransform& Transform)
{
	if (PickUpIndicateComponent)
	{
		PickUpIndicateComponent->SetDrawSize(PickUpIndicateSize);
	}
}

void AHCInteractableComponent::BeginInteraction(AHCHeroCharacter* ReceivingHero)
{
}

// Called when the game starts or when spawned
void AHCInteractableComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

void AHCInteractableComponent::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
	if (PickUpIndicateComponent)
	{
		PickUpIndicateComponent->SetVisibility(true, true);
	}

	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(OtherActor))
	{
		Hero->CurrentInteractableComponent = this;
	}
}

void AHCInteractableComponent::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (PickUpIndicateComponent)
	{
		PickUpIndicateComponent->SetVisibility(false, true);
	}
	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(OtherActor))
	{
		Hero->CurrentInteractableComponent = nullptr;
	}
}
