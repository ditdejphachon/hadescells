// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactables/HCAutoPickUp.h"
#include "GameplayAbilitySpec.h"
#include "GameplayEffectTypes.h"
#include "GameplayTagContainer.h"
#include <GameplayEffect.h>
#include <Abilities/HCAbilitySystemComponent.h>
#include "Heroes/HCHeroCharacter.h"
#include "Abilities/HCAttributeSetBase.h"
#include "Components/CapsuleComponent.h"

AHCAutoPickUp::AHCAutoPickUp(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	CollisionCapsule->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Overlap);
}
void AHCAutoPickUp::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
	if (m_HasPickedUp)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCAutoPickUp::OnOverlapBegin]: Has already been picked up. returning."));
		return;
	}

	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(OtherActor))
	{
		UGameplayEffect* GoldGE = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("GoldGE")));
		{
			GoldGE->DurationPolicy = EGameplayEffectDurationType::Instant;

			FGameplayModifierInfo ModifierInfo;
			ModifierInfo.Attribute = UHCAttributeSetBase::GetGoldAttribute();
			ModifierInfo.ModifierOp = EGameplayModOp::Additive;
			ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(DropGold));

			GoldGE->Modifiers.Add(ModifierInfo);
		}

		UGameplayEffect* ExpGE = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("ExpGE")));
		{
			ExpGE->DurationPolicy = EGameplayEffectDurationType::Instant;

			FGameplayModifierInfo ModifierInfo;
			ModifierInfo.Attribute = UHCAttributeSetBase::GetExperienceAttribute();
			ModifierInfo.ModifierOp = EGameplayModOp::Additive;
			ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(DropExp));

			ExpGE->Modifiers.Add(ModifierInfo);
		}

	
		if (UHCAbilitySystemComponent* HCASC = Hero->GetHCAbilitySystemComponent())
		{
			FGameplayEffectContextHandle EffectContext = HCASC->MakeEffectContext();
			HCASC->ApplyGameplayEffectToSelf(GoldGE, 1.0f, EffectContext);
			HCASC->ApplyGameplayEffectToSelf(ExpGE, 1.0f, EffectContext);
			HCASC->AddGameplayCue(FGameplayTag::RequestGameplayTag(FName("GameplayCue.PickUp")), EffectContext);
			m_HasPickedUp = true;

			TWeakObjectPtr<AActor> WeakThis = this;
			TWeakObjectPtr<UHCAbilitySystemComponent> WeakHCASC = HCASC;

			FTimerHandle TimerHandle;
			FTimerDelegate TimerDelegate;
			TimerDelegate.BindLambda([WeakThis, WeakHCASC]() {
				if (WeakHCASC.IsValid() && WeakThis.IsValid())
				{
					WeakHCASC->RemoveGameplayCue(FGameplayTag::RequestGameplayTag(FName("GameplayCue.PickUp")));
					WeakThis->Destroy();
				}
			});

			float Delay = 1.0f;  // Delay in seconds
			GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, Delay, false);
		}
	}
}

void AHCAutoPickUp::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}