// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies/HCEnemyBase.h"
#include <Kismet/GameplayStatics.h>
#include <Characters/HCPlayerController.h>
#include "Components/WidgetComponent.h"
#include "UIs/HCW_FloatingStatusBar.h"
#include <Abilities/HCAbilitySet.h>
#include "Abilities/HCAbilitySystemComponent.h"
#include <Enemies/HCEnemyData.h>
#include "Equipment/HCEquipmentDefinition.h"
#include <Equipment/HCEquipmentManagerComponent.h>
#include <Equipment/HCEquipmentInstance.h>
#include <Equipment/UHCEquipmentData.h>
#include "Equipment/Weapon/HCWeaponBase.h"
#include <HadesCells/HadesCellsGameMode.h>
#include <Heroes/HCHeroCharacter.h>
#include "Abilities/HCAttributeSetBase.h"
#include "Interactables/HCAutoPickUp.h"

AHCEnemyBase::AHCEnemyBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	FloatingStatusBarComponent = CreateDefaultSubobject<UWidgetComponent>(FName("FloatingStatusBarComponent"));
	FloatingStatusBarComponent->SetupAttachment(RootComponent);
	FloatingStatusBarComponent->SetRelativeLocation(FVector(0, 0, 120));
	FloatingStatusBarComponent->SetWidgetSpace(EWidgetSpace::Screen);
	FloatingStatusBarComponent->SetDrawSize(FloatingStatusBarSize);

	bIsShieldBreak = false;
}

void AHCEnemyBase::OnConstruction(const FTransform& Transform)
{
	FloatingStatusBarComponent->SetDrawSize(FloatingStatusBarSize);
}

void AHCEnemyBase::BeginPlay()
{
	
	checkf(AbilitySystemComponent, TEXT("[AHCCharacterBase::InitializeAttributes] AbilitySystemComponent is null ptr"));
	checkf(PawnData, TEXT("[AHCCharacterBase::InitializeAttributes] PawnData is null ptr"));
	if (UHCEnemyData* EnemyData = Cast<UHCEnemyData>(PawnData))
	{
		// add equipments
		for (TObjectPtr<UUHCEquipmentData> EquipmentData : EnemyData->EquipmentsToAdd)
		{
			EquipmentManagerComponent->EquipItem(EquipmentData.Get());
		}

		TArray<AActor*> Weapons = EquipmentManagerComponent->GetSpawnActorFromEquipmentSlotType(EHCEquipmentSlotType::MainWeapon);
		if (Weapons.IsValidIndex(0))
		{
			CurrentAttackWeapon = Cast<AHCWeaponBase>(Weapons[0]);
			CurrentAttackWeapon->InstigatorActor = this;
		}

		// add abilities
		if (!EnemyData->AbilitySets.IsEmpty())
		{
			for (const UHCAbilitySet* AbilitySet : EnemyData->AbilitySets)
			{
				if (AbilitySet)
				{
					AbilitySet->GiveToAbililtySystem(AbilitySystemComponent, nullptr, EHCEquipmentSlotType::None, 1, GetOwner());
				}
			}
		}
	}
	

	InitializeFloatingStatusBar();
	Super::BeginPlay();
}

void AHCEnemyBase::InitializeFloatingStatusBar()
{
	if (W_FloatingStatusBar)
	{
		UE_LOG(LogTemp, Warning, TEXT("W_FloatingStatusBar is already created"));
		return;
	}

	AHCPlayerController* PC = Cast<AHCPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));


	if (PC && WBP_FloatingStatusBar)
	{
		W_FloatingStatusBar = CreateWidget<UHCW_FloatingStatusBar>(PC, WBP_FloatingStatusBar);
		float MaxHealthPercentage = HealthComponent->GetHealthNormalized();
		float MaxShieldPercentage = HealthComponent->GetShieldNormalized();
		float MaxPosturePercentage = HealthComponent->GetPostureNormalized();

		W_FloatingStatusBar->SetHealthPercentage(MaxHealthPercentage);
		W_FloatingStatusBar->SetShieldPercentage(MaxShieldPercentage);
		W_FloatingStatusBar->SetPosturePercentage(MaxPosturePercentage);
		FloatingStatusBarComponent->SetWidget(W_FloatingStatusBar);
	}
}

void AHCEnemyBase::HandleHealthChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCEnemyBase::HandleHealthChanged] Already Dead"));
		return;
	}

	if (FMath::IsNearlyZero(InNewValue))
	{
		UE_LOG(LogTemp, Warning, TEXT("Litteraly Dying"));
	
		if (FloatingStatusBarComponent)
		{
			FloatingStatusBarComponent->SetVisibility(false);
			FloatingStatusBarComponent->Deactivate();
		}

		Die();
		DropResources();
		FinishDying();
		return;
	}


	if (W_FloatingStatusBar && InHealthComponent)
	{
		float CurrentMaxHealth = InHealthComponent->GetMaxHealth();
		W_FloatingStatusBar->SetHealthPercentage(FMath::IsNearlyZero(CurrentMaxHealth) ? 0.0f : InNewValue / CurrentMaxHealth);
	}
}

void AHCEnemyBase::HandleShieldChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCEnemyBase::HandleHealthChanged] Already Dead"));
		return;
	}

	if (W_FloatingStatusBar && InHealthComponent && !bIsShieldBreak)
	{
		float CurrentMaxShield = InHealthComponent->GetMaxShield();
		W_FloatingStatusBar->SetShieldPercentage(FMath::IsNearlyZero(CurrentMaxShield) ? 0.0f : InNewValue / CurrentMaxShield);

		if (FMath::IsNearlyZero(InNewValue))
		{
			// TODO: do OnShieldBreak activities
			//UE_LOG(LogTemp, Warning, TEXT("Shield break"));
			W_FloatingStatusBar->RemoveShieldBar();
			if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
			{
				Hero->PlayCameraShake();
			}

			bIsShieldBreak = true;
		}
	}
}

void AHCEnemyBase::HandlePostureChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCEnemyBase::HandleHealthChanged] Already Dead"));
		return;
	}

	if (W_FloatingStatusBar && InHealthComponent)
	{
		float CurrentMaxPosture = InHealthComponent->GetMaxPosture();
		W_FloatingStatusBar->SetPosturePercentage(FMath::IsNearlyZero(CurrentMaxPosture) ? 0.0f : InNewValue / CurrentMaxPosture);

		if (FMath::IsNearlyZero(InNewValue))
		{
			// TODO: do OnShieldBreak activities
			if (UHCEnemyData* EnemyData = Cast<UHCEnemyData>(PawnData))
			{
				FHCAbilitySet_GameplayEffect GE = EnemyData->StunGameplayEffect;
				if (!IsValid(GE.GameplayEffect))
				{
					UE_LOG(LogTemp, Error, TEXT("The granted gameplay effect [%s] is not valid"), *GetNameSafe(this));
					return;
				}

				const UGameplayEffect* GECDO = GE.GameplayEffect->GetDefaultObject<UGameplayEffect>();
				AbilitySystemComponent->ApplyGameplayEffectToSelf(GECDO, GE.EffectLevel, AbilitySystemComponent->MakeEffectContext());
			}
		}
	}
}

void AHCEnemyBase::ActivateAbility(const FGameplayTag& InGameplayTag)
{
	checkf(AbilitySystemComponent, TEXT("[AHCCharacterBase::InitializeAttributes] AbilitySystemComponent is null ptr"));
	
	FGameplayTagContainer GameplayTagContainer;
	GameplayTagContainer.AddTag(InGameplayTag);
	AbilitySystemComponent->TryActivateAbilitiesByTag(GameplayTagContainer, true);
}

void AHCEnemyBase::DropResources()
{
	if (ProgressionSystemComponent)
	{
		float DropGold = ProgressionSystemComponent->GetGold();
		float DropExp = ProgressionSystemComponent->GetExp();

		AHCAutoPickUp* AutoPickUpComp = nullptr;
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			AutoPickUpComp = Cast<AHCAutoPickUp>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, AutoPickUpClass, GetActorTransform(), ESpawnActorCollisionHandlingMethod::AlwaysSpawn, this));

			AutoPickUpComp->DropGold = DropGold;
			AutoPickUpComp->DropExp = DropExp;
			UGameplayStatics::FinishSpawningActor(AutoPickUpComp, GetActorTransform());
		}
	}
}
