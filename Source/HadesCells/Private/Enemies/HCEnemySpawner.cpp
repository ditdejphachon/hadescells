// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies/HCEnemySpawner.h"
#include <NiagaraFunctionLibrary.h>
#include <Kismet/GameplayStatics.h>

// Sets default values
AHCEnemySpawner::AHCEnemySpawner()
{
}

void AHCEnemySpawner::StartSpawning()
{
	GetWorld()->SpawnActor<AActor>(SpawnEnemyInfos.EnemyToSpawn, GetActorLocation(), GetActorRotation());

    if (NiagaraFX)
    {
        UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NiagaraFX, GetActorLocation(), GetActorRotation());
        return;
    }

    if (ParticleFX)
    {
        UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX, GetActorLocation(), GetActorRotation());
    }
}

// Called when the game starts or when spawned
void AHCEnemySpawner::BeginPlay()
{
	Super::BeginPlay();

	StartSpawning();
}
