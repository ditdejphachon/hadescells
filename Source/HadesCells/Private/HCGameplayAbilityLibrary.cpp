// Fill out your copyright notice in the Description page of Project Settings.


#include "HCGameplayAbilityLibrary.h"
#include "AbilitySystemComponent.h"

void UHCGameplayAbilityLibrary::RemoveLooseGameplayTag(UAbilitySystemComponent* InASC, FGameplayTag GameplayTagToRemove, int32 AmountToRemove)
{
	if (InASC)
	{
		InASC->RemoveLooseGameplayTag(GameplayTagToRemove, AmountToRemove);
	}
}

float UHCGameplayAbilityLibrary::CalculateLaunchVelocity(const FVector& Start, const FVector& Target, float LaunchAngle, float GravityZ)
{
    FVector Delta = Target - Start;
    float DistanceXY = FVector(Delta.X, Delta.Y, 0.0f).Size();
    float DistanceZ = Delta.Z;
    float LaunchAngle_Rad = FMath::DegreesToRadians(LaunchAngle);
    float SqrSpeed = (DistanceXY * GravityZ) / FMath::Sin(2 * LaunchAngle_Rad);
    float Speed = FMath::Sqrt(SqrSpeed);

    if (!FMath::IsNaN(Speed) && FMath::IsFinite(Speed))
    {
        return Speed * GravityZ;
    }
    else
    {
        return 0.0f;
    }
}

