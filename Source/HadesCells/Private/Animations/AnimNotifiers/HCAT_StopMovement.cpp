// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/AnimNotifiers/HCAT_StopMovement.h"
#include <Characters/HCCharacterBase.h>

UHCAT_StopMovement::UHCAT_StopMovement()
{
}

void UHCAT_StopMovement::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* CurrentCharacter = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		CurrentCharacter->bCanMove = false;

	}
}

void UHCAT_StopMovement::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime, const FAnimNotifyEventReference& EventReference)
{
}

void UHCAT_StopMovement::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* CurrentCharacter = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		CurrentCharacter->bCanMove = true;
	}
}
