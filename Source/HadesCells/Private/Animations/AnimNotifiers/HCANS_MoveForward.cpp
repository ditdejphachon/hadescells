// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/AnimNotifiers/HCANS_MoveForward.h"

UHCANS_MoveForward::UHCANS_MoveForward()
{
	//bShouldFireInEditor = false;
}

void UHCANS_MoveForward::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
}

void UHCANS_MoveForward::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime, const FAnimNotifyEventReference& EventReference)
{
	if (AActor* CurrentActor = MeshComp->GetOwner())
	{
		FVector Forward = CurrentActor->GetActorForwardVector();
		float OffsetAmount = FrameDeltaTime * MoveDistance;
		CurrentActor->AddActorWorldOffset(OffsetAmount * Forward, true, nullptr);
	}
}

void UHCANS_MoveForward::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
}
