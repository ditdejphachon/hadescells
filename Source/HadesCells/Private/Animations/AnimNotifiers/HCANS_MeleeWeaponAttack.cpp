// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/AnimNotifiers/HCANS_MeleeWeaponAttack.h"
#include "Characters/HCCharacterBase.h"
#include "Equipment/Weapon/HCWeaponBase.h"

UHCANS_MeleeWeaponAttack::UHCANS_MeleeWeaponAttack()
{
}

void UHCANS_MeleeWeaponAttack::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* BaseCharacter = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		if (AHCWeaponBase* CurrentWeapon = BaseCharacter->CurrentAttackWeapon)
		{
			CurrentWeapon->SetAttacking();
		}
	}
}

void UHCANS_MeleeWeaponAttack::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* BaseCharacter = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		if (AHCWeaponBase* CurrentWeapon = BaseCharacter->CurrentAttackWeapon)
		{
			BaseCharacter->CurrentAttackWeapon->SetEndAttacking();

		}
	}
}
