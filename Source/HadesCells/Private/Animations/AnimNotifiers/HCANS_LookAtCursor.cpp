// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/AnimNotifiers/HCANS_LookAtCursor.h"
#include <Characters/HCCharacterBase.h>
#include <Heroes/HCHeroCharacter.h>

UHCANS_LookAtCursor::UHCANS_LookAtCursor()
{
}

void UHCANS_LookAtCursor::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* CurrentCharacter = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, TEXT("Should look at cursor"));
		}

		CurrentCharacter->LookAtCursor();
	}
}

void UHCANS_LookAtCursor::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime, const FAnimNotifyEventReference& EventReference)
{
}

void UHCANS_LookAtCursor::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
}
