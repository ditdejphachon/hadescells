// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/AnimNotifiers/HCANS_KnockBack.h"
#include "Characters/HCCharacterBase.h"

UHCANS_KnockBack::UHCANS_KnockBack()
{
}

void UHCANS_KnockBack::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		Char->bCanKnockBack = true;
	}
}

void UHCANS_KnockBack::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		Char->bCanKnockBack = false;
	}
}
