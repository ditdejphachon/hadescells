// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/AnimNotifiers/HCANS_JumpSection.h"
#include "Characters/HCCharacterBase.h"

UHCANS_JumpSection::UHCANS_JumpSection()
{
}

void UHCANS_JumpSection::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* CharacterBase = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		CharacterBase->bEnableCombo = true;
		CharacterBase->JumpSectionANS = this;
	}
}

void UHCANS_JumpSection::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime, const FAnimNotifyEventReference& EventReference)
{
}

void UHCANS_JumpSection::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	if (AHCCharacterBase* CharacterBase = Cast<AHCCharacterBase>(MeshComp->GetOwner()))
	{
		CharacterBase->bEnableCombo = false;
		CharacterBase->JumpSectionANS = nullptr;
	}
}
