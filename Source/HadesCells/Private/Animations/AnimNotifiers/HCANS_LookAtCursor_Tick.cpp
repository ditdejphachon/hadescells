// Fill out your copyright notice in the Description page of Project Settings.


#include "Animations/AnimNotifiers/HCANS_LookAtCursor_Tick.h"
#include <Heroes/HCHeroCharacter.h>

UHCANS_LookAtCursor_Tick::UHCANS_LookAtCursor_Tick()
{
}

void UHCANS_LookAtCursor_Tick::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(MeshComp->GetOwner()))
	{
		Hero->bAllowOnTickLookAtCursor = true;
	}
}

void UHCANS_LookAtCursor_Tick::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(MeshComp->GetOwner()))
	{
		Hero->bAllowOnTickLookAtCursor = false;
	}
}
