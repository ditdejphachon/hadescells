// Fill out your copyright notice in the Description page of Project Settings.


#include "Save/HCGIBase.h"
#include <Kismet/GameplayStatics.h>

UHCGIBase::UHCGIBase()
{
	SaveGameSlotName = "JeeTest";
	UserIndex = 0;
	
}

UHCSaveGameBase* UHCGIBase::LoadGame()
{
	SaveGameObject = Cast<UHCSaveGameBase>(UGameplayStatics::LoadGameFromSlot(SaveGameSlotName, UserIndex));

	if (SaveGameObject == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGIBase::LoadGame] Cannot load save from slot name: [%s] and index: [%d]"), *SaveGameSlotName, UserIndex);
		UE_LOG(LogTemp, Warning, TEXT("[UHCGIBase::LoadGame] Createing a new one"));
		SaveGameObject = Cast<UHCSaveGameBase>(UGameplayStatics::CreateSaveGameObject(UHCSaveGameBase::StaticClass()));
		SaveGame();
	}

	return SaveGameObject;
}

void UHCGIBase::SaveGame()
{
	if (UGameplayStatics::SaveGameToSlot(SaveGameObject, SaveGameSlotName, UserIndex))
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGIBase::SaveGame] Save Game succeed"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCGIBase::SaveGame] Save Game failed"));
	}
}

void UHCGIBase::Init()
{
	Super::Init();
}

void UHCGIBase::OnStart()
{
	Super::OnStart();

	SaveGameObject = LoadGame();
	ChamberNumber = 0;
	// TODO: need rework
	if (SaveGameObject)
	{
		SaveGameObject->PlayerHealth = 2000.0f;
		SaveGameObject->PlayerShield = 500.0f;

		SaveGameObject->PrimaryWeaponED = nullptr;
		SaveGameObject->SecondaryWeaponED = nullptr;
		SaveGameObject->PrimarySkillED = nullptr;
		SaveGameObject->SecondarySkillED = nullptr;
		SaveGame();
	}
}
