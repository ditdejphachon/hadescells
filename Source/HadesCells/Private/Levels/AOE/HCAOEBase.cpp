// Fill out your copyright notice in the Description page of Project Settings.


#include "Levels/AOE/HCAOEBase.h"
#include "Components/CapsuleComponent.h"
#include "GameplayEffect.h"
#include <Characters/HCCharacterBase.h>
#include <AbilitySystemComponent.h>
#include <AbilitySystemGlobals.h>
#include "AbilitySystemBlueprintLibrary.h"
#include <Components/BoxComponent.h>

// Sets default values
AHCAOEBase::AHCAOEBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
    CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	RootComponent = CollisionBox;

	AreaMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AreaMesh"));
	AreaMesh->SetupAttachment(RootComponent);
}

void AHCAOEBase::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorld()->GetTimerManager().SetTimer(AOETimerHandle, this, &AHCAOEBase::ApplyAreaEffect, AOETickRate, true);
}

void AHCAOEBase::ApplyAreaEffect()
{
    TArray<AActor*> OverlappingActors;
    CollisionBox->GetOverlappingActors(OverlappingActors);

    for (AActor* OverlappedActor : OverlappingActors)
    {
        if (!OverlappedActor)
        {
            UE_LOG(LogTemp, Warning, TEXT("[AHCAOEBase::ApplyAreaEffect] OverlappedActor is invalid"));
            continue;
        }

        UE_LOG(LogTemp, Warning, TEXT("OverlappedActor: [%s]"), *OverlappedActor->GetHumanReadableName());

        FGameplayEventData EventData;
        EventData.Instigator = this;
        EventData.Target = OverlappedActor;

        AHCCharacterBase* HitActor = Cast<AHCCharacterBase>(OverlappedActor);
        if (HitActor)
        {
            if (HitActor->bHasDied)
            {
                UE_LOG(LogTemp, Warning, TEXT("Already dead. continuing"));
                continue;
            }

            if (UAbilitySystemComponent* HitActorASC = HitActor->GetAbilitySystemComponent())
            {
              
                FGameplayEffectContextHandle EffectContext = HitActorASC->MakeEffectContext();
                EffectContext.AddSourceObject(this);

                FGameplayEffectSpecHandle AreaEffectSpecHandle = HitActorASC->MakeOutgoingSpec(AreaEffect, 1.0f, EffectContext);
                HitActorASC->ApplyGameplayEffectSpecToSelf(*AreaEffectSpecHandle.Data.Get());

                FVector HitDirection = (HitActor->GetActorLocation() - GetActorLocation()).GetSafeNormal();

                if (FMath::Abs(HitDirection.X) > FMath::Abs(HitDirection.Y))
                {
                    if (HitDirection.X > 0.0f)
                    {
                        // hit from the right
                        HitActor->PlayHitReactionMontage(EHCDirection::Right);
                    }
                    else
                    {
                        // hit from the left
                        HitActor->PlayHitReactionMontage(EHCDirection::Left);
                    }
                }
                else
                {
                    if (HitDirection.Y > 0.0f)
                    {
                        // hit from the front
                        HitActor->PlayHitReactionMontage(EHCDirection::Top);
                    }
                    else
                    {
                        // hit from the back
                        HitActor->PlayHitReactionMontage(EHCDirection::Bottom);
                    }
                }

                UE_LOG(LogTemp, Warning, TEXT("Applied actors."));
            }
            else
            {
                //UE_LOG(LogTemp, Error, TEXT("HHitActorASC is invalid"));
            }
        }
        else
        {
            //UE_LOG(LogTemp, Error, TEXT("HitActor and InstigatorActor are invalid"));
        }
    }
}
