// Fill out your copyright notice in the Description page of Project Settings.


#include "Levels/Traps/HCTrapBase.h"
#include "Equipment/Projectile/HCProjectileBase.h"
#include "Components/BoxComponent.h"
#include "Characters/HCCharacterBase.h"
#include <Kismet/KismetMathLibrary.h>
#include <HCGameplayAbilityLibrary.h>
#include <Kismet/GameplayStatics.h>

AHCTrapBase::AHCTrapBase()
{
	PrimaryActorTick.bCanEverTick = true;
	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AHCTrapBase::OnBeginOverlap);
	RootComponent = CollisionBox;

	TrapMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TrapMesh"));
	TrapMesh->SetupAttachment(RootComponent);
	bCanTrigger = true;
}

void AHCTrapBase::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bCanTrigger)
	{
		if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(OtherActor))
		{
			TriggerTrap();
			bCanTrigger = false;
			// Set the timer with a lambda function
			FTimerHandle TimerHandle;
			FTimerDelegate TimerDelegate;
			TimerDelegate.BindLambda([&]() {
				bCanTrigger = true;
				});

			GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, TrapDelay, false);
		}
	}
}

void AHCTrapBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AHCTrapBase::TriggerTrap()
{
	// TODO: get the direction vector
	if (StartingTargetPoint)
	{
		FVector LaunchDirection = (GetActorLocation() - StartingTargetPoint->GetActorLocation()).GetUnsafeNormal();
		//FVector LaunchLocation = LaunchDirection + StartingTargetPoint->GetActorLocation();
		FVector LaunchLocation = StartingTargetPoint->GetActorLocation();
		FRotator LaunchRotator = UKismetMathLibrary::MakeRotFromX(LaunchDirection);
		LaunchRotator.Pitch = LaunchAngle;
		FTransform SpawnTransform = FTransform(LaunchRotator, LaunchLocation);
		float LaunchVelocity = UHCGameplayAbilityLibrary::CalculateLaunchVelocity(StartingTargetPoint->GetActorLocation(), GetActorLocation(), LaunchAngle);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		if (AHCProjectileBase* SpawnedTrap = Cast<AHCProjectileBase>(UGameplayStatics::BeginDeferredActorSpawnFromClass(this, TrapSpawn, SpawnTransform, ESpawnActorCollisionHandlingMethod::AlwaysSpawn, this)))
		{
			//SpawnedTrap->CalculatedInitialSpeed = LaunchVelocity;
			SpawnedTrap->CalculatedInitialSpeed = LaunchSpeed;
			UGameplayStatics::FinishSpawningActor(SpawnedTrap, SpawnTransform);
		}
	}
}


