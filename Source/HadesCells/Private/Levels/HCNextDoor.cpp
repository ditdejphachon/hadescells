// Fill out your copyright notice in the Description page of Project Settings.


#include "Levels/HCNextDoor.h"
#include <NiagaraFunctionLibrary.h>
#include <Kismet/GameplayStatics.h>
#include <HadesCells/HadesCellsGameMode.h>
#include "Components/WidgetComponent.h"

AHCNextDoor::AHCNextDoor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	DoorMesh->SetupAttachment(RootComponent);

    NiagaraComponent = nullptr;
    ParticleSystemComponent = nullptr;
}

void AHCNextDoor::BeginPlay()
{
	Super::BeginPlay();
	
    if (AHadesCellsGameMode* GM = Cast<AHadesCellsGameMode>(GetWorld()->GetAuthGameMode()))
    {
        GM->OnReadyToProceed.AddUObject(this, &AHCNextDoor::OnReadyToProceed);
    }

    if (NiagaraFX)
    {
        NiagaraComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NiagaraFX, GetActorLocation(), GetActorRotation());
        return;
    }

    if (ParticleFX)
    {
        ParticleSystemComponent = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleFX, GetActorLocation(), GetActorRotation());
    }

    if (PickUpIndicateComponent)
    {
        PickUpIndicateComponent->SetVisibility(false);
    }
}

void AHCNextDoor::BeginInteraction(AHCHeroCharacter* ReceivingHero)
{
    if (AHadesCellsGameMode* GM = Cast<AHadesCellsGameMode>(GetWorld()->GetAuthGameMode()))
    {
        GM->ProceedToNextLevel();
    }
}

void AHCNextDoor::OnReadyToProceed()
{
    if (PickUpIndicateComponent)
    {
        PickUpIndicateComponent->SetVisibility(true);
    }

    if (NiagaraComponent)
    {
        NiagaraComponent->DestroyComponent();
    }

    if (ParticleSystemComponent)
    {
        ParticleSystemComponent->DestroyComponent();
    }
}


void AHCNextDoor::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
    if (AHadesCellsGameMode* GM = Cast<AHadesCellsGameMode>(GetWorld()->GetAuthGameMode()))
    {
        if (!GM->bCanProceed)
        {
            UE_LOG(LogTemp, Warning, TEXT("[AHCNextDoor::OnOverlapBegin] cannot proceed"));
            return;
        }
    }

    AHCInteractableComponent::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepHitResult);
}

void AHCNextDoor::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    if (AHadesCellsGameMode* GM = Cast<AHadesCellsGameMode>(GetWorld()->GetAuthGameMode()))
    {
        if (!GM->bCanProceed)
        {
            return;
        }
    }

    AHCInteractableComponent::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}