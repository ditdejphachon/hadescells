// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/Projectile/HCProjectileBase.h"
#include <Kismet/GameplayStatics.h>
#include "Kismet/KismetSystemLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "NiagaraComponent.h"

// Sets default values
AHCProjectileBase::AHCProjectileBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	{
		ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
		ProjectileMesh->bRenderCustomDepth = true;
		ProjectileMesh->bReceivesDecals = false;
		RootComponent = ProjectileMesh;
	}

	{
		PSComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("PSComp"));
		PSComp->AttachToComponent(ProjectileMesh, FAttachmentTransformRules::KeepRelativeTransform);
		NSComp = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NSComp"));
		NSComp->AttachToComponent(ProjectileMesh, FAttachmentTransformRules::KeepRelativeTransform);
	}
	
	{
		ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
		ProjectileMovementComponent->InitialSpeed = CalculatedInitialSpeed;
		ProjectileMovementComponent->MaxSpeed = CalculatedInitialSpeed;
	}

	SphereTraceRadius = 50.0f;
	TraceEndMultiplier = 500.0f;
	bIsSelfDamage = false;
	bIsDebug = true;
	AppliedActors.Empty();
}

// Called when the game starts or when spawned
void AHCProjectileBase::BeginPlay()
{
	Super::BeginPlay();
}

void AHCProjectileBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->InitialSpeed = CalculatedInitialSpeed;
		ProjectileMovementComponent->MaxSpeed = CalculatedInitialSpeed;
	}
}

void AHCProjectileBase::ApplyGameEffectToTarget(UAbilitySystemComponent* InASC, ACharacter* InActor)
{
	for (TSubclassOf<UGameplayEffect> DamageEffect : DamageEffects)
	{
		FGameplayEffectContextHandle EffectContext = InASC->MakeEffectContext();
		EffectContext.AddSourceObject(this);

		FGameplayEffectSpecHandle DamageEffectSpecHandle = InASC->MakeOutgoingSpec(DamageEffect, 1.0f, EffectContext);
		InASC->ApplyGameplayEffectSpecToSelf(*DamageEffectSpecHandle.Data.Get());

		AppliedActors.AddUnique(InActor);
	}
}

void AHCProjectileBase::SpawnParticleSystems(bool bDestroySelf)
{
	// play vfx regardless
	for (const FHCGCInfo& GCInfo : GameplayCueInfos)
	{
		if (GCInfo.ParticleFX)
		{
			UParticleSystemComponent* SpawnedParticleSystem = UGameplayStatics::SpawnEmitterAttached(GCInfo.ParticleFX, ProjectileMesh, GCInfo.AttachPointNames, GetActorLocation(), FRotator(), GCInfo.FXScale, EAttachLocation::KeepWorldPosition, true);

			if (SpawnedParticleSystem && bDestroySelf)
			{
				SpawnedParticleSystem->OnSystemFinished.AddDynamic(this, &AHCProjectileBase::OnParticleSystemFinished);

				ProjectileMesh->SetVisibility(false, false);
				ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
			}
		}
	}
}

void AHCProjectileBase::SpawnExtraActors()
{
	for (FHCProjectileExtraActorInfo& ActorToSpawnInfo : ActorToSpawnInfos)
	{
		if (ActorToSpawnInfo.ActorToSpawn)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Cast<APawn>(this);

			const float AngleIncrement = 360.0f / ActorToSpawnInfo.NumberToSpawn;

			for (int32 i = 0; i < ActorToSpawnInfo.NumberToSpawn; ++i)
			{
				float Angle = i * AngleIncrement;
				float RadAngle = FMath::DegreesToRadians(Angle);

				FVector SpawnLocation = FVector(
					 GetActorLocation().X + ActorToSpawnInfo.SpawnRadius * FMath::Cos(RadAngle),
					 GetActorLocation().Y + ActorToSpawnInfo.SpawnRadius * FMath::Sin(RadAngle),
					 GetActorLocation().Z
				);

				FRotator SpawnRotation = FRotator::ZeroRotator; 

				GetWorld()->SpawnActor<AActor>(ActorToSpawnInfo.ActorToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
			}
		}
	}
}

void AHCProjectileBase::KnockActorBack(AActor* InActor, FVector InDirection)
{
	if (KnockBackDistance == 0.0f)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCProjectileBase::KnockActorBack] KnockBackDistance is 0.0f. Returning."));
		return;
	}

	if (AHCCharacterBase* Char = Cast<AHCCharacterBase>(InActor))
	{
		Char->KnockBack(KnockBackDistance, KnockBackSpeed, InDirection);
	}
}

void AHCProjectileBase::DestroyParticleSystems()
{
	if (PSComp)
	{
		PSComp->DestroyComponent();
	}

	if (NSComp)
	{
		NSComp->DestroyComponent();
	}
}
