// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/Projectile/HCImpactProjectileBase.h"
#include "Kismet/KismetSystemLibrary.h"
#include "AbilitySystemComponent.h"
#include <Kismet/GameplayStatics.h>

AHCImpactProjectileBase::AHCImpactProjectileBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ProjectileMesh->OnComponentHit.AddDynamic(this, &AHCImpactProjectileBase::OnComponentHit);

}

void AHCImpactProjectileBase::BeginPlay()
{
	AHCProjectileBase::BeginPlay();
}

void AHCImpactProjectileBase::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor == GetInstigator())
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot hit self OtherComp: [%s]"), *OtherComp->GetReadableName());
		return;
	}

	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->Deactivate();
	}

	Detonate();
}

void AHCImpactProjectileBase::OnParticleSystemFinished(UParticleSystemComponent* FinishedComponent)
{
	AHCProjectileBase::OnParticleSystemFinished(FinishedComponent);
}