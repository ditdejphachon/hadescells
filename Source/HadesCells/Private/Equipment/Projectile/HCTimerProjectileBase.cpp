// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/Projectile/HCTimerProjectileBase.h"

AHCTimerProjectileBase::AHCTimerProjectileBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	TimeTilDetonate = 3.0f;
}

void AHCTimerProjectileBase::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(TimeTilDetonateHandle, this, &AHCTimerProjectileBase::PreDetonate, TimeTilDetonate, false);
	FlashingDelay = TimeTilDetonate / BsFactor_1;
	StartFlashing();
}

void AHCTimerProjectileBase::PreDetonate()
{
	Detonate();
	GetWorld()->GetTimerManager().ClearTimer(TimeTilDetonateHandle);
	GetWorld()->GetTimerManager().ClearTimer(FlashingHandle);
}

void AHCTimerProjectileBase::StartFlashing()
{
	TArray<UActorComponent*> ComponentToFlashs = GetComponentsByClass(UStaticMeshComponent::StaticClass());
	for (UActorComponent* Component : ComponentToFlashs)
	{
		if (UPrimitiveComponent* PrimComp = Cast<UPrimitiveComponent>(Component))
		{
			PrimComp->SetCustomDepthStencilValue(bIsFlashing ? FlashingStencilValue : 0);
		}
	}

	bIsFlashing = !bIsFlashing;
	FlashingDelay *= BsFactor_2;
	GetWorldTimerManager().SetTimer(FlashingHandle, this, &AHCTimerProjectileBase::StartFlashing, FlashingDelay, false);
}
