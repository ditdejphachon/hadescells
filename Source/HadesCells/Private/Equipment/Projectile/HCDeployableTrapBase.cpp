// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/Projectile/HCDeployableTrapBase.h"
#include "Components/BoxComponent.h"
#include <AbilitySystemBlueprintLibrary.h>


AHCDeployableTrapBase::AHCDeployableTrapBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ProjectileMesh->OnComponentHit.AddDynamic(this, &AHCDeployableTrapBase::OnComponentHit);
	BoxCollider = CreateDefaultSubobject<UBoxComponent>("BoxCollider");
	BoxCollider->SetupAttachment(ProjectileMesh);

	TrapLifeSpan = 10.0f;
	bHit = false;
}

void AHCDeployableTrapBase::BeginPlay()
{
	Super::BeginPlay();

	float DurationMagnitude = 0.0f;

	for (TSubclassOf<UGameplayEffect> DamageEffect : DamageEffects)
	{
		if (const UGameplayEffect* DamageEffectCD0 = GetDefault<UGameplayEffect>(DamageEffect))
		{
			if (DamageEffectCD0->DurationPolicy == EGameplayEffectDurationType::HasDuration)
			{
				DamageEffectCD0->DurationMagnitude.GetStaticMagnitudeIfPossible(1.0f, DurationMagnitude);

				if (DurationMagnitude > 0.0f)
				{
					TrapLifeSpan = FMath::Max(TrapLifeSpan, DurationMagnitude);
				}
			}
			
		}
	}

	SetActorRotation(FRotator::ZeroRotator);
}

void AHCDeployableTrapBase::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor == GetInstigator())
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot hit self OtherComp: [%s]"), *OtherComp->GetReadableName());
		return;
	}

	if (bHit && bShouldHitOnce)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCDeployableTrapBase::OnComponentHit] should hit once. returning."));
		return;
	}

	if (AHCCharacterBase* HitActor = Cast<AHCCharacterBase>(OtherActor))
	{
		// TODO: snap to the target
		FVector ActorLocation = HitActor->GetActorLocation();
		ActorLocation.Z = 0.0f;
		SetActorLocation(ActorLocation);
		SetActorRotation(HitActor->GetActorRotation());
		PostTrapActivation(OtherActor);
	}

	GetWorld()->GetTimerManager().SetTimer(LifeSpanTimerHandle, this, &AHCDeployableTrapBase::OnEndOfLifeSpan, TrapLifeSpan, false);

	ProjectileMovementComponent->SetVelocityInLocalSpace(FVector::ZeroVector);
	ProjectileMovementComponent->Deactivate();
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &AHCDeployableTrapBase::OnBeginOverlap);

	TArray<AActor*> OverlappingActors;
	BoxCollider->GetOverlappingActors(OverlappingActors);
	/*for (AActor* OverlappingActor : OverlappingActors)
	{
		PostTrapActivation(OverlappingActor);
	}*/

	for (size_t i = 0; i < OverlappingActors.Num(); i++)
	{
		if (AHCCharacterBase* HitActor = Cast<AHCCharacterBase>(OverlappingActors[i]))
		{
			if (bHit && bShouldHitOnce)
			{
				UE_LOG(LogTemp, Warning, TEXT("[AHCDeployableTrapBase::OnComponentHit] should hit once. returning."));
				return;
			}

			if (i == 0)
			{
				FVector ActorLocation = HitActor->GetActorLocation();
				ActorLocation.Z = 0.0f;

				SetActorLocation(ActorLocation);
				SetActorRotation(FRotator::ZeroRotator);
				bHit = true;
			}

			PostTrapActivation(HitActor);
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("Binded to overlap"));
	ProjectileMesh->OnComponentHit.RemoveAll(this);
}

void AHCDeployableTrapBase::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == GetInstigator())
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot hit self OtherComp: [%s]"), *OtherComp->GetReadableName());
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("should spawn actors"));

	PostTrapActivation(OtherActor);
}

void AHCDeployableTrapBase::OnEndOfLifeSpan()
{
	// TODO: spawn end of life span particles (World Relative spawn settings)
	Destroy();
}

void AHCDeployableTrapBase::PostTrapActivation(AActor* InActor)
{
	if (bShouldSpawnActors)
	{
		SpawnExtraActors();
	}
	else
	{
		// Activate GE to the first overlapped actor
		if (AHCCharacterBase* HitActor = Cast<AHCCharacterBase>(InActor))
		{
			if (HitActor == GetInstigator())
			{
				UE_LOG(LogTemp, Warning, TEXT("Cannot hit self OtherComp: [%s]"), *HitActor->GetName());
				return;
			}

			if (AppliedActors.Contains(HitActor))
			{
				UE_LOG(LogTemp, Warning, TEXT("contain applied actors. skip"));
				return;
			}

			if (UAbilitySystemComponent* HitActorASC = HitActor->GetAbilitySystemComponent())
			{
				ApplyGameEffectToTarget(HitActorASC, HitActor);
				SpawnParticleSystems(false);
				ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			}
		}
	}
}
