// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/Projectile/HCThrowableProjectileBase.h"
#include <Kismet/KismetSystemLibrary.h>
#include <Kismet/GameplayStatics.h>
#include <Heroes/HCHeroCharacter.h>


AHCThrowableProjectileBase::AHCThrowableProjectileBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void AHCThrowableProjectileBase::Detonate()
{
	FCollisionQueryParams TraceParams(FName(TEXT("SphereTrace")), false, nullptr);
	TraceParams.bReturnPhysicalMaterial = true;

	if (!bIsSelfDamage)
	{
		TraceParams.AddIgnoredActor(GetInstigator());
	}

	TArray<FHitResult> HitResults;

	GetWorld()->SweepMultiByChannel(
		HitResults, // hit result
		GetActorLocation(), // start
		GetActorLocation() + GetActorForwardVector() * TraceEndMultiplier, // end
		FQuat::Identity, // rotation
		ECollisionChannel::ECC_Visibility, // trace channel
		FCollisionShape::MakeSphere(SphereTraceRadius), // trace shape
		TraceParams // trace params
	);

	if (bIsDebug)
	{
		UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation(), SphereTraceRadius);
	}

	K2_OnHit();
	DestroyParticleSystems();

	for (FHitResult HitResult : HitResults)
	{
		if (AHCCharacterBase* HitActor = Cast<AHCCharacterBase>(HitResult.GetActor()))
		{
			if (!bIsSelfDamage && HitActor == GetInstigator())
			{
				UE_LOG(LogTemp, Warning, TEXT("ignore instigator. skip"));
				continue;
			}

			if (AppliedActors.Contains(HitActor))
			{
				UE_LOG(LogTemp, Warning, TEXT("contain applied actors. skip"));
				continue;
			}

			if (UAbilitySystemComponent* HitActorASC = HitActor->GetAbilitySystemComponent())
			{
				ApplyGameEffectToTarget(HitActorASC, HitActor);
				KnockActorBack(HitActor, (HitActor->GetActorLocation() - GetActorLocation()).GetUnsafeNormal());
				if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
				{
					Hero->PlayCameraShake();
				}
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("HitActorASC is invalid"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("HitActor is invalid"));
		}
	}

	SpawnParticleSystems();
}
