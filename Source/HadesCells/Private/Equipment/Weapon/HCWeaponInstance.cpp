// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/Weapon/HCWeaponInstance.h"
#include "Animation/AnimInstance.h"
#include "Characters/HCCharacterBase.h"

UHCWeaponInstance::UHCWeaponInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void UHCWeaponInstance::OnEquipped()
{
	Super::OnEquipped();
	if (OnEquippedAnim)
	{
		if (APawn* OwningPawn = GetPawn())
		{
			if (AHCCharacterBase* OwningCharacter = Cast<AHCCharacterBase>(OwningPawn))
			{
				OwningCharacter->PlayAnimMontage(OnEquippedAnim);
			}
		}
	}
}

void UHCWeaponInstance::OnUnEquipped()
{
	if (OnUnEquippedAnim)
	{
		if (APawn* OwningPawn = GetPawn())
		{
			if (AHCCharacterBase* OwningCharacter = Cast<AHCCharacterBase>(OwningPawn))
			{
				OwningCharacter->PlayAnimMontage(OnUnEquippedAnim);
			}
		}
	}

	Super::OnUnEquipped();
}
