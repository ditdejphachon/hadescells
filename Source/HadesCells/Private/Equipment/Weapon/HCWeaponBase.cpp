// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/Weapon/HCWeaponBase.h"
#include "Components/BoxComponent.h"
#include "Equipment/HCEquipmentManagerComponent.h"
#include "GameplayEffect.h"
#include <Characters/HCCharacterBase.h>
#include <AbilitySystemComponent.h>
#include <AbilitySystemGlobals.h>
#include "AbilitySystemBlueprintLibrary.h"
#include <Enemies/HCEnemyBase.h>

// Sets default values
AHCWeaponBase::AHCWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    // Create and attach the mesh component
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    RootComponent = MeshComponent;

    WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
    WeaponMesh->SetupAttachment(MeshComponent);
    WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    // Create and attach the box collider component
    BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
    BoxCollider->SetupAttachment(WeaponMesh);

    BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &AHCWeaponBase::OnBeginOverlap);
    BoxCollider->OnComponentEndOverlap.AddDynamic(this, &AHCWeaponBase::OnEndOverlap);

    bIsReadyForExec = true;
}

// Called when the game starts or when spawned
void AHCWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
	if (BoxCollider)
	{
		BoxCollider->IgnoreActorWhenMoving(nullptr, true);
		BoxCollider->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void AHCWeaponBase::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    StartAttackExec();
}

void AHCWeaponBase::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    bIsReadyForExec = true;
}

void AHCWeaponBase::SetAttacking()
{
    bIsAttacking = true;
    AppliedActors.Empty();
    BoxCollider->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AHCWeaponBase::SetEndAttacking()
{
    bIsAttacking = false;
    AppliedActors.Empty();
    BoxCollider->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AHCWeaponBase::StartAttackExec()
{
    UE_LOG(LogTemp, Warning, TEXT("OnBeginOverlap bIsAttacking: [%s] bIsReadyForExec:[%s]"), bIsAttacking ? TEXT("true") : TEXT("false"), bIsReadyForExec ? TEXT("true") : TEXT("false"));

    for (AActor* AppliedActor : AppliedActors)
    {
        UE_LOG(LogTemp, Warning, TEXT("AppliedActor: [%s]"), *AppliedActor->GetHumanReadableName());
    }

    if (bIsAttacking && bIsReadyForExec)
    {
        TArray<AActor*> OverlappingActors;
        BoxCollider->GetOverlappingActors(OverlappingActors);

        for (AActor* OverlappedActor : OverlappingActors)
        {
            if (OverlappedActor)
            {
                UE_LOG(LogTemp, Warning, TEXT("OverlappedActor: [%s]"), *OverlappedActor->GetHumanReadableName());
            }

            if (AppliedActors.Contains(OverlappedActor))
            {
                UE_LOG(LogTemp, Warning, TEXT("contain applied actors. skip"));
                continue;
            }

            if (InstigatorActor == OverlappedActor || OverlappedActor == nullptr)
            {
                //UE_LOG(LogTemp, Warning, TEXT("instigator and OverlappedActor are the same actor. Or. invalid OverlappedActor"));
                continue;
            }

            // TODO: check enemy hits enemy case
            AHCEnemyBase* InstigatorEnemy = Cast<AHCEnemyBase>(InstigatorActor);
            AHCEnemyBase* OverlappedEnemy = Cast<AHCEnemyBase>(OverlappedActor);
            if (InstigatorEnemy && OverlappedEnemy)
            {
                UE_LOG(LogTemp, Warning, TEXT("Enemy cannot hit each other"));
                continue;
            }

            FGameplayEventData EventData;
            EventData.Instigator = InstigatorActor;
            EventData.Target = OverlappedActor;

            AHCCharacterBase* HitActor = Cast<AHCCharacterBase>(OverlappedActor);
            if (HitActor && InstigatorActor)
            {
                if (HitActor->bHasDied)
                {
                    UE_LOG(LogTemp, Warning, TEXT("Already dead. Returning"));
                    continue;
                }

                if (UAbilitySystemComponent* HitActorASC = HitActor->GetAbilitySystemComponent())
                {
                    for (TSubclassOf<UGameplayEffect> DamageEffect : DamageEffects)
                    {
                        FGameplayEffectContextHandle EffectContext = HitActorASC->MakeEffectContext();
                        EffectContext.AddSourceObject(this);

                        {
                            FGameplayTagContainer OwnedTags;
                            HitActorASC->GetOwnedGameplayTags(OwnedTags);
                            const UGameplayEffect* GE = GetDefault<UGameplayEffect>(DamageEffect);

                            FGameplayTagContainer addedTags = GE->InheritableOwnedTagsContainer.Added;
                            if (OwnedTags.HasAny(addedTags))
                            {
                                UE_LOG(LogTemp, Warning, TEXT("[AHCWeaponBase::StartAttackExec] GE already Applied"));
                                continue;
                            }
                        }

                        DamageEffectSpecHandle = HitActorASC->MakeOutgoingSpec(DamageEffect, 1.0f, EffectContext);
                        HitActorASC->ApplyGameplayEffectSpecToSelf(*DamageEffectSpecHandle.Data.Get());

                        FGameplayTagContainer PoiseTag;
                        PoiseTag.AddTag(FGameplayTag::RequestGameplayTag(FName("State.Poise")));
                        if (!HitActorASC->HasAnyMatchingGameplayTags(PoiseTag))
                        {
                            FVector HitDirection = (HitActor->GetActorLocation() - GetActorLocation()).GetSafeNormal();

                            if (FMath::Abs(HitDirection.X) > FMath::Abs(HitDirection.Y))
                            {
                                if (HitDirection.X > 0.0f)
                                {
                                    // hit from the right
                                    HitActor->PlayHitReactionMontage(EHCDirection::Right);
                                }
                                else
                                {
                                    // hit from the left
                                    HitActor->PlayHitReactionMontage(EHCDirection::Left);
                                }
                            }
                            else
                            {
                                if (HitDirection.Y > 0.0f)
                                {
                                    // hit from the front
                                    HitActor->PlayHitReactionMontage(EHCDirection::Top);
                                }
                                else
                                {
                                    // hit from the back
                                    HitActor->PlayHitReactionMontage(EHCDirection::Bottom);
                                }
                            }
                        }

                        if (InstigatorActor->bCanKnockBack)
                        {
                            HitActor->KnockBack(KnockBackDistance, KnockBackSpeed, (HitActor->GetActorLocation() - InstigatorActor->GetActorLocation()).GetUnsafeNormal());
                        }

                        AppliedActors.AddUnique(HitActor);

                        UE_LOG(LogTemp, Warning, TEXT("Applied actors."));
                    }
                }
                else
                {
                    //UE_LOG(LogTemp, Error, TEXT("HHitActorASC is invalid"));
                }
            }
            else
            {
                //UE_LOG(LogTemp, Error, TEXT("HitActor and InstigatorActor are invalid"));
            }
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Ignore. Cannot Attack"));
    }
}

