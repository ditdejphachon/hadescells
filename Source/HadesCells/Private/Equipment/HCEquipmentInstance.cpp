// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/HCEquipmentInstance.h"
#include "Equipment/HCEquipmentDefinition.h"
#include "GameFramework/Character.h"
	
UHCEquipmentInstance::UHCEquipmentInstance(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
}

UWorld* UHCEquipmentInstance::GetWorld() const
{
	if (APawn* OwningPawn = GetPawn())
	{
		return OwningPawn->GetWorld();
	}
	return nullptr;
}

APawn* UHCEquipmentInstance::GetPawn() const
{
	return Cast<APawn>(GetOuter());
}

APawn* UHCEquipmentInstance::GetTypedPawn(TSubclassOf<APawn> PawnType) const
{
	if (UClass* ActualPawnType = PawnType)
	{
		if (GetOuter()->IsA(ActualPawnType))
		{
			return Cast<APawn>(GetOuter());
		}
	}
	return nullptr;
}

void UHCEquipmentInstance::SpawnEquipmentActors(const TArray<struct FHCEquipmentActorToSpawn>& ActorToSpawn)
{
	if (APawn* OwningPawn = GetPawn())
	{
		USceneComponent* AttachTarget = OwningPawn->GetRootComponent();
		if (ACharacter* Char = Cast<ACharacter>(OwningPawn))
		{
			AttachTarget = Char->GetMesh();
		}

		for (const FHCEquipmentActorToSpawn& SpawnInfo : ActorToSpawn)
		{
			AActor* NewActor = GetWorld()->SpawnActorDeferred<AActor>(SpawnInfo.ActorToSpawn, FTransform::Identity, OwningPawn);
			NewActor->FinishSpawning(FTransform::Identity, true);
			NewActor->SetActorRelativeTransform(SpawnInfo.AttachTransform);
			NewActor->AttachToComponent(AttachTarget, FAttachmentTransformRules::KeepRelativeTransform, SpawnInfo.AttachSocket);
			SpawnedActors.Add(NewActor);
		}
	}
}

void UHCEquipmentInstance::DestroyEquipmentActors()
{
	for (AActor* Actor : SpawnedActors)
	{
		if (Actor)
		{
			Actor->Destroy();
		}
	}
}

void UHCEquipmentInstance::OnEquipped()
{
	K2_OnEquipped();
}

void UHCEquipmentInstance::OnUnEquipped()
{
	K2_OnUnEquipped();
}
