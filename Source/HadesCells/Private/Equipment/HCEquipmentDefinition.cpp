// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/HCEquipmentDefinition.h"
#include "Equipment/HCEquipmentInstance.h"

UHCEquipmentDefinition::UHCEquipmentDefinition(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	InstanceType = UHCEquipmentInstance::StaticClass();
}
