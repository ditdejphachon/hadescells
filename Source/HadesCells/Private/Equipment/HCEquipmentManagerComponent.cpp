// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/HCEquipmentManagerComponent.h"
#include "Equipment/HCEquipmentDefinition.h"
#include "Equipment/UHCEquipmentData.h"

#include "Abilities/HCAbilitySystemComponent.h"
#include "Abilities/HCAbilitySet.h"
#include "AbilitySystemComponent.h"
#include <AbilitySystemGlobals.h>

#include "Components/ActorComponent.h"
#include <Heroes/HCHeroCharacter.h>
#include <Kismet/GameplayStatics.h>
#include <Characters/HCPlayerController.h>
#include <UIs/HCHUDBase.h>
#include <Equipment/Weapon/HCWeaponBase.h>
#include <Equipment/HCEquipmentPickUp.h>

FOnEquipmentAdded OnEquipmentAdded;
FHCEquipmentSlotFull OnEquipmentSlotFull;

#pragma region FHCAppliedEquipmentEntry
FString FHCAppliedEquipmentEntry::GetDebugString() const
{
	return FString::Printf(TEXT("[FHCAppliedEquipmentEntry] Debug string: [%s] of [%s]"), *GetNameSafe(Instance), *GetNameSafe(EquipmentDefinition.Get()));
}
#pragma endregion FHCAppliedEquipmentEntry

#pragma region FHCEquipmentList
UHCEquipmentInstance* FHCEquipmentList::AddEntry(TObjectPtr<UUHCEquipmentData> EquipmentData, UHCEquipmentManagerComponent* InEquipmentManagerComponent)
{
	// check if the equipment defifnition and the owner component are valid and has an authority
	check(EquipmentData != nullptr);
	check(OwnerComponent);
	check(OwnerComponent->GetOwner()->HasAuthority());
	check(InEquipmentManagerComponent);


	if (InEquipmentManagerComponent->MainWeaponEDCache && EquipmentData == InEquipmentManagerComponent->MainWeaponEDCache)
	{
		UE_LOG(LogTemp, Warning, TEXT("Same MainWeaponEDCache. NONO"));
		return nullptr;
	}
	if (InEquipmentManagerComponent->SecondaryWeaponEDCache && EquipmentData == InEquipmentManagerComponent->SecondaryWeaponEDCache)
	{
		UE_LOG(LogTemp, Warning, TEXT("Same SecondaryWeaponEDCache. NONO"));
		return nullptr;
	}
	if (InEquipmentManagerComponent->MainSkillEDCache && EquipmentData == InEquipmentManagerComponent->MainSkillEDCache)
	{
		UE_LOG(LogTemp, Warning, TEXT("Same MainSkillEDCache. NONO"));
		return nullptr;
	}
	if (InEquipmentManagerComponent->SecondarySkillEDCache && EquipmentData == InEquipmentManagerComponent->SecondarySkillEDCache)
	{
		UE_LOG(LogTemp, Warning, TEXT("Same SecondarySkillEDCache. NONO"));
		return nullptr;
	}

	// construct and add the new equipment entry into the entry list
	TSubclassOf<UHCEquipmentDefinition> InEquipmentDefinition = EquipmentData->EquipmentDefinition;
	const UHCEquipmentDefinition* EquipmentCD0 = GetDefault<UHCEquipmentDefinition>(InEquipmentDefinition);
	TSubclassOf<UHCEquipmentInstance> InstanceType = EquipmentCD0->InstanceType;
	if (InstanceType == nullptr)
	{
		InstanceType = UHCEquipmentInstance::StaticClass();
	}

	FHCAppliedEquipmentEntry NewEntry;
	NewEntry.EquipmentDefinition = InEquipmentDefinition;
	NewEntry.Instance = NewObject<UHCEquipmentInstance>(OwnerComponent->GetOwner(), InstanceType);


	auto GiveAbility = [&](UHCEquipmentInstance* EquipmentInstance, EHCEquipmentSlotType SlotType) {
		// Give ability
		if (UHCAbilitySystemComponent* HCASC = GetAbilitySystemComponent())
		{
			for (TObjectPtr<const UHCAbilitySet> AbilitySet : EquipmentCD0->AbilitySetsToGrant)
			{
				AbilitySet->GiveToAbililtySystem(HCASC, &NewEntry.GrantedHandles, SlotType, EquipmentCD0->AbilityLevel, EquipmentInstance);
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("[FHCEquipmentList::AddEntry] HCASC is nullptr"));
		}
	};

	const UHCEquipmentInstance* InstanceTypeCD0 = GetDefault<UHCEquipmentInstance>(InstanceType);

	if (InstanceTypeCD0)
	{
		switch (InstanceTypeCD0->EquipmentType)
		{
		case EHCEquipmentType::Weapon:
			if (!MainWeapon.IsValid())
			{
				GiveAbility(NewEntry.Instance, EHCEquipmentSlotType::MainWeapon);
				MainWeapon = NewEntry;
				MainWeapon.Instance->SpawnEquipmentActors(EquipmentCD0->ActorsToSpawn);
				InEquipmentManagerComponent->ShowWeaponFromSlotType(EHCEquipmentSlotType::MainWeapon);
				InEquipmentManagerComponent->OnEquipmentAdded.Broadcast(EquipmentData.Get(), EHCEquipmentSlotType::MainWeapon);
				InEquipmentManagerComponent->MainWeaponEDCache = EquipmentData.Get();
				InEquipmentManagerComponent->MainWeaponEICache = MainWeapon.Instance;
			}
			else if (!SecondaryWeapon.IsValid())
			{
				GiveAbility(NewEntry.Instance, EHCEquipmentSlotType::SecondaryWeapon);
				SecondaryWeapon = NewEntry;
				SecondaryWeapon.Instance->SpawnEquipmentActors(EquipmentCD0->ActorsToSpawn);
				InEquipmentManagerComponent->ShowWeaponFromSlotType(EHCEquipmentSlotType::SecondaryWeapon);
				InEquipmentManagerComponent->OnEquipmentAdded.Broadcast(EquipmentData.Get(), EHCEquipmentSlotType::SecondaryWeapon);
				InEquipmentManagerComponent->SecondaryWeaponEDCache = EquipmentData.Get();
				InEquipmentManagerComponent->SecondaryWeaponEICache = SecondaryWeapon.Instance;
			}
			else
			{
				NewEntry.Instance = nullptr;
				InEquipmentManagerComponent->OnEquipmentSlotFull.Broadcast(EquipmentData.Get(), EHCEquipmentType::Weapon);
			}
			break;
		case EHCEquipmentType::Skill:
			if (!MainSkill.IsValid())
			{
				GiveAbility(NewEntry.Instance, EHCEquipmentSlotType::MainSkill);
				MainSkill = NewEntry;
				InEquipmentManagerComponent->OnEquipmentAdded.Broadcast(EquipmentData.Get(), EHCEquipmentSlotType::MainSkill);
				InEquipmentManagerComponent->MainSkillEDCache = EquipmentData.Get();
				InEquipmentManagerComponent->MainSkillEICache = MainSkill.Instance;
			}
			else if (!SecondarySkill.IsValid())
			{
				GiveAbility(NewEntry.Instance, EHCEquipmentSlotType::SecondarySkill);
				SecondarySkill = NewEntry;
				InEquipmentManagerComponent->OnEquipmentAdded.Broadcast(EquipmentData.Get(), EHCEquipmentSlotType::SecondarySkill);
				InEquipmentManagerComponent->SecondarySkillEDCache = EquipmentData.Get();
				InEquipmentManagerComponent->SecondarySkillEICache = SecondarySkill.Instance;
			}
			else
			{
				NewEntry.Instance = nullptr;
				InEquipmentManagerComponent->OnEquipmentSlotFull.Broadcast(EquipmentData.Get(), EHCEquipmentType::Skill);
			}
			break;
		case EHCEquipmentType::Buff:
			GiveAbility(NewEntry.Instance, EHCEquipmentSlotType::None);
			break;
		default:
			break;
		}
	}
		
	return NewEntry.Instance;
}

void FHCEquipmentList::RemoveEntry(UHCEquipmentInstance* InInstanceToRemove, UHCEquipmentManagerComponent* InEquipmentManagerComponent, EHCEquipmentSlotType SlotType)
{
	check(InEquipmentManagerComponent);

	auto RemoveFromEntry = [&](FHCAppliedEquipmentEntry& Entry) {
		if (Entry.Instance == InInstanceToRemove)
		{
			if (UHCAbilitySystemComponent* HCASC = GetAbilitySystemComponent())
			{
				Entry.GrantedHandles.TakeFromAbilitySystem(HCASC);
			}

			InInstanceToRemove->DestroyEquipmentActors();
		}
	};

	if (InInstanceToRemove == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Cannot remove the equipment. InInstanceToRemove is invalid"));
		return;
	}

	switch (SlotType)
	{
		case EHCEquipmentSlotType::MainWeapon:
			RemoveFromEntry(MainWeapon);
			MainWeapon.Clear();
			InEquipmentManagerComponent->OnEquipmentRemoved.Broadcast(EHCEquipmentSlotType::MainWeapon);
			break;
		case EHCEquipmentSlotType::SecondaryWeapon:
			RemoveFromEntry(SecondaryWeapon);
			SecondaryWeapon.Clear();
			InEquipmentManagerComponent->OnEquipmentRemoved.Broadcast(EHCEquipmentSlotType::SecondaryWeapon);

			break;
		case EHCEquipmentSlotType::MainSkill:
			RemoveFromEntry(MainSkill);
			MainSkill.Clear();
			InEquipmentManagerComponent->OnEquipmentRemoved.Broadcast(EHCEquipmentSlotType::MainSkill);

			break;
		case EHCEquipmentSlotType::SecondarySkill:
			RemoveFromEntry(SecondarySkill);
			SecondarySkill.Clear();
			InEquipmentManagerComponent->OnEquipmentRemoved.Broadcast(EHCEquipmentSlotType::SecondarySkill);

			break;
		default:
			break;
	}
	//
}

UHCAbilitySystemComponent* FHCEquipmentList::GetAbilitySystemComponent() const
{
	check(OwnerComponent);
	AActor* OwningActor = OwnerComponent->GetOwner();
	return Cast<UHCAbilitySystemComponent>(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(OwningActor));
}
#pragma endregion FHCEquipmentList

UHCEquipmentManagerComponent::UHCEquipmentManagerComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, EquipmentList(this)
{
	bWantsInitializeComponent = true;
}

UHCEquipmentInstance* UHCEquipmentManagerComponent::EquipItem(UUHCEquipmentData* EquipmentData)
{
	UHCEquipmentInstance* Result = nullptr;
	if (EquipmentData)
	{
		Result = EquipmentList.AddEntry(EquipmentData, this);
		if (Result)
		{
			Result->OnEquipped();

			if (IsUsingRegisteredSubObjectList() && IsReadyForReplication())
			{
				AddReplicatedSubObject(Result);
			}
		}
	}
	return Result;
}

void UHCEquipmentManagerComponent::UnEquipItem(UHCEquipmentInstance* InInstanceToRemove, EHCEquipmentSlotType SlotType)
{
	if (InInstanceToRemove)
	{
		if (IsUsingRegisteredSubObjectList())
		{
			RemoveReplicatedSubObject(InInstanceToRemove);
		}
 		InInstanceToRemove->OnUnEquipped();
		EquipmentList.RemoveEntry(InInstanceToRemove, this, SlotType);
	}
}

void UHCEquipmentManagerComponent::ReplaceItem(UUHCEquipmentData* EquipmentData, EHCEquipmentType EquipmentType, EHCEquipmentSlotType SlotType)
{
	UHCEquipmentInstance* EI = nullptr;
	UUHCEquipmentData* ED = nullptr;
	if (EquipmentType == EHCEquipmentType::Weapon)
	{
		if (SlotType == EHCEquipmentSlotType::MainWeapon)
		{
			EI = MainWeaponEICache;
			ED = MainWeaponEDCache;
		}
		else if (SlotType == EHCEquipmentSlotType::SecondaryWeapon)
		{
			EI = SecondaryWeaponEICache;
			ED = SecondaryWeaponEDCache;
		}
	}
	else if (EquipmentType == EHCEquipmentType::Skill)
	{
		if (SlotType == EHCEquipmentSlotType::MainSkill)
		{
			EI = MainSkillEICache;
			ED = MainSkillEDCache;
		}
		else if (SlotType == EHCEquipmentSlotType::SecondarySkill)
		{
			EI = SecondarySkillEICache;
			ED = SecondarySkillEDCache;
		}
	}

	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
	{
		if (AHCEquipmentPickUp* CurrentPickUp = Cast<AHCEquipmentPickUp>(Hero->CurrentInteractableComponent))
		{
			CurrentPickUp->ReplaceEquipmentData(ED);
		}

		Hero->CurrentInteractableComponent = nullptr;
	}

	UnEquipItem(EI, SlotType);
	EquipItem(EquipmentData);

}

void UHCEquipmentManagerComponent::InitializeComponent()
{
	Super::InitializeComponent();
}

void UHCEquipmentManagerComponent::UninitializeComponent()
{
	//TArray<UHCEquipmentInstance*> AllEquipmentInstances;

	//// gathering all instances before removal to avoid side effects affecting the equipment list iterator	
	//for (const FHCAppliedEquipmentEntry& Entry : EquipmentList.Entries)
	//{
	//	AllEquipmentInstances.Add(Entry.Instance);
	//}

	//for (UHCEquipmentInstance* Instance: AllEquipmentInstances)
	//{
	//	UnEquipItem(Instance);
	//}
	UnEquipItem(EquipmentList.MainWeapon.Instance, EHCEquipmentSlotType::MainWeapon);
	UnEquipItem(EquipmentList.SecondaryWeapon.Instance, EHCEquipmentSlotType::SecondaryWeapon);
	UnEquipItem(EquipmentList.MainSkill.Instance, EHCEquipmentSlotType::MainSkill);
	UnEquipItem(EquipmentList.SecondarySkill.Instance, EHCEquipmentSlotType::SecondarySkill);

	Super::UninitializeComponent();
}

TArray<AActor*> UHCEquipmentManagerComponent::GetSpawnActorFromEquipmentSlotType(EHCEquipmentSlotType SlotType)
{
	TArray<AActor*> Result;

	switch (SlotType)
	{
		case EHCEquipmentSlotType::MainWeapon:
			if (EquipmentList.MainWeapon.IsValid())
			{
				Result = EquipmentList.MainWeapon.Instance.Get()->GetSpawnedActor();
			}
			break;
		case EHCEquipmentSlotType::SecondaryWeapon:
			if (EquipmentList.SecondaryWeapon.IsValid())
			{
				Result = EquipmentList.SecondaryWeapon.Instance.Get()->GetSpawnedActor();
			}
			break;
		case EHCEquipmentSlotType::MainSkill:
			if (EquipmentList.MainSkill.IsValid())
			{
				Result = EquipmentList.MainSkill.Instance.Get()->GetSpawnedActor();
			}
			break;
		case EHCEquipmentSlotType::SecondarySkill:
			if (EquipmentList.SecondaryWeapon.IsValid())
			{
				Result = EquipmentList.SecondarySkill.Instance.Get()->GetSpawnedActor();
			}
			break;
		default:
			break;
	}
	return Result;
}

void UHCEquipmentManagerComponent::ShowWeaponFromSlotType(EHCEquipmentSlotType SlotType)
{
	TArray<AActor*> CurrentMainWeapons = GetSpawnActorFromEquipmentSlotType(EHCEquipmentSlotType::MainWeapon);
	TArray<AActor*> CurrentSecondaryWeapons = GetSpawnActorFromEquipmentSlotType(EHCEquipmentSlotType::SecondaryWeapon);

	auto SetWeaponHidden = [&](bool IsSetMainWeaponHidden) {
		if (CurrentMainWeapons.IsValidIndex(0) && CurrentMainWeapons[0])
		{
			for (auto& spawnedActorFromMainWeapon : CurrentMainWeapons)
			{
				spawnedActorFromMainWeapon->SetActorHiddenInGame(IsSetMainWeaponHidden ? true : false);
			}
		}
		
		if (CurrentSecondaryWeapons.IsValidIndex(0) && CurrentSecondaryWeapons[0])
		{
			for (auto& spawnedActorFromSecondaryWeapon : CurrentSecondaryWeapons)
			{
				spawnedActorFromSecondaryWeapon->SetActorHiddenInGame(IsSetMainWeaponHidden ? false : true);
			}
		}
	
	};

	switch (SlotType)
	{
		case EHCEquipmentSlotType::MainWeapon:
			if (EquipmentList.MainWeapon.IsValid())
			{
				SetWeaponHidden(false);
			}
			break;
		case EHCEquipmentSlotType::SecondaryWeapon:
			if (EquipmentList.SecondaryWeapon.IsValid())
			{
				SetWeaponHidden(true);
			}
			break;
		default:
			UE_LOG(LogTemp, Warning, TEXT("[UHCEquipmentManagerComponent::ShowWeaponFromSlotType] [WARNING] Invalid EHCEquipmentSlotType"));
			break;
	}
}

/*
UHCEquipmentInstance* UHCEquipmentManagerComponent::GetFirstInstanceOfType(TSubclassOf<UHCEquipmentInstance> InInstanceType)
{
	for (const FHCAppliedEquipmentEntry& Entry : EquipmentList.Entries)
	{
		if (UHCEquipmentInstance* Instance = Entry.Instance)
		{
			if (Instance->IsA(InInstanceType))
			{
				return Instance;
			}
		}
	}
	return nullptr;
}

TArray<UHCEquipmentInstance*> UHCEquipmentManagerComponent::GetInstancesOfType(TSubclassOf<UHCEquipmentInstance> InInstanceType)
{
	TArray<UHCEquipmentInstance*> Results;
	for (const FHCAppliedEquipmentEntry& Entry : EquipmentList.Entries)
	{
		if (UHCEquipmentInstance* Instance = Entry.Instance)
		{
			if (Instance->IsA(InInstanceType))
			{
				Results.Add(Instance);
			}
		}
	}
	return Results;
}
*/
