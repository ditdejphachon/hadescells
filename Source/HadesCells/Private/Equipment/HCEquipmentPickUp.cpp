// Fill out your copyright notice in the Description page of Project Settings.


#include "Equipment/HCEquipmentPickUp.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Equipment/UHCEquipmentData.h"
#include "Heroes/HCHeroCharacter.h"
#include "Equipment/HCEquipmentManagerComponent.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"

// Sets default values
AHCEquipmentPickUp::AHCEquipmentPickUp(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnerMesh"));
	SpawnerMesh->SetupAttachment(RootComponent);

	EquipmentDisplayMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EquipmentMesh"));
	EquipmentDisplayMesh->SetupAttachment(RootComponent);

	TotalTime = 0.0f;
	Amplitude = 100.0f; 
	Period = 2.0f;
}

// Called when the game starts or when spawned
void AHCEquipmentPickUp::BeginPlay()
{
	Super::BeginPlay();
}

void AHCEquipmentPickUp::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
}

void AHCEquipmentPickUp::PlayPickUpEffects()
{
	if (EquipmentData->PickedUpSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, EquipmentData->PickedUpSound, GetActorLocation());
	}

	if (EquipmentData->PickedUpNiagaraEffect)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), EquipmentData->PickedUpNiagaraEffect, GetActorLocation());
	}

	if (EquipmentData->PickedUpParticleEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, EquipmentData->PickedUpParticleEffect, GetActorLocation());
	}
}

void AHCEquipmentPickUp::SetDisplayMesh()
{
	if (EquipmentData && EquipmentData->DisplayMesh)
	{
		EquipmentDisplayMesh->SetStaticMesh(EquipmentData->DisplayMesh);
		EquipmentDisplayMesh->SetRelativeLocation(EquipmentData->DisplayMeshOffset);
		EquipmentDisplayMesh->SetRelativeScale3D(EquipmentData->DisplayMeshScale);
	}
}

// Called every frame
void AHCEquipmentPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (EquipmentDisplayMesh)
	{
		TotalTime += DeltaTime;
		float SinValue = Amplitude * FMath::Sin(Period * TotalTime);

		FVector NewLocation = EquipmentDisplayMesh->GetRelativeLocation();
		NewLocation.Z += SinValue; // Adjust the Z position of the actor to move it up and down
		EquipmentDisplayMesh->SetRelativeLocation(NewLocation);
		EquipmentDisplayMesh->AddRelativeRotation(FRotator(0.0f, GetWorld()->GetDeltaSeconds() * EquipmentMeshRotationSpeed, 0.0f));
	}
}

void AHCEquipmentPickUp::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	SetDisplayMesh();
}

void AHCEquipmentPickUp::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
	AHCInteractableComponent::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepHitResult);
}

void AHCEquipmentPickUp::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AHCInteractableComponent::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}

bool AHCEquipmentPickUp::GiveEquipment(AHCHeroCharacter* ReceivingHero)
{
	UHCEquipmentInstance* EquipmentInstance = ReceivingHero->EquipmentManagerComponent.Get()->EquipItem(EquipmentData.Get());
	return EquipmentInstance == nullptr ? false : true;
}

void AHCEquipmentPickUp::BeginInteraction(AHCHeroCharacter* ReceivingHero)
{
	if (GiveEquipment(ReceivingHero))
	{
		PlayPickUpEffects();
		EquipmentDisplayMesh->DestroyComponent();
		CollisionCapsule->DestroyComponent();
	}
	else
	{
		PlayPickUpEffects();
	}
}

void AHCEquipmentPickUp::ReplaceEquipmentData(UUHCEquipmentData* NewED)
{
	EquipmentData = NewED;
	SetDisplayMesh();
}
