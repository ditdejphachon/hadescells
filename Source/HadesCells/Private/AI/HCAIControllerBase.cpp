// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/HCAIControllerBase.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"

AHCAIControllerBase::AHCAIControllerBase()
{
    BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));
}

void AHCAIControllerBase::BeginPlay()
{
	Super::BeginPlay();
}

void AHCAIControllerBase::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    // If you have a Behavior Tree asset assigned, run it on AI Possession
    if (BehaviorTree)
    {
        BlackboardComponent->InitializeBlackboard(*(BehaviorTree->BlackboardAsset));
        RunBehaviorTree(BehaviorTree);
    }
}
