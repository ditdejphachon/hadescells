// Fill out your copyright notice in the Description page of Project Settings.


#include "HCCharacterMovementComponent.h"

#pragma region includes
#include "Characters/HCCharacterBase.h"
#include "GameplayTagContainer.h"
#include "AbilitySystemComponent.h"
#pragma endregion

UHCCharacterMovementComponent::UHCCharacterMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// TODO: init multiplier
}

float UHCCharacterMovementComponent::GetMaxSpeed() const
{
	AHCCharacterBase* Owner = Cast<AHCCharacterBase>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("%s() No Owner"), *FString(__FUNCTION__));
		return Super::GetMaxSpeed();
	}

	if (!Owner->IsAlive())
	{
		return 0.f;
	}

	// TODO: add stun
	if (
		Owner->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Stun"))) 
		|| Owner->GetAbilitySystemComponent()->HasMatchingGameplayTag(FGameplayTag::RequestGameplayTag(FName("State.Debuff.Root")))
		)
	{
		return 0.0f;
	}

	return Owner->GetMoveSpeed();
}