// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCHUDBase.h"
#include "UIs/HCW_HeroOverlay.h"
#include "UIs/HCW_SwapEquipment.h"
#include "UIs/HCW_EquipmentSlot.h"
#include "UIs/HCW_UpgradeEquipment.h"

#include <Characters/HCHealthComponent.h>
#include "Equipment/HCEquipmentManagerComponent.h"
#include <HadesCells/HadesCellsGameMode.h>
#include <Kismet/GameplayStatics.h>
#include <Heroes/HCHeroCharacter.h>

AHCHUDBase::AHCHUDBase(const FObjectInitializer& ObjectInitializer)
{
}

void AHCHUDBase::BeginPlay()
{
	Super::BeginPlay();

	if (WBP_HeroOverlay)
	{
		W_HeroOverlay = CreateWidget<UHCW_HeroOverlay>(GetWorld(), WBP_HeroOverlay);
	}

	if (WBP_SwapEquipment)
	{
		W_SwapEquipment = CreateWidget<UHCW_SwapEquipment>(GetWorld(), WBP_SwapEquipment);
	}
}

void AHCHUDBase::ToggleHeroOverlay(bool bIsShow)
{
	if (W_HeroOverlay)
	{
		if (bIsShow)
		{
			W_HeroOverlay->AddToViewport();
		}
		else
		{
			W_HeroOverlay->RemoveFromParent();
		}
	}
}

void AHCHUDBase::SetUpHeroOverlayHealthSection(UHCHealthComponent* InHealthComponent)
{
	if (W_HeroOverlay)
	{
		if (InHealthComponent)
		{
			W_HeroOverlay->SetHealth(InHealthComponent->GetHealth(), InHealthComponent->GetMaxHealth(), InHealthComponent->GetHealthNormalized());
			W_HeroOverlay->SetShield(InHealthComponent->GetShield(), InHealthComponent->GetMaxShield(), InHealthComponent->GetShieldNormalized());
		}
	}
}

void AHCHUDBase::SetUpHeroGoldSection(UHCProgressionSystemComponent* InProgressionComponent)
{
	if (W_HeroOverlay)
	{
		if (InProgressionComponent)
		{
			W_HeroOverlay->SetGold(InProgressionComponent->GetGold());
		}
	}
}

void AHCHUDBase::SetUpHeroExpSection(UHCProgressionSystemComponent* InProgressionComponent)
{
	if (W_HeroOverlay)
	{
		if (InProgressionComponent)
		{
			W_HeroOverlay->SetExp(InProgressionComponent->GetExpNormalized());
		}
	}
}

void AHCHUDBase::SetUpHeroOverlayEquipmentSection(UHCEquipmentManagerComponent* InEquipmentManagerComponent)
{
	if (W_HeroOverlay)
	{
		if (InEquipmentManagerComponent)
		{
			InEquipmentManagerComponent->OnEquipmentAdded.AddUObject(this, &AHCHUDBase::HandleEquipmentAdded);
			InEquipmentManagerComponent->OnEquipmentSlotFull.AddUObject(this, &AHCHUDBase::HandleEquipmentSlotFull);
		}
	}
}

void AHCHUDBase::UpdateEquipmentSlot(const UHCEquipmentDefinition* InEquipmentDefinition)
{
}

void AHCHUDBase::ToggleEquipmentSwappingUI(bool bIsShow, UUHCEquipmentData* EquipmentData, EHCEquipmentType EquipmentType)
{
	W_SwapEquipment = CreateWidget<UHCW_SwapEquipment>(GetWorld(), WBP_SwapEquipment);

	if (W_SwapEquipment)
	{
		if (bIsShow)
		{
			if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
			{
				if (UHCEquipmentManagerComponent* EMC = Hero->EquipmentManagerComponent)
				{
					TArray<UUHCEquipmentData*> EDs;
					if (EquipmentType == EHCEquipmentType::Weapon)
					{
						EDs.Add(EMC->MainWeaponEDCache);
						EDs.Add(EMC->SecondaryWeaponEDCache);
					}
					else if (EquipmentType == EHCEquipmentType::Skill)
					{
						EDs.Add(EMC->MainSkillEDCache);
						EDs.Add(EMC->SecondarySkillEDCache);
					}
					EDs.Add(EquipmentData);
					W_SwapEquipment->AddToViewport();
					W_SwapEquipment->SetEquipmentImages(EDs, EquipmentType);
					W_SwapEquipment->SetUserFocus(UGameplayStatics::GetPlayerController(GetWorld(), 0));
				}
			}
		}
		else
		{
			W_SwapEquipment->RemoveFromParent();
		}
	}
}

void AHCHUDBase::SetUpEnemyKillUI(uint8 EnemiesToKill)
{
	if (W_HeroOverlay)
	{
		W_HeroOverlay->SetTotalEnemyAmount(EnemiesToKill);
	}
}

void AHCHUDBase::UpdateEnemyKillUI(uint8 EnemyKilled)
{
	if (W_HeroOverlay)
	{
		W_HeroOverlay->SetEnemyKilledAmount(EnemyKilled);
	}
}

void AHCHUDBase::EnableEquipmentCooldown(const FGameplayTag& InTag, float InCooldownTime)
{
	if (IsValid(W_HeroOverlay))
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHUDBase::EnableEquipmentCooldown] InTag: [%s] InCooldownTime: [%f]"), *InTag.ToString(), InCooldownTime);
		if (InTag == FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item1")))
		{
			W_HeroOverlay->MainSkillSlot->EnableCoolDown(InCooldownTime);
		}
		else if (InTag ==FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item2")))
		{
			W_HeroOverlay->SecondarySkillSlot->EnableCoolDown(InCooldownTime);
		}
	}
}

void AHCHUDBase::ToggleDeathScreen(bool bIsShow)
{
	W_DeathScreen = CreateWidget(GetWorld(), WBP_DeathScreen);

	if (IsValid(W_DeathScreen))
	{
		if (bIsShow)
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCHUDBase::ToggleDeathScreen] showing death screen"));
			W_DeathScreen->AddToViewport();
		}
		else
		{
			W_DeathScreen->RemoveFromParent();
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("[AHCHUDBase::ToggleDeathScreen] W_DeathScreen is invalid. NANI!!!"));
}

void AHCHUDBase::ToggleEquipmentUpgradeUI(bool bIsShow)
{
	W_UpgradeEquipment = CreateWidget<UHCW_UpgradeEquipment>(GetWorld(), WBP_UpgradeEquipment);

	if (W_UpgradeEquipment)
	{
		if (bIsShow)
		{
			if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
			{
				if (UHCEquipmentManagerComponent* EMC = Hero->EquipmentManagerComponent)
				{
					TArray<UUHCEquipmentData*> EDs;
					EDs.Add(EMC->MainWeaponEDCache);
					EDs.Add(EMC->SecondaryWeaponEDCache);
					EDs.Add(EMC->MainSkillEDCache);
					EDs.Add(EMC->SecondarySkillEDCache);

					W_UpgradeEquipment->AddToViewport();
					W_UpgradeEquipment->SetEquipmentList(EDs);
					W_UpgradeEquipment->SetUserFocus(UGameplayStatics::GetPlayerController(GetWorld(), 0));
					UGameplayStatics::SetGamePaused(GetWorld(), true);
				}
			}
		}
		else
		{
			W_UpgradeEquipment->RemoveFromParent();
		}
	}
}

void AHCHUDBase::HandleEquipmentAdded(UUHCEquipmentData* EquipmentData, EHCEquipmentSlotType SlotType)
{
	if (W_HeroOverlay && EquipmentData)
	{
		W_HeroOverlay->SetEquipmentSlot(EquipmentData, SlotType);
	}
}

void AHCHUDBase::HandleEquipmentSlotFull(UUHCEquipmentData* EquipmentData, EHCEquipmentType EquipmentType)
{
	UGameplayStatics::SetGamePaused(GetWorld(), true);
	ToggleEquipmentSwappingUI(true, EquipmentData, EquipmentType);
}

