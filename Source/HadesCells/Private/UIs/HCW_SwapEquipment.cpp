// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_SwapEquipment.h"
#include "Components/Image.h"
#include "Engine/Texture2D.h"
#include <Equipment/UHCEquipmentData.h>
#include <Heroes/HCHeroCharacter.h>
#include <Kismet/GameplayStatics.h>
#include "Equipment/HCEquipmentManagerComponent.h"

void UHCW_SwapEquipment::SetEquipmentImages(TArray<UUHCEquipmentData*> EquipmentDatas, EHCEquipmentType EquipmentType)
{
	if (EquipmentDatas.Num() < 3)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UHCW_SwapEquipment::SetEquipmentImages], EquipmentDatas's size is less than 3"));
		return;
	}

	EDs = EquipmentDatas;
	EquipmentTypeToReplace = EquipmentType;
	EquipmentDataToReplace = EquipmentDatas[2];

	if (Equipment_Image_1)
	{
		Equipment_Image_1->SetBrushFromTexture(EquipmentDatas[0]->DisplayIcon);
	}
	if (Equipment_Image_2)
	{
		Equipment_Image_2->SetBrushFromTexture(EquipmentDatas[1]->DisplayIcon);
	}
	if (Equipment_Image_3)
	{
		Equipment_Image_3->SetBrushFromTexture(EquipmentDatas[2]->DisplayIcon);
	}
	
	MoveLeft();
}

void UHCW_SwapEquipment::MoveLeft()
{
	Equipment_Border_1->SetVisibility(ESlateVisibility::Visible);
	Equipment_Border_2->SetVisibility(ESlateVisibility::Hidden);
}

void UHCW_SwapEquipment::MoveRight()
{
	Equipment_Border_1->SetVisibility(ESlateVisibility::Hidden);
	Equipment_Border_2->SetVisibility(ESlateVisibility::Visible);
}

FReply UHCW_SwapEquipment::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	if (InKeyEvent.GetKey() == EKeys::A || InKeyEvent.GetKey() == EKeys::Left)
	{
		MoveLeft();
		return FReply::Handled();
	}

	if (InKeyEvent.GetKey() == EKeys::D || InKeyEvent.GetKey() == EKeys::Right)
	{
		MoveRight();
		return FReply::Handled();
	}

	if (InKeyEvent.GetKey() == EKeys::E || InKeyEvent.GetKey() == EKeys::Enter)
	{
		ConfirmReplacement();
		return FReply::Handled();
	}

	return FReply::Unhandled();
}

void UHCW_SwapEquipment::NativeConstruct()
{
	Super::NativeConstruct();

	if (Equipment_Image_1)
	{
		Equipment_Image_1->OnMouseButtonDownEvent.BindDynamic(this, &UHCW_SwapEquipment::OnClickImage_1);
	}

	if (Equipment_Image_2)
	{
		Equipment_Image_2->OnMouseButtonDownEvent.BindDynamic(this, &UHCW_SwapEquipment::OnClickImage_2);
	}
}

void UHCW_SwapEquipment::ConfirmReplacement()
{
	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
	{
		if (UHCEquipmentManagerComponent* EMC = Hero->EquipmentManagerComponent)
		{
			EHCEquipmentSlotType SlotType = EHCEquipmentSlotType::MainWeapon;

			bool bIsMain = Equipment_Border_1->GetVisibility() == ESlateVisibility::Visible;
			if (EquipmentTypeToReplace == EHCEquipmentType::Weapon)
			{
				SlotType = bIsMain ? EHCEquipmentSlotType::MainWeapon : EHCEquipmentSlotType::SecondaryWeapon;
			}
			else if (EquipmentTypeToReplace == EHCEquipmentType::Skill)
			{
				SlotType = bIsMain ? EHCEquipmentSlotType::MainSkill : EHCEquipmentSlotType::SecondarySkill;
			}

			EMC->ReplaceItem(EquipmentDataToReplace, EquipmentTypeToReplace, SlotType);
			RemoveFromParent();
			UGameplayStatics::SetGamePaused(GetWorld(), false);

		}
	}
}

FEventReply UHCW_SwapEquipment::OnClickImage_1(FGeometry MyGeometry, const FPointerEvent& MouseEvent)
{
	MoveLeft();
	return FEventReply(true);
}

FEventReply UHCW_SwapEquipment::OnClickImage_2(FGeometry MyGeometry, const FPointerEvent& MouseEvent)
{
	MoveRight();
	return FEventReply(true);
}