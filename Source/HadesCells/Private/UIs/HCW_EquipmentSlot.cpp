// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_EquipmentSlot.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Engine/Texture2D.h"

UHCW_EquipmentSlot::UHCW_EquipmentSlot(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UHCW_EquipmentSlot::SetEquipmentImage(UTexture2D* InImage)
{
	if (InImage)
	{
		EquipmentImage->SetBrushFromTexture(InImage);
	}
}

void UHCW_EquipmentSlot::EnableCoolDown(float InCooldownTime)
{
	if (CooldownImage && CooldownTimeRemaining)
	{
		MaxCooldownTime = InCooldownTime;
		CurrentTimeRemaining = InCooldownTime;

		FTimerDelegate TimerDelegate;
		TimerDelegate.BindLambda([&]() {
			CurrentTimeRemaining -= FMath::Clamp(CooldownDelay, 0.0f, MaxCooldownTime);
			FNumberFormattingOptions Options;
			{
				Options.RoundingMode = ERoundingMode::HalfToEven;
				Options.AlwaysSign = false;
				Options.UseGrouping = true;
				Options.MinimumIntegralDigits = 1;
				Options.MaximumIntegralDigits = 4;
				Options.MinimumFractionalDigits = 0;
				Options.MaximumFractionalDigits = 0;
			}
			
			FText CurrentText = FText::AsNumber(CurrentTimeRemaining, &Options);
			if (CooldownTimeRemaining)
			{
				CooldownTimeRemaining->SetText(CurrentText);
			}

			if (CurrentTimeRemaining <= 0.0f)
			{
				if (CooldownImage && CooldownTimeRemaining)
				{
					CooldownImage->SetVisibility(ESlateVisibility::Hidden);
					CooldownTimeRemaining->SetVisibility(ESlateVisibility::Hidden);
				}
				
				GetWorld()->GetTimerManager().ClearTimer(CooldownHandle);
			}
		});

		GetWorld()->GetTimerManager().SetTimer(CooldownHandle, TimerDelegate, CooldownDelay, true);

		{
			CooldownImage->SetVisibility(ESlateVisibility::Visible);
			CooldownTimeRemaining->SetVisibility(ESlateVisibility::Visible);
		}
	}
}

void UHCW_EquipmentSlot::NativeConstruct()
{
	Super::NativeConstruct();

	auto SetActivateButtonImageTexture = [this](const FString& InAssetPath) {
		ActivateButtonImageTexture = LoadObject<UTexture2D>(nullptr, *InAssetPath);
		if (ActivateButtonImageTexture && ActivateButtonImage)
		{
			ActivateButtonImage->SetBrushFromTexture(ActivateButtonImageTexture);

		}
	};

	switch (EquipmentType)
	{
		case EHCEquipmentSlotType::MainWeapon:
			SetActivateButtonImageTexture("/Script/Engine.Texture2D'/Game/Blueprints/UIs/Assets/Icons/Control/Control_Mouse_Left_Click.Control_Mouse_Left_Click'");
			break;
		case EHCEquipmentSlotType::SecondaryWeapon:
			SetActivateButtonImageTexture("/Script/Engine.Texture2D'/Game/Blueprints/UIs/Assets/Icons/Control/Control_Mouse_Right_Click.Control_Mouse_Right_Click'");
			break;
		case EHCEquipmentSlotType::MainSkill:
			SetActivateButtonImageTexture("/Script/Engine.Texture2D'/Game/Blueprints/UIs/Assets/Icons/Control/Control_Q.Control_Q'");
			break;
		case EHCEquipmentSlotType::SecondarySkill:
			SetActivateButtonImageTexture("/Script/Engine.Texture2D'/Game/Blueprints/UIs/Assets/Icons/Control/Control_E.Control_E'");
			break;
		default:
			break;
	}

	MaxCooldownTime = 0.0f;
}

void UHCW_EquipmentSlot::NativeDestruct()
{
	GetWorld()->GetTimerManager().ClearTimer(CooldownHandle);
}
