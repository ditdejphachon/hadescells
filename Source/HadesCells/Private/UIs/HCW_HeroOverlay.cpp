// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_HeroOverlay.h"

#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include <Kismet/KismetTextLibrary.h>
#include "Equipment/UHCEquipmentData.h"
#include "Equipment/HCEquipmentDefinition.h"
#include <Equipment/HCEquipmentInstance.h>
#include "UIs/HCW_EquipmentSlot.h"
#include "UIs/HCW_GameModeInfo.h"

void UHCW_HeroOverlay::NativeConstruct()
{
	Super::NativeConstruct();
}

void UHCW_HeroOverlay::NativeDestruct()
{
	Super::NativeDestruct();
}

void UHCW_HeroOverlay::SetHealth(const float& InCurrentValue, const float& InMaxValue, const float& InNormalizedValue)
{
	FText NewHealthText = GetAttributeText(InCurrentValue, InMaxValue);
	SetText(HealthText, NewHealthText);
	SetProgressBar(HealthBar, InNormalizedValue);
}

void UHCW_HeroOverlay::SetShield(const float& InCurrentValue, const float& InMaxValue, const float& InNormalizedValue)
{
	FText NewShieldText = GetAttributeText(InCurrentValue, InMaxValue);
	SetText(ShieldText, NewShieldText);
	SetProgressBar(ShieldBar, InNormalizedValue);
}

void UHCW_HeroOverlay::SetEquipmentSlot(UUHCEquipmentData* InEquipmentData, EHCEquipmentSlotType SlotType)
{
	if (InEquipmentData)
	{
		switch (SlotType)
		{
		case EHCEquipmentSlotType::MainWeapon:
			if (MainWeaponSlot)
			{
				MainWeaponSlot->SetEquipmentImage(InEquipmentData->DisplayIcon);
			}
			break;
		case EHCEquipmentSlotType::SecondaryWeapon:
			if (SecondaryWeaponSlot)
			{
				SecondaryWeaponSlot->SetEquipmentImage(InEquipmentData->DisplayIcon);
			}
			break;
		case EHCEquipmentSlotType::MainSkill:
			if (MainSkillSlot)
			{
				MainSkillSlot->SetEquipmentImage(InEquipmentData->DisplayIcon);
			}
			break;
		case EHCEquipmentSlotType::SecondarySkill:
			if (SecondarySkillSlot)
			{
				SecondarySkillSlot->SetEquipmentImage(InEquipmentData->DisplayIcon);
			}
			break;
		default:
			break;
		}
	}
}

void UHCW_HeroOverlay::SetEnemyKilledAmount(uint8 InKilledAmount)
{
	if (GameModeInfo)
	{
		GameModeInfo->SetEnemyKilledAmount(InKilledAmount);
	}
}

void UHCW_HeroOverlay::SetTotalEnemyAmount(uint8 InTotalAmount)
{
	if (GameModeInfo)
	{
		GameModeInfo->SetTotalEnemyAmount(InTotalAmount);
	}
}

void UHCW_HeroOverlay::SetExp(const float& InNormalizedValue)
{
	if (ExpBar)
	{
		SetProgressBar(ExpBar, InNormalizedValue);
		K2_OnExpChanged();
	}
}

void UHCW_HeroOverlay::SetGold(const float& InCurrentValue)
{
	if (GoldText)
	{
		FNumberFormattingOptions Options;
		Options.RoundingMode = ERoundingMode::HalfToEven;
		Options.AlwaysSign = false;
		Options.UseGrouping = true;
		Options.MinimumIntegralDigits = 1;
		Options.MaximumIntegralDigits = 4;
		Options.MinimumFractionalDigits = 0;
		Options.MaximumFractionalDigits = 0;

		FText CurrentText = FText::AsNumber(InCurrentValue, &Options);
		SetText(GoldText, CurrentText);
		K2_OnGoldChanged();
	}
}

FText UHCW_HeroOverlay::GetAttributeText(const float& InCurrentValue, const float& InMaxValue)
{
	FNumberFormattingOptions Options;
	Options.RoundingMode = ERoundingMode::HalfToEven;
	Options.AlwaysSign = false;
	Options.UseGrouping = true;
	Options.MinimumIntegralDigits = 1;
	Options.MaximumIntegralDigits = 4;
	Options.MinimumFractionalDigits = 0;
	Options.MaximumFractionalDigits = 0;

	FText CurrentText = FText::AsNumber(InCurrentValue, &Options);
	FText MaxText = FText::AsNumber(InMaxValue, &Options);
	FText NewValueText = FText::Format(FText::FromString("{0} / {1}"), CurrentText, MaxText);

	return NewValueText;
}

void UHCW_HeroOverlay::SetText(UTextBlock* InTextBlock, const FText& InText)
{
	if (InTextBlock)
	{
		InTextBlock->SetText(InText);
	}
}

void UHCW_HeroOverlay::SetProgressBar(UProgressBar* InProgressBar, const float& InValue)
{
	if (InProgressBar)
	{
		InProgressBar->SetPercent(InValue);
	}
}
