// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_FloatingStatusBar.h"
#include "Components/ProgressBar.h"
#include "Components/VerticalBox.h"
#include "Components/VerticalBoxSlot.h"


void UHCW_FloatingStatusBar::SetHealthPercentage(float HealthPercentage)
{
	if (HealthBar)
	{
		HealthBar->SetPercent(HealthPercentage);
	}
}

void UHCW_FloatingStatusBar::SetShieldPercentage(float ShieldPercentage)
{
	if (ShieldBar)
	{
		ShieldBar->SetPercent(ShieldPercentage);
	}
}

void UHCW_FloatingStatusBar::SetPosturePercentage(float PosturePercentage)
{
	if (PostureBar)
	{
		PostureBar->SetPercent(PosturePercentage);
	}
}

void UHCW_FloatingStatusBar::RemoveShieldBar()
{
	// NOTE: left this here just in case
	/*if (ShieldBar && StatusVerticalBox)
	{
		int32 ChildIndex = StatusVerticalBox->GetChildIndex(ShieldBar);
		if (ChildIndex != INDEX_NONE)
		{
			if (UVerticalBoxSlot* Slot = Cast<UVerticalBoxSlot>(StatusVerticalBox->GetChildAt(ChildIndex)))
			{
				FSlateChildSize Size;
				Size.SizeRule = ESlateSizeRule::Fill;
				Size.Value = 0.0f;

				Slot->SetSize(Size);
			}
		}
	}*/

	if (ShieldBar && StatusVerticalBox)
	{
		StatusVerticalBox->RemoveChild(ShieldBar);
	}
}
