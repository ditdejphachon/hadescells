// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_DamagePopUp.h"
#include "Components/TextBlock.h"
#include <Heroes/HCHeroCharacter.h>
#include <Characters/HCPlayerController.h>

void UHCW_DamagePopUp::SetDamageText(const float& InDamageValue, const FGameplayTagContainer& AdditionalTagContainer)
{
	FNumberFormattingOptions Options;
	Options.RoundingMode = ERoundingMode::HalfToEven;
	Options.AlwaysSign = false;
	Options.UseGrouping = true;
	Options.MinimumIntegralDigits = 1;
	Options.MaximumIntegralDigits = 4;
	Options.MinimumFractionalDigits = 0;
	Options.MaximumFractionalDigits = 0;

	FText CurrentText = FText::AsNumber(InDamageValue, &Options);
	DamageText->SetText(CurrentText);
	
	if (bIsHero)
	{
		if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.Buff.AddHealth"))))
		{
			DamageText->SetColorAndOpacity(AddHealthTextStyle);
			return;
		}

		DamageText->SetColorAndOpacity(HeroDamageTextStyle);
		return;
	}

	
	if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.OnElement.BlueFlame"))))
	{
		DamageText->SetColorAndOpacity(BlueFlameDamageTextStyle);
	}
	else if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Fire"))))
	{
		DamageText->SetColorAndOpacity(FireDamageTextStyle);
	}
	else if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Water"))))
	{
		DamageText->SetColorAndOpacity(WaterDamageTextStyle);
	}
	else if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Ice"))))
	{
		DamageText->SetColorAndOpacity(IceDamageTextStyle);
	}
	else if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Poison"))))
	{
		DamageText->SetColorAndOpacity(PoisonDamageTextStyle);
	}
	else if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.OnElement.Electricity"))))
	{
		DamageText->SetColorAndOpacity(ElectricityDamageTextStyle);
	}
	else if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.Buff.AddHealth"))))
	{
		DamageText->SetColorAndOpacity(AddHealthTextStyle);
	}
	else if (AdditionalTagContainer.HasTag(FGameplayTag::RequestGameplayTag(FName("State.Hit.Shield"))))
	{
		DamageText->SetColorAndOpacity(ShieldDamageTextStyle);
	}
	else
	{
		DamageText->SetColorAndOpacity(DefaultDamageTextStyle);
	}
}

void UHCW_DamagePopUp::NativeConstruct()
{
	Super::NativeConstruct();
	GetWorld()->GetTimerManager().SetTimer(FadeTimerHandle, this, &UHCW_DamagePopUp::SelfDestruct, TimeTilFadeAway, false);
	K2_PostConstruct();
}

void UHCW_DamagePopUp::SelfDestruct()
{
	GetWorld()->GetTimerManager().ClearTimer(FadeTimerHandle);
	RemoveFromParent();
	RemoveFromRoot();
	Destruct();
}
