// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_EquipmentCard.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"
#include "Components/Button.h"
#include <Equipment/UHCEquipmentData.h>
#include "Equipment/HCEquipmentDefinition.h"
#include "UIs/HCW_UpgradeEquipment.h"

void UHCW_EquipmentCard::SetEquipmentInfo(UUHCEquipmentData* EquipmentData)
{
	if (EquipmentData)
	{
		m_OwningEquipmentData = EquipmentData;
		EquipmentImage->SetBrushFromTexture(EquipmentData->DisplayIcon);
		EquipmentName->SetText(EquipmentData->Name);

		// set level
		TSubclassOf<UHCEquipmentDefinition> InEquipmentDefinition = EquipmentData->EquipmentDefinition;
		const UHCEquipmentDefinition* EquipmentCD0 = GetDefault<UHCEquipmentDefinition>(InEquipmentDefinition);
		int32 AbilityLevel = EquipmentCD0->AbilityLevel;

		if (AbilityLevel < EquipmentData->MaxLevel)
		{
			FString LevelText = FString::Printf(TEXT("Level: %d"), AbilityLevel);
			EquipmentLevel->SetText(FText::FromString(LevelText));
		}
		else
		{
			EquipmentLevel->SetText(FText::FromString("Level: Max"));
		}

		// set description
		if (AbilityLevel < EquipmentData->MaxLevel && EquipmentData->UpgradeDescriptions.IsValidIndex(AbilityLevel))
		{
			EquipmentDescriptions->SetText(EquipmentData->UpgradeDescriptions[FMath::Clamp(AbilityLevel - 1, 0, EquipmentData->MaxLevel - 1)]);
		}
		else
		{
			EquipmentDescriptions->SetText(FText::FromString("Reached Max Level. Cannot Upgrade further."));
		}
	}
}

void UHCW_EquipmentCard::SelectCard()
{
	if (m_UpgradeEquipmentWidget)
	{
		m_UpgradeEquipmentWidget->SetUpgradeEquipment(m_OwningEquipmentData);
	}
}

void UHCW_EquipmentCard::SetOwner(UHCW_UpgradeEquipment* InOwner)
{
	if (InOwner)
	{
		m_UpgradeEquipmentWidget = InOwner;
	}
}
