// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_UpgradeEquipment.h"
#include "Components/VerticalBox.h"
#include "Components/VerticalBoxSlot.h"
#include <Equipment/UHCEquipmentData.h>
#include <Heroes/HCHeroCharacter.h>
#include <Kismet/GameplayStatics.h>
#include "Equipment/HCEquipmentManagerComponent.h"
#include "UIs/HCW_EquipmentCard.h"
#include "Components/Button.h"
#include <GameplayEffect.h>
#include "Abilities/HCAttributeSetBase.h"
#include "Abilities/HCAbilitySystemComponent.h"


void UHCW_UpgradeEquipment::SetEquipmentList(TArray<UUHCEquipmentData*> Infos)
{
	EquipmentList->ClearChildren();
	for (UUHCEquipmentData* Info : Infos)
	{
		if (Info)
		{
			if (UHCW_EquipmentCard* EquipmentCard = CreateWidget<UHCW_EquipmentCard>(this, WBP_EquipmentCardClass))
			{
				EquipmentCard->SetOwner(this);
				EquipmentCard->SetEquipmentInfo(Info);
				UVerticalBoxSlot* _Slot = EquipmentList->AddChildToVerticalBox(EquipmentCard);
				_Slot->SetSize(FSlateChildSize(ESlateSizeRule::Type::Fill));
				_Slot->SetPadding(FMargin(0.0f, 16.0f));
			}
		}
	}
}

void UHCW_UpgradeEquipment::SetUpgradeEquipment(UUHCEquipmentData* Data)
{
	if (Data)
	{
		m_EquipmentDataToUpgrade = Data;
		ConfirmUpgrade();
	}
}

void UHCW_UpgradeEquipment::ConfirmUpgrade()
{
	// TODO: upgrade the equipment
	// TODO: apply the left-over exp
	if (AHCHeroCharacter* Hero = Cast<AHCHeroCharacter>(GetOwningPlayerPawn()))
	{
		if (UHCAbilitySystemComponent* HCASC = Hero->GetHCAbilitySystemComponent())
		{
			float LeftOverExp = Hero->ProgressionSystemComponent->GetLeftOverExp();

			// Clear exp
			{
				UGameplayEffect* ExpGE = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("ClearExpGE")));
				ExpGE->DurationPolicy = EGameplayEffectDurationType::Instant;

				FGameplayModifierInfo ModifierInfo;
				ModifierInfo.Attribute = UHCAttributeSetBase::GetExperienceAttribute();
				ModifierInfo.ModifierOp = EGameplayModOp::Override;
				ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(0.0f));

				ExpGE->Modifiers.Add(ModifierInfo);

				FGameplayEffectContextHandle EffectContext = HCASC->MakeEffectContext();
				FActiveGameplayEffectHandle ActiveEffectHandle = HCASC->ApplyGameplayEffectToSelf(ExpGE, 1.0f, EffectContext);
			}

			// update max exp 
			{
				UGameplayEffect* ExpGE = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("UpdateMaxExpGE")));
				ExpGE->DurationPolicy = EGameplayEffectDurationType::Instant;

				FGameplayModifierInfo ModifierInfo;
				ModifierInfo.Attribute = UHCAttributeSetBase::GetMaxExperienceAttribute();
				ModifierInfo.ModifierOp = EGameplayModOp::Override;
				ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(Hero->ProgressionSystemComponent->GetMaxExp() * 1.2f));

				ExpGE->Modifiers.Add(ModifierInfo);

				FGameplayEffectContextHandle EffectContext = HCASC->MakeEffectContext();
				FActiveGameplayEffectHandle ActiveEffectHandle = HCASC->ApplyGameplayEffectToSelf(ExpGE, 1.0f, EffectContext);
			}

			// update exp
			{
				UGameplayEffect* ExpGE = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("UpdateExpGE")));
				ExpGE->DurationPolicy = EGameplayEffectDurationType::Instant;

				FGameplayModifierInfo ModifierInfo;
				ModifierInfo.Attribute = UHCAttributeSetBase::GetExperienceAttribute();
				ModifierInfo.ModifierOp = EGameplayModOp::Additive;
				ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(LeftOverExp));

				ExpGE->Modifiers.Add(ModifierInfo);

				FGameplayEffectContextHandle EffectContext = HCASC->MakeEffectContext();
				FActiveGameplayEffectHandle ActiveEffectHandle = HCASC->ApplyGameplayEffectToSelf(ExpGE, 1.0f, EffectContext);
			}
		}
	}

	UGameplayStatics::SetGamePaused(GetWorld(), false);
	RemoveFromParent();
}


