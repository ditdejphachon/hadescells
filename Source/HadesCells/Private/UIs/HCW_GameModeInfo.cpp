// Fill out your copyright notice in the Description page of Project Settings.


#include "UIs/HCW_GameModeInfo.h"
#include "Components/TextBlock.h"

void UHCW_GameModeInfo::SetObjectiveText(const FString& InObjectiveText)
{
	if (Objective)
	{
		Objective->SetText(FText::FromString(InObjectiveText));
	}
}

void UHCW_GameModeInfo::SetEnemyKilledAmount(uint8 InKilledAmount)
{
	FText NewValueText = FText::Format(FText::FromString("{0} / {1}"), InKilledAmount, TotalEnemyAmount);
	if (EnemiesKilledAmount)
	{
		EnemiesKilledAmount->SetText(NewValueText);
	}
}