// Fill out your copyright notice in the Description page of Project Settings.


#include "Input/HCInputConfig.h"
#include "InputAction.h"

UHCInputConfig::UHCInputConfig(const FObjectInitializer& ObjectInitializer)
{
}

const UInputAction* UHCInputConfig::FindNativeInputActionForTag(const FGameplayTag& InputTag, bool bLogNotFound) const
{
	for (const FHCInputAction& Action : NativeInputActions)
	{
		if (Action.InputAction && (Action.InputTag == InputTag))
		{
			return Action.InputAction;
		}
	}

	if (bLogNotFound)
	{
		UE_LOG(LogTemp, Error, TEXT("Can't find NativeInputAction for InputTag [%s] "), *InputTag.ToString());
	}

	return nullptr;
}

const UInputAction* UHCInputConfig::FindAbilityInputActionForTag(const FGameplayTag& InputTag, bool bLogNotFound) const
{
	for (const FHCInputAction& Action : AbilityInputActions)
	{
		if (Action.InputAction && (Action.InputTag == InputTag))
		{
			return Action.InputAction;
		}
	}

	if (bLogNotFound)
	{
		UE_LOG(LogTemp, Error, TEXT("Can't find AbilityInputActions for InputTag [%s] "), *InputTag.ToString());
	}

	return nullptr;
}
