// Fill out your copyright notice in the Description page of Project Settings.


#include "Heroes/HCHeroCharacter.h"

#pragma region includes
// GAS
#include "Abilities/HCAbilitySystemComponent.h"
#include "Abilities/HCAttributeSetBase.h"

// components
#include "Components/CapsuleComponent.h"
#include "HCCharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Actor Components
#include "Equipment/HCEquipmentManagerComponent.h"

// inputs
#include "Components/InputComponent.h"
#include "Input/HCInputConfig.h"
#include "Input/HCInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"

// Kismets
#include <Characters/HCPlayerController.h>
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>

// Equipment
#include "Equipment/HCEquipmentInstance.h"
#include "Equipment/Weapon/HCWeaponBase.h"
#include <UIs/HCHUDBase.h>

//Interactable
#include "Interactables/HCInteractableComponent.h"
#pragma endregion

//GameMode
#include <HadesCells/HadesCellsGameMode.h>
#include <Save/HCGIBase.h>
#include <Save/HCSaveGameBase.h>
#include <Engine/AssetManager.h>

AHCHeroCharacter::AHCHeroCharacter(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(FName("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bUsePawnControlRotation = false;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(FName("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom);

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	// Makes sure that the animations play on the Server so that we can use bone and socket transforms
	// to do things like spawning projectiles and other FX.
	GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionProfileName(FName("NoCollision"));

	bHasBindedConfirmCancelToASC = false;
	CurrentInteractableComponent = nullptr;
	bAllowOnTickLookAtCursor = false;

	ProgressionSystemComponent->OnExpFull.AddUObject(this, &AHCHeroCharacter::HandleExpFull);
}

void  AHCHeroCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (FollowCamera && CameraBoom)
	{
		FollowCamera->FieldOfView = FollowCameraFOV;
		CameraBoom->TargetArmLength = CameraDistance;
		//CameraBoom->TargetOffset = FVector(0.0f, CameraDistance * bsFactor_1 ,CameraDistance * bsFactor_2);
		CameraBoom->TargetOffset = FVector(bsFactor_1, bsFactor_2, bsFactor_3);

	}
}

void AHCHeroCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	/*const AHCPlayerController* PC = Cast<AHCPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	check(PC);*/
	const APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	check(PC);

	const ULocalPlayer* LC = PC->GetLocalPlayer();
	check(LC);

	UEnhancedInputLocalPlayerSubsystem* InputSubSystem = LC->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>();
	check(InputSubSystem);

	InputSubSystem->ClearAllMappings();

	// add input mapping context into E-input subsystem
	InputSubSystem->AddMappingContext(DefaultInputMappingContext,0);

	if (PlayerInputComponent && PawnData)
	{
		UHCInputConfig* IC = PawnData->InputConfig;
		UHCInputComponent* HCIC = CastChecked<UHCInputComponent>(PlayerInputComponent);
		// TODO: bind custom control settings [see ULyraInputComponent::AddInputMappings]

		// bind ability inputs
		TArray<uint32> BindHandles;
		HCIC->BindAbilityActions(IC, this, &AHCHeroCharacter::Input_AbilityInputTagPressed, &AHCHeroCharacter::Input_AbilityInputTagReleased, BindHandles);
		//HCIC->BindAbilityActions(IC, this, &AHCHeroCharacter::Input_AbilityInputTagTriggered, BindHandles);

		// bind native inputs
		HCIC->BindNativeAction(IC, FGameplayTag::RequestGameplayTag(FName("Movement")), ETriggerEvent::Triggered, this,  &AHCHeroCharacter::Move);
		HCIC->BindNativeAction(IC, FGameplayTag::RequestGameplayTag(FName("Interact")), ETriggerEvent::Triggered, this, &AHCHeroCharacter::Interact);
		//HCIC->BindNativeAction(IC, FGameplayTag::RequestGameplayTag(FName("LookAtCursor")), ETriggerEvent::Triggered, this, &AHCHeroCharacter::LookAtCursor);
	}
}


void AHCHeroCharacter::BeginPlay()
{
	Super::BeginPlay();

	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (PC)
	{
		PC->bShowMouseCursor = true;
		PC->bEnableClickEvents = true;
		PC->bEnableMouseOverEvents = true;
	}

	if (AHCHUDBase* HUD = Cast<AHCHUDBase>(PC->GetHUD()))
	{
		if (HealthComponent)
		{
			HUD->SetUpHeroOverlayHealthSection(HealthComponent);
		}

		if (EquipmentManagerComponent)
		{
			HUD->SetUpHeroOverlayEquipmentSection(EquipmentManagerComponent);
		}
		HUD->ToggleHeroOverlay(true);
	}

	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetDashCountAttribute()).AddUObject(this, &AHCHeroCharacter::HandleDashCountChanged);
		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetMaxDashCountAttribute()).AddUObject(this, &AHCHeroCharacter::HandleMaxDashCountChanged);
		AbilitySystemComponent->OnActiveGameplayEffectAddedDelegateToSelf.AddUObject(this, &AHCHeroCharacter::OnActiveGameplayEffectAddedCallback);
	}
	
	// Load Game
	if (UHCGIBase* GI = Cast<UHCGIBase>(GetGameInstance()))
	{
		if (UHCSaveGameBase* SG = GI->LoadGame())
		{
			// load and apply old health
			{
				float oldHealth = SG->PlayerHealth;
				if (oldHealth == 0.0f)
				{
					UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::BeginPlay] zero health. Skip."));
					return;
				}
				UGameplayEffect* LoadHealthGE = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("GE_LoadHealth")));
				LoadHealthGE->DurationPolicy = EGameplayEffectDurationType::Instant;

				FGameplayModifierInfo ModifierInfo;
				ModifierInfo.Attribute = AttributeSetBase->GetHealthAttribute();
				ModifierInfo.ModifierOp = EGameplayModOp::Override;
				ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(oldHealth));
				LoadHealthGE->Modifiers.Add(ModifierInfo);

				FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
				FActiveGameplayEffectHandle ActiveEffectHandle = AbilitySystemComponent->ApplyGameplayEffectToSelf(LoadHealthGE, 1.0f, EffectContext);
			}

			// load and apply old shield
			{
				float oldShield = SG->PlayerShield;
				UGameplayEffect* LoadShieldGE = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("GE_LoadShield")));
				LoadShieldGE->DurationPolicy = EGameplayEffectDurationType::Instant;

				FGameplayModifierInfo ModifierInfo;
				ModifierInfo.Attribute = AttributeSetBase->GetShieldAttribute();
				ModifierInfo.ModifierOp = EGameplayModOp::Override;
				ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(oldShield));
				LoadShieldGE->Modifiers.Add(ModifierInfo);

				FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
				FActiveGameplayEffectHandle ActiveEffectHandle = AbilitySystemComponent->ApplyGameplayEffectToSelf(LoadShieldGE, 1.0f, EffectContext);
			}
			
			// load and apply equipment
			{
				if (TObjectPtr<UUHCEquipmentData> PWED = SG->PrimaryWeaponED)
				{
					EquipmentManagerComponent->EquipItem(PWED);
				}
				if (TObjectPtr<UUHCEquipmentData> SWED = SG->SecondaryWeaponED)
				{
					EquipmentManagerComponent->EquipItem(SWED);
				}
				if (TObjectPtr<UUHCEquipmentData> PSED = SG->PrimarySkillED)
				{
					EquipmentManagerComponent->EquipItem(PSED);
				}
				if (TObjectPtr<UUHCEquipmentData> SWED = SG->SecondarySkillED)
				{
					EquipmentManagerComponent->EquipItem(SWED);
				}
			}

			// Do asset loading here
			/*{
				FPrimaryAssetId PWPAI = SG->PrimaryWeaponPAI;
				FPrimaryAssetId SWPAI = SG->SecondaryWeaponPAI;
				FPrimaryAssetId PSPAI = SG->PrimarySkillPAI;
				FPrimaryAssetId SSPAI = SG->SecondarySkillPAI;
				UAssetManager* Manager = UAssetManager::GetIfValid();
				if (Manager)
				{
					UUHCEquipmentData* PWED = Cast<UUHCEquipmentData>(Manager->GetPrimaryAssetObject(PWPAI));
					if (PWED)
					{
						EquipmentManagerComponent->EquipItem(PWED);
					}

					UUHCEquipmentData* SWED = Cast<UUHCEquipmentData>(Manager->GetPrimaryAssetObject(SWPAI));
					if (SWED)
					{
						EquipmentManagerComponent->EquipItem(SWED);
					}

					UUHCEquipmentData* PSED = Cast<UUHCEquipmentData>(Manager->GetPrimaryAssetObject(PSPAI));
					if (PSED)
					{
						EquipmentManagerComponent->EquipItem(PSED);
					}

					UUHCEquipmentData* SSED = Cast<UUHCEquipmentData>(Manager->GetPrimaryAssetObject(SSPAI));
					if (SSED)
					{
						EquipmentManagerComponent->EquipItem(SSED);
					}
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::BeginPlay] cannot get asset manager"));
				}
			}*/
		}
	}
}

void AHCHeroCharacter::Tick(float DeltaSeconds)
{
	if (bAllowOnTickLookAtCursor)
	{
		LookAtCursor();
	}
}

void AHCHeroCharacter::HandleHealthChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
	UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleHealthChanged] Start HandleHealthChanged"));

	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleHealthChanged] already dead. returning"));
		return;
	}

	if (AHCHUDBase* HUD = Cast<AHCHUDBase>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
	{
		if (HealthComponent)
		{
			HUD->SetUpHeroOverlayHealthSection(HealthComponent);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleHealthChanged] HealthComponent is invalid"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleHealthChanged] Player controller is invalid"));
	}

	if (InNewValue <= 0.0f)
	{
		Die();
		FinishDying();
	}
}

void AHCHeroCharacter::HandleShieldChanged(UHCHealthComponent* InHealthComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleHealthChanged] already dead. returning"));
		return;
	}

	if (bIsShieldBreak)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleShieldChanged] Shield already broken"));
		return;
	}

	if (AHCHUDBase* HUD = Cast<AHCHUDBase>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
	{
		if (HealthComponent)
		{
			HUD->SetUpHeroOverlayHealthSection(HealthComponent);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleShieldChanged] HealthComponent is invalid"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleShieldChanged] Player controller is invalid"));
	}

	if (InNewValue <= 0.0f && !bIsShieldBreak)
	{
		PlayCameraShake();
		bIsShieldBreak = true;
	}

}

void AHCHeroCharacter::HandleGoldChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
	UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleGoldChanged] Start HandleGoldChanged"));

	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleGoldChanged] already dead. returning"));
		return;
	}

	if (AHCHUDBase* HUD = Cast<AHCHUDBase>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
	{
		if (ProgressionSystemComponent)
		{
			HUD->SetUpHeroGoldSection(ProgressionSystemComponent);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleGoldChanged] ProgressionSystemComponent is invalid"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleGoldChanged] Player controller is invalid"));
	}
}

void AHCHeroCharacter::HandleExpChanged(UHCProgressionSystemComponent* InProgressionComponent, float InOldValue, float InNewValue, AActor* InInstigator)
{
	UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpChanged] Start HandleExpChanged"));

	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpChanged] already dead. returning"));
		return;
	}

	if (AHCHUDBase* HUD = Cast<AHCHUDBase>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
	{
		if (ProgressionSystemComponent)
		{
			HUD->SetUpHeroExpSection(ProgressionSystemComponent);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpChanged] ProgressionSystemComponent is invalid"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpChanged] Player controller is invalid"));
	}
}

void AHCHeroCharacter::HandleExpFull(UHCProgressionSystemComponent* InProgressionComponent)
{
	UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpFull] Start HandleExpFull"));

	if (bHasDied)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpFull] already dead. returning"));
		return;
	}

	if (AHCHUDBase* HUD = Cast<AHCHUDBase>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHUD()))
	{
		if (ProgressionSystemComponent)
		{
			// TODO: Get HUD and toggle upgrade UI
			HUD->ToggleEquipmentUpgradeUI(true);

			// TODO: move this part after upgrade confirm in the UI component
			{
				// TODO: create new Exp GE from the left-over Exp	
				// TODO: apply it to the character
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpFull] ProgressionSystemComponent is invalid"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::HandleExpFull] Player controller is invalid"));
	}
	
}

void AHCHeroCharacter::HandleDashCountChanged(const FOnAttributeChangeData& ChangeData)
{
	if (FMath::IsNearlyZero(ChangeData.NewValue))
	{
		// Apply refill gameplay effect
		UGameplayEffect* RefillEffect = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("DashCountRefillEffect")));
		RefillEffect->DurationPolicy = EGameplayEffectDurationType::Instant;

		FGameplayModifierInfo ModifierInfo;
		ModifierInfo.Attribute = AttributeSetBase->GetDashCountAttribute();
		ModifierInfo.ModifierOp = EGameplayModOp::Override;
		ModifierInfo.ModifierMagnitude = FGameplayEffectModifierMagnitude(FScalableFloat(GetMaxDashCount()));
		RefillEffect->Modifiers.Add(ModifierInfo);

		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		FActiveGameplayEffectHandle ActiveEffectHandle = AbilitySystemComponent->ApplyGameplayEffectToSelf(RefillEffect, 1.0f, EffectContext);
	}
}

void AHCHeroCharacter::HandleMaxDashCountChanged(const FOnAttributeChangeData& ChangeData)
{
}

void AHCHeroCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (FollowCamera)
	{
		// find out which way is forward
		
		const FRotator Rotation = FollowCamera->GetComponentRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// set hero's world direction
		HeroWorldDirection = FRotationMatrix(YawRotation).TransformVector(FVector(MovementVector.Y, MovementVector.X, 0.0f));

		if (bCanMove)
		{
			// get forward vector
			const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

			// get right vector 
			const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

			// add movement 
			AddMovementInput(ForwardDirection, MovementVector.Y);
			AddMovementInput(RightDirection, MovementVector.X);

		}
		//GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Yellow, FString::Printf(TEXT("MovementVector: [%s]"), *MovementVector.ToString()));
	}
}

void AHCHeroCharacter::Interact(const FInputActionValue& Value)
{
	if (CurrentInteractableComponent)
	{
		CurrentInteractableComponent->BeginInteraction(this);
	}

}

FVector AHCHeroCharacter::LookAtCursor()
{
	//if (!FMath::IsNearlyZero(GetVelocity().Length()))
	//{
	//	return;
	//}

	const APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (PC)
	{
		FVector StartMouseWorldLocation;
		FVector StartMouseWorldDirection;
		PC->DeprojectMousePositionToWorld(StartMouseWorldLocation, StartMouseWorldDirection);

		CursorWorldLocation = StartMouseWorldLocation + (StartMouseWorldDirection * LookAtRayCastLength); // TODO: change to UPROPERTY

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);

		FHitResult Hit;
		GetWorld()->LineTraceSingleByChannel(Hit, StartMouseWorldLocation, CursorWorldLocation, ECC_Visibility, QueryParams);

		FRotator PlayerRot = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), Hit.Location);

		SetActorRotation(FRotator(0.0f, PlayerRot.Yaw, 0.0f));

		LookAtLocation = Hit.Location;

		return Hit.Location;
	}

	return FVector::ZeroVector;
}

float AHCHeroCharacter::GetDashCount() const
{
	if (AttributeSetBase)
	{
		return AttributeSetBase->GetDashCount();
	}
	return 0.0f;
}

float AHCHeroCharacter::GetMaxDashCount() const
{
	if (AttributeSetBase)
	{
		return AttributeSetBase->GetMaxDashCount();
	}
	return 0.0f;
}

void AHCHeroCharacter::BindConfirmCancelToASC()
{
	if (!bHasBindedConfirmCancelToASC && AbilitySystemComponent)
	{
	/*	AbilitySystemComponent->BindAbilityActivationToInputComponent
		(
			InputComponent, 
			FGameplayAbilityInputBinds(
				FString("ConfirmTarget"),
				FString("CancelTarget"), 
				FString("EHCAbilityInputID"),
				static_cast<int32>(EHCAbilityInputID::Confirm),
				static_cast<int32>(EHCAbilityInputID::Cancel)
			)
		);*/

		bHasBindedConfirmCancelToASC = true;
	}
}

void AHCHeroCharacter::PlayCameraShake()
{
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayWorldCameraShake(GetWorld(), CameraShake, GetActorLocation(), InnerRadius, OuterRadius, Falloff, bOrientShakeTowardsEpicenter);
}

void AHCHeroCharacter::Input_AbilityInputTagPressed(FGameplayTag InputTag)
{
	if (UHCAbilitySystemComponent* HCASC = GetHCAbilitySystemComponent())
	{
		FGameplayTag Attack_1 = FGameplayTag::RequestGameplayTag(FName(TEXT("Ability.Attack.1")));
		FGameplayTag Attack_2 = FGameplayTag::RequestGameplayTag(FName(TEXT("Ability.Attack.2")));

		// TODO: recheck whether currentAttactWeapon assignment is really needed
		if (InputTag.MatchesTag(Attack_1) && EquipmentManagerComponent->GetEquipmentList().MainWeapon.IsValid())
		{
			TArray<AActor*> Weapons = EquipmentManagerComponent->GetSpawnActorFromEquipmentSlotType(EHCEquipmentSlotType::MainWeapon);
			//if (Weapons.IsValidIndex(0) && Weapons[0] && CurrentAttackWeapon != Weapons[0])
			if (Weapons.IsValidIndex(0) && Weapons[0])
			{
				CurrentAttackWeapon = Cast<AHCWeaponBase>(Weapons[0]);
				if (CurrentAttackWeapon)
				{
					CurrentAttackWeapon->InstigatorActor = this;

				}
			}
		}
		else if (InputTag.MatchesTag(Attack_2) && EquipmentManagerComponent->GetEquipmentList().SecondaryWeapon.IsValid())
		{
			TArray<AActor*> Weapons = EquipmentManagerComponent->GetSpawnActorFromEquipmentSlotType(EHCEquipmentSlotType::SecondaryWeapon);
			if (Weapons.IsValidIndex(0) && Weapons[0])
			{
				CurrentAttackWeapon = Cast<AHCWeaponBase>(Weapons[0]);
				if (CurrentAttackWeapon)
				{
					CurrentAttackWeapon->InstigatorActor = this;

				}
			}
		}
	
		HCASC->AbilityInputTagPressed(InputTag);
	}
}

void AHCHeroCharacter::Input_AbilityInputTagReleased(FGameplayTag InputTag)
{
	if (UHCAbilitySystemComponent* HCASC = GetHCAbilitySystemComponent())
	{
		HCASC->AbilityInputTagReleased(InputTag);
	}
}
void AHCHeroCharacter::Die()
{
	AHCCharacterBase::Die();
	ClearTimerHandles();
}
void AHCHeroCharacter::FinishDying()
{
	if (HealthComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::FinishDying] Clearing Delegates"));

		HealthComponent->OnHealthChanged.RemoveAll(this);
		HealthComponent->OnShieldChanged.RemoveAll(this);
		HealthComponent->OnPostureChanged.RemoveAll(this);
		HealthComponent->OnMaxHealthChanged.RemoveAll(this);
		HealthComponent->OnMaxShieldChanged.RemoveAll(this);
		HealthComponent->OnMaxPostureChanged.RemoveAll(this);
	}

	if (APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		if (AHCHUDBase* HUD = Cast<AHCHUDBase>(PC->GetHUD()))
		{
			HUD->ToggleHeroOverlay(false);
			HUD->ToggleDeathScreen(true);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::FinishDying] HUD is invalid"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[AHCHeroCharacter::FinishDying] PC is invalid"));
	}
}

void AHCHeroCharacter::OnActiveGameplayEffectAddedCallback(UAbilitySystemComponent* Target, const FGameplayEffectSpec& SpecApplied, FActiveGameplayEffectHandle ActiveHandle)
{
 	FGameplayTagContainer GrantedTags;
	SpecApplied.GetAllGrantedTags(GrantedTags);

	const FGameplayTag CooldownTag_1 = FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item1"));
	const FGameplayTag CooldownTag_2 = FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item2"));

	if (GrantedTags.HasTag(CooldownTag_1))
	{
		HandleItemCooldown(CooldownTag_1);
	}
	else if (GrantedTags.HasTag(CooldownTag_2))
	{
		HandleItemCooldown(CooldownTag_2);
	}
}

void AHCHeroCharacter::HandleItemCooldown(const FGameplayTag CallbackTag)
{
	if (APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		if (AHCHUDBase* HUD = Cast<AHCHUDBase>(PC->GetHUD()))
		{
			FGameplayEffectQuery const Query = FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(FGameplayTagContainer(CallbackTag));
			TArray< TPair<float, float> > DurationAndTimeRemaining = AbilitySystemComponent->GetActiveEffectsTimeRemainingAndDuration(Query);
			if (DurationAndTimeRemaining.Num() > 0)
			{
				if (CallbackTag == FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item1")))
				{
					HUD->EnableEquipmentCooldown(CallbackTag, DurationAndTimeRemaining[0].Value);	
				}
				else if (CallbackTag == FGameplayTag::RequestGameplayTag(FName("Cooldown.Ability.item2")))
				{
					HUD->EnableEquipmentCooldown(CallbackTag, DurationAndTimeRemaining[0].Value);
				}
			}
		}
	}
	

}
//
//void AHCHeroCharacter::Input_AbilityInputTagTriggered(const FInputActionInstance& InputActionInstance, FGameplayTag InputTag)
//{
//	if (UHCAbilitySystemComponent* HCASC = GetHCAbilitySystemComponent())
//	{
//		const FInputActionValue inputActionValue = InputActionInstance.GetValue();
//		if (inputActionValue.Get<bool>())
//		{
//			Input_AbilityInputTagPressed(InputTag);
//		}
//		else
//		{
//			Input_AbilityInputTagReleased();
//		}
//	}
//}
//
//
