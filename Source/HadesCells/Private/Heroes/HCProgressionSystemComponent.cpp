// Fill out your copyright notice in the Description page of Project Settings.


#include "Heroes/HCProgressionSystemComponent.h"
#include "Abilities/HCAbilitySystemComponent.h"
#include "Abilities/HCAttributeSetBase.h"
#include "GameplayEffectExtension.h"

UHCProgressionSystemComponent::UHCProgressionSystemComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	AbilitySystemComponent = nullptr;

}

void UHCProgressionSystemComponent::InitializeWithAbilitySystem(UHCAbilitySystemComponent* InASC)
{
	AActor* Owner = GetOwner();
	check(Owner);

	if (AbilitySystemComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("[UHCProgressionSystemComponent::InitializeWithAbilitySystem]: owner [%s] has already been initialized with an ability system."), *GetNameSafe(Owner));
		return;
	}

	AbilitySystemComponent = InASC;
	if (!AbilitySystemComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("[UHCProgressionSystemComponent::InitializeWithAbilitySystem]: Cannot initialize progression system component for owner [%s] with NULL ability system."), *GetNameSafe(Owner));
		return;
	}

	AttributeSet = AbilitySystemComponent->GetSet<UHCAttributeSetBase>();
	if (!AttributeSet)
	{
		UE_LOG(LogTemp, Error, TEXT("[UHCProgressionSystemComponent::InitializeWithAbilitySystem]: Cannot initialize progression system component for owner [%s] with NULL attribute set on the ability system."), *GetNameSafe(Owner));
		return;
	}

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetGoldAttribute()).AddUObject(this, &UHCProgressionSystemComponent::HandleGoldChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetExperienceAttribute()).AddUObject(this, &UHCProgressionSystemComponent::HandleExpChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UHCAttributeSetBase::GetMaxExperienceAttribute()).AddUObject(this, &UHCProgressionSystemComponent::HandleMaxExpChanged);

	OnGoldChanged.Broadcast(this, AttributeSet->GetGold(), AttributeSet->GetGold(), nullptr);
	OnExpChanged.Broadcast(this, AttributeSet->GetExperience(), AttributeSet->GetMaxExperience(), nullptr);
	OnMaxExpChanged.Broadcast(this, AttributeSet->GetMaxExperience(), AttributeSet->GetMaxExperience(), nullptr);
}

void UHCProgressionSystemComponent::UninitializeFromAbilitySystem()
{
}

float UHCProgressionSystemComponent::GetExp() const
{
	return (AttributeSet ? AttributeSet->GetExperience() : 0.0f);
}

float UHCProgressionSystemComponent::GetMaxExp() const
{
	return (AttributeSet ? AttributeSet->GetMaxExperience() : 0.0f);
}

float UHCProgressionSystemComponent::GetExpNormalized() const
{
	if (AttributeSet)
	{
		const float Exp = AttributeSet->GetExperience();
		const float MaxExp = AttributeSet->GetMaxExperience();

		return ((MaxExp > 0.0f) ? (Exp / MaxExp) : 0.0f);
	}

	return 0.0f;
}

float UHCProgressionSystemComponent::GetGold() const
{
	return (AttributeSet ? AttributeSet->GetGold() : 0.0f);
}

static AActor* GetInstigatorFromAttrChangeData(const FOnAttributeChangeData& ChangeData)
{
	if (ChangeData.GEModData != nullptr)
	{
		const FGameplayEffectContextHandle& EffectContext = ChangeData.GEModData->EffectSpec.GetEffectContext();
		return EffectContext.GetOriginalInstigator();
	}

	return nullptr;
}

void UHCProgressionSystemComponent::HandleGoldChanged(const FOnAttributeChangeData& ChangeData)
{
	OnGoldChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}

void UHCProgressionSystemComponent::HandleExpChanged(const FOnAttributeChangeData& ChangeData)
{
	float currentExp = GetExp();
	float currentMaxEp = GetMaxExp();
	if (currentExp >= currentMaxEp)
	{
		OnExpChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
		m_LeftOverExp = currentExp - currentMaxEp;
		OnExpFull.Broadcast(this);
	}
	else
	{
		OnExpChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
	}
}

void UHCProgressionSystemComponent::HandleMaxExpChanged(const FOnAttributeChangeData& ChangeData)
{
	OnMaxExpChanged.Broadcast(this, ChangeData.OldValue, ChangeData.NewValue, GetInstigatorFromAttrChangeData(ChangeData));
}
